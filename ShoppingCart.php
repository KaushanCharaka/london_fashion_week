<?php
session_start();
$session_value=(isset($_SESSION['login_user']))?$_SESSION['login_user']:'';
?>
<!DOCTYPE html>
<html>

<head>
    <title>London Fashion Week</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />

    <link rel="stylesheet" href="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css" />
    <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.3/jquery.min.js"></script>
    <script src="engine1/jquery.payment.js"></script>

    <style>
        .ui-grid-b {
            height: 100px !important;
        }
        
        #change-b {
            height: 40px !important;
            font-family: "Source Sans Pro";
            font-size: 18px;
        }
        
        img {
            max-width: 100%;
            max-height: 100%;
            display: block;
            margin-left: auto;
            margin-right: auto;
            margin-top: auto;
            margin-bottom: auto;
            align: center;
        }
        
        .portrait {
            height: 80px;
            width: 25px;
        }
        
        .landscape {
            height: 25px;
            width: 80px;
        }
        
        .square {
            height: 75px;
            width: 75px;
        }
        
        #header-grid {
            background-color: black;
            border-style: none !important;
        }
        
        .ui-grid {
            background-color: black;
            border-style: none !important;
        }
        
        [data-role=page] {
            height: 100% !important;
            position: relative !important;
            font-family: "Source Sans Pro"
        }
        
        [data-role=header] {
            font-size: 25px;
            align-content: center;
        }
        
        [data-role=content] {
            height: 100%;
            margin: 0 auto;
            width: auto;
        }
        /* [data-role=footer] {
            position: relative !important;
            top: auto !important;
            width: 100%;
        } */
        
        [data-role=panel] {
            font-family: "Source Sans Pro";
            color: white;
        }
        
        .ui-block-a {
            margin-top: 5px;
            margin-bottom: 5px;
        }
        
        .ui-block-e {
            margin-top: 5px;
            margin-bottom: 5px;
            align: center;
        }
        
        #topbar {
            margin-top: 0px;
            margin-bottom: 0px;
            align: center;
        }
        
        .ui-block-b {
            margin-top: 15px;
            text-align: center;
            font-family: "Source Sans Pro";
            color: white;
            font-weight: normal;
            font-size: 18px;
            text-shadow: none;
        }
        
        .ui-bar {
            text-align: center;
            font-family: "Source Sans Pro";
            color: white;
            font-weight: normal;
            font-size: 12px;
            text-shadow: none;
            border-style: none !important;
        }
        
        .ui-block-c {
            margin-top: 16px;
        }
        
        .ui-block-d {
            margin-top: 18px;
        }
        
        body,
        input,
        select,
        textarea,
        button,
        .ui-btn {
            line-height: 1.3;
            font-family: "Source Sans Pro";
        }
        
        #search-basic {
            font-size: 24sp;
        }
        
        .ui-panel-position-right.ui-panel-display-reveal {
            -webkit-box-shadow: left;
        }
        
        .ui-btn-icon-left:after,
        .ui-btn-icon-right:after,
        .ui-btn-icon-top:after,
        .ui-btn-icon-bottom:after,
        .ui-btn-icon-notext:after {
            content: none;
        }
        
        .ui-icon-delete:after {
            background-color: black;
        }
        
        .ui-listview>li.ui-last-child>a.ui-btn {
            border-bottom-width: 0px;
        }
        
        .ui-page-theme-a .ui-btn {
            background-color: white;
        }
        
        .ui-panel-inner {
            background-color: white;
        }
        
        #wowslider-container1 {
            margin: 0px;
        }
        
        .ui-content {
            padding: 0px;
        }
        
        .ui-grid-solo {
            text-align: center;
            font-family: "Source Sans Pro";
        }
        
        #news-content {
            text-align: justify;
            margin-left: 20px;
            margin-right: 20px;
        }
        
        #center-button {
            margin: 0 auto;
            text-align: center;
            font-family: "Source Sans Pro";
            padding: 1px;
        }
        
        li.ui-block-b.ui-state-default.ui-corner-top {
            margin-top: 0px;
        }
        
        li.ui-block-c.ui-state-default.ui-corner-top {
            margin-top: 0px;
        }
        
        li.ui-block-a.ui-state-default.ui-corner-top {
            margin-top: 0px;
        }
        
        li.ui-block-d.ui-state-default.ui-corner-top {
            margin-top: 0px;
        }
        
        [data-role="navbar"] {
            padding: 15px;
        }
        
        .ui-mini {
            margin-left: 18px;
            margin-right: 18px;
            margin-top: 19px;
            margin-bottom: 0px;
        }
        
        .ui-page-theme-a .ui-btn.ui-btn-active {
            background-color: black;
            border-color: black;
            lighting-color: aliceblue
        }
        
        .ui-input-text,
        .ui-input-search {
            margin-left: 18px;
            margin-right: 18px;
            margin-top: 0px;
        }
        
        #login-text {
            font-family: "Source Sans Pro";
            margin-left: 18px;
            margin-top: 18px;
            font-weight: bold;
        }
        
        #login-btn {
            font-family: "Source Sans Pro";
            margin-left: 18px;
            margin-right: 18px;
            margin-top: 30px;
            font-weight: bold;
        }
        
        #customButton {
            font-family: "Source Sans Pro";
            margin: 30px;
            margin-top: 20px;
            font-weight: bold;
        }
        
        .has-error input {
            border-width: 2px;
        }
        
        .validation.text-danger:after {
            content: 'Validation failed';
        }
        
        .validation.text-success:after {
            content: 'Validation passed';
        }
        
        #top-image {
            margin-top: 0px;
            margin-bottom: 0px;
        }
    </style>
</head>

<body>

    <div data-role="page">

        <div data-role="header" data-position="fixed" style="border-style: none;">
            <div class="ui-grid-c" id="header-grid">
                <div class="ui-block-a" style="width: 30% !important; "><a href="http://localhost/London_Fashion_Week/product.php" data-transition="slide" data-ajax="false">
                <img src="images/back.png " width="50px " height="50px " alt="London Fashion Week Logo "></a></div>
                <div class="ui-block-b" style="width: 40% !important; ">Shopping Cart</div>
                <div class="ui-block-c" style="width: 15% !important; "><a href="http://localhost/London_Fashion_Week/ShoppingCart.php" data-transition="slide" data-ajax="false">
                <img src="images/cart.png " width="30px " height="27px " alt="Cart "></a></div>
                <div class="ui-block-d" style="width: 15% !important; ">
                    <a href="#nav-panel"><img src="images/hamburger.png " width="28px " height="23px " alt="Cart "></a>
                </div>
            </div>
            <!-- /grid-b -->
        </div>
        <!-- /header -->

        <?php          
   
   if(!isset($_SESSION['login_user'])){
       // header("Location: http://".$_SERVER['HTTP_HOST']."/London_Fashion_Week/Login.php", true, 302);
       echo "<div data-role=\"panel\" data-position=\"right\" data-display=\"reveal\" data-theme=\"c\" id=\"nav-panel\">";
       echo "<ul data-role=\"listview\">";
       echo "<li data-icon=\"delete\"><a href=\"#\" data-rel=\"close\">Menu</a></li>";    
       echo "<input type=\"search\" name=\"search\" data-corners=\"false\" id=\"search-basic\" value=\"\" />";    
       echo " <li><a href=\"http://localhost/London_Fashion_Week/Login.php\" data-transition=\"slide\" data-ajax=\"false\">Sign in</a></li>"; 
       echo " <li><a href=\"http://localhost/London_Fashion_Week/Register.php\" data-transition=\"slide\" data-ajax=\"false\">Register</a></li>";
       echo " <li><a href=\"http://localhost/London_Fashion_Week/index.php\" data-transition=\"slide\" data-ajax=\"false\">Home</a></li>"; 
       echo " <li><a href=\"http://localhost/London_Fashion_Week/all_news.php\" data-transition=\"slide\" data-ajax=\"false\">News</a></li>"; 
       echo " <li><a href=\"http://localhost/London_Fashion_Week/portfolio.php\" data-transition=\"slide\" data-ajax=\"false\">Portfolio</a></li>"; 
       echo " <li><a href=\"http://localhost/London_Fashion_Week/Events.php\" data-transition=\"slide\" data-ajax=\"false\">Events</a></li>"; 
       echo " <li><a href=\"http://localhost/London_Fashion_Week/Favorite_list.php\" data-transition=\"slide\" data-ajax=\"false\">Favorite List</a></li>";
       echo " <li><a href=\"http://localhost/London_Fashion_Week/Point_of_Interest.php\" data-transition=\"slide\" data-ajax=\"false\">Point of Interest</a></li>"; 
       echo " <li><a href=\"http://localhost/London_Fashion_Week/gallery.php\" data-transition=\"slide\" data-ajax=\"false\">Gallery</a></li>";   
       echo " <li><a href=\"http://localhost/London_Fashion_Week/AboutUs.php\" data-transition=\"slide\" data-ajax=\"false\">About Us</a></li>";   
       echo " </ul>";   
       echo "</div>";
   }else{
       echo "<div data-role=\"panel\" data-position=\"right\" data-display=\"reveal\" data-theme=\"c\" id=\"nav-panel\">";
       echo "<ul data-role=\"listview\">";
       echo "<li data-icon=\"delete\"><a href=\"#\" data-rel=\"close\">Menu</a></li>";    
       echo "<input type=\"search\" name=\"search\" data-corners=\"false\" id=\"search-basic\" value=\"\" />";    
       echo " <li><a href=\"http://localhost/London_Fashion_Week/index.php\" data-transition=\"slide\" data-ajax=\"false\">Home</a></li>";             
       echo " <li><a href=\"http://localhost/London_Fashion_Week/MyAccount.php\" data-transition=\"slide\" data-ajax=\"false\">My Account</a></li>";
       echo " <li><a href=\"http://localhost/London_Fashion_Week/all_news.php\" data-transition=\"slide\" data-ajax=\"false\">News</a></li>"; 
       echo " <li><a href=\"http://localhost/London_Fashion_Week/portfolio.php\" data-transition=\"slide\" data-ajax=\"false\">Portfolio</a></li>"; 
       echo " <li><a href=\"http://localhost/London_Fashion_Week/Events.php\" data-transition=\"slide\" data-ajax=\"false\">Events</a></li>"; 
       echo " <li><a href=\"http://localhost/London_Fashion_Week/Favorite_list.php\" data-transition=\"slide\" data-ajax=\"false\">Favorite List</a></li>";
       echo " <li><a href=\"http://localhost/London_Fashion_Week/Point_of_Interest.php\" data-transition=\"slide\" data-ajax=\"false\">Point of Interest</a></li>"; 
       echo " <li><a href=\"http://localhost/London_Fashion_Week/gallery.php\" data-transition=\"slide\" data-ajax=\"false\">Gallery</a></li>";   
       echo " <li><a href=\"http://localhost/London_Fashion_Week/AboutUs.php\" data-transition=\"slide\" data-ajax=\"false\">About Us</a></li>";    
       echo " <li><a href=\"http://localhost/London_Fashion_Week/Logout.php\" data-transition=\"slide\" data-ajax=\"false\">Sign out</a></li>";   
       echo " </ul>";   
       echo "</div>";
   }
   ?>

        <!-- /panel -->


        <div data-role="content">

            <div class="ui-grid-b" id="top-image">
                <div class="ui-block-e" id="topbar" style="width: 30% !important; "></div>
                <div class="ui-block-e" id="topbar" style="width: 40% !important; "><img src="images/shop_1.png "></div>
                <div class="ui-block-e" id="topbar" style="width: 30% !important; "></div>
            </div>



            <li id="href-products" class="product1">

                <div class="ui-grid-a" id="bottom-points" style="margin-top:4px; background-color: rgb(255, 255, 255);vertical-align: middle;">
                    <div class="ui-block-e" style="width: 30% !important;height:100% !important;"><img src="images/product_1.jpg"></div>

                    <div class="ui-block-e" style="width: 70% !important; ">
                        <div class="ui-grid-a" id="bottom-points">

                            <div class="ui-block-e" id="list-heading" style="width: 80% !important;font-size: 16px; font-weight: bold;max-lines: 3;height: 30px;margin-top: 10px;margin-left: 10px">ACQUA DI GIÒ ABSOLU</div>
                            <div class="ui-block-e" id="like-btn" style="font-weight: bold;">
                                $52

                            </div>
                        </div>
                        <div class="ui-grid-solo">
                            <div class="ui-grid-a" id="bottom-points">
                                <div class="ui-block-e" id="list-desc" style="width: 50% !important; height: 20%;margin-left: 10px">
                                    Introducing ACQUA DI GIÒ ABSOLU fragrance for men

                                </div>

                                <div class="ui-block-e" style="width: 30% !important; text-align: right:48px;  ">QTY<nbsb>1</span></div>

                            </div>
                        </div>

                        <div class="ui-grid-solo">
                            <div class="ui-grid-a" id="bottom-points">
                                <div class="ui-block-e" style="width: 55% !important; font-size: 12px;margin-left: 10px">
                                    You ll earn 50 pts</div>
                                <div class="ui-block-e" style="width: 3% !important; "></div>
                                <div class="ui-block-e" style="width: 15% !important; ">
                                    <button class="ui-btn ui-btn-inline" style="width:100px;font-weight: bold;font-size: 14px;vertical-align: middle; background-color:#DA1F1F; color: white;text-shadow: none; ">Remove
                                            </button>
                                </div>
                            </div>


                        </div>
                    </div>

            </li>

            <li id="href-products" class="product1">

                <div class="ui-grid-a" id="bottom-points" style="margin-top:4px; background-color: rgb(255, 255, 255);vertical-align: middle;">
                    <div class="ui-block-e" style="width: 30% !important;height:100% !important;"><img src="images/product_1.jpg"></div>

                    <div class="ui-block-e" style="width: 70% !important; ">
                        <div class="ui-grid-a" id="bottom-points">

                            <div class="ui-block-e" id="list-heading" style="width: 80% !important;font-size: 16px; font-weight: bold;max-lines: 3;height: 30px;margin-top: 10px;margin-left: 10px">ACQUA DI GIÒ ABSOLU</div>
                            <div class="ui-block-e" id="like-btn" style="font-weight: bold;">
                                $52

                            </div>
                        </div>
                        <div class="ui-grid-solo">
                            <div class="ui-grid-a" id="bottom-points">
                                <div class="ui-block-e" id="list-desc" style="width: 50% !important; height: 20%;margin-left: 10px">
                                    Introducing ACQUA DI GIÒ ABSOLU fragrance for men

                                </div>

                                <div class="ui-block-e" style="width: 30% !important; text-align: right:48px;  ">QTY<nbsb>1</span></div>

                            </div>
                        </div>

                        <div class="ui-grid-solo">
                            <div class="ui-grid-a" id="bottom-points">
                                <div class="ui-block-e" style="width: 55% !important; font-size: 12px;margin-left: 10px">
                                    You ll earn 50 pts</div>
                                <div class="ui-block-e" style="width: 3% !important; "></div>
                                <div class="ui-block-e" style="width: 15% !important; margin-right: 60px;">
                                    <button class="ui-btn ui-btn-inline" style="width:100px;font-weight: bold;font-size: 14px;vertical-align: middle; background-color:#DA1F1F; color: white;text-shadow: none; padding-right: 100px;">Remove
                                            </button>
                                </div>
                            </div>


                        </div>
                    </div>

            </li>




            <div class="ui-grid-b">
                <div class="ui-block-e" style="width: 30% !important; "></div>
                <div class="ui-block-e" style="width: 40% !important; "></div>

                <div class="ui-block-e" style="width: 30% !important; "></div>
            </div>

            <div class="ui-grid-b" id="change-b">
                <div class="ui-block-e" style="width: 30% !important; text-align: center">Items</div>
                <div class="ui-block-e" style="width: 40% !important; text-align: center"></div>

                <div class="ui-block-e" style="width: 30% !important; text-align: center">2</div>
            </div>

            <div class="ui-grid-b" id="change-b">
                <div class="ui-block-e" style="width: 30% !important; text-align: center;font-weight: bold">Total</div>
                <div class="ui-block-e" style="width: 40% !important; text-align: center"></div>

                <div class="ui-block-e" style="width: 30% !important; text-align: center;font-weight: bold">$144</div>
            </div>


            <!-- Payment -->
            <form action="your-server-side-code" method="POST">
                <script src="https://checkout.stripe.com/checkout.js"></script>

                <a href="#" id="customButton" style="margin-bottom: 120px;" class="ui-btn ui-btn-b">Proceed to Checkout</a>

                <script type="text/javascript">
                     
                    var handler = StripeCheckout.configure({
                        key: 'pk_test_TYooMQauvdEDq54NiTphI7jx',
                        image: 'https://stripe.com/img/documentation/checkout/marketplace.png',
                        locale: 'auto',
                        token: function (token) {
                            // You can access the token ID with `token.id`.
                            // Get the token ID to your server-side code for use.
                            window.location.href = "http://localhost/London_Fashion_Week/ShoppingCartLast.php";
                        },
                        success: function (data) {
                            window.location.href = "http://localhost/London_Fashion_Week/ShoppingCartLast.php";
                        }
                    });

                    document.getElementById('customButton').addEventListener('click', function (e) {
                        var myvar='<?php echo $session_value;?>';                 
                        // Open Checkout with further options:
                        if(myvar){
                            handler.open({
                            name: 'Stripe.com',
                            description: '2 widgets',
                            zipCode: true,
                            amount: 2000
                        });
                        e.preventDefault();
                        }else{
                            window.location.href = "http://localhost/London_Fashion_Week/Login.php";
                        }
                       
                    });

                    // Close Checkout on page navigation:
                    window.addEventListener('popstate', function () {
                        handler.close();
                    });
                </script>
            </form>
            <!-- /payment -->
            </div>






            <!-- Footer -->
            <div data-role="footer" style="border-style: none;background-color: black;padding-top: 4px" data-position="relative">
                <div class="ui-grid">
                    <div class="ui-bar" style="height:20px;margin-top: 15px;">TEAM STYLEHUNT © 2018</div>
                </div>
                <div class="ui-grid-c">
                    <div class="ui-block-e" style="width: 16.66% !important; "></div>
                    <div class="ui-block-e" style="width: 16.66% !important; "><img src="images/facebook.png " width="25px " height="25px " alt="Facebook Logo "></div>
                    <div class="ui-block-e" style="width: 16.66% !important; "><img src="images/insta.png " width="25px " height="25px " alt="Insta Logo "></div>
                    <div class="ui-block-e" style="width: 16.66% !important; "><img src="images/twitter.png " width="25px " height="25px " alt="Twitter Logo "></div>
                    <div class="ui-block-e" style="width: 16.66% !important; "><img src="images/gplus.png " width="25px " height="25px " alt="Gplus Logo "></div>
                    <div class="ui-block-e" style="width: 16.66% !important; "></div>
                </div>
                <div class="ui-grid">
                    <div class="ui-bar" style="height:40px;margin-top: 7px">ALL RIGHTS RESERVED</div>
                </div>
            </div>
            <!-- /footer -->
            </div>
            <!-- /page -->

</body>

</html>