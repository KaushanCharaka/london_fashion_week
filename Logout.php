<?php
   session_start();
   
   if(session_destroy()) {
      // header("Location: login.php");
      header("Location: http://".$_SERVER['HTTP_HOST']."/London_Fashion_Week/index.php", true, 302);
   }
?>