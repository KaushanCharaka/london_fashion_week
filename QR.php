<?php
session_start();
?>
<!DOCTYPE html>
<html>

<head>
    <title>London Fashion Week</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />

    <link rel="stylesheet" href="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css" />
    <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script>
    <script src="https://rawgit.com/schmich/instascan-builds/master/instascan.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

   

        <style>
            .ui-grid-b {
                height: 80px !important;
            }
            
            img {
                max-width: 100%;
                max-height: 100%;
                display: block;
                margin-left: auto;
                margin-right: auto;
                margin-top: auto;
                margin-bottom: auto;
                align: center;
            }
            
            .portrait {
                height: 80px;
                width: 25px;
            }
            
            .landscape {
                height: 25px;
                width: 80px;
            }
            
            .square {
                height: 75px;
                width: 75px;
            }
            
            .ui-grid-c {
                background-color: black;
                border-style: none !important;
            }
            
            .ui-grid {
                background-color: black;
                border-style: none !important;
            }
            
            [data-role=page] {
                height: 100% !important;
                position: relative !important;
                font-family: "Source Sans Pro"
            }
            
            [data-role=header] {
                font-size: 25px;
                align-content: center;
            }
            
            [data-role=content] {
                height: 100%;
                margin: 0 auto;
                width: auto;
            }
            /* [data-role=footer] {
            position: relative !important;
            top: auto !important;
            width: 100%;
        } */
            
            [data-role=panel] {
                font-family: "Source Sans Pro";
                color: white;
            }
            
            .ui-block-a {
                margin-top: 5px;
                margin-bottom: 5px;
            }
            
            .ui-block-e {
                margin-top: 5px;
                margin-bottom: 5px;
                align: center;
            }
            
            .ui-block-b {
                margin-top: 15px;
                text-align: center;
                font-family: "Source Sans Pro";
                color: white;
                font-weight: normal;
                font-size: 18px;
                text-shadow: none;
            }
            
            .ui-bar {
                text-align: center;
                font-family: "Source Sans Pro";
                color: white;
                font-weight: normal;
                font-size: 12px;
                text-shadow: none;
                border-style: none !important;
            }
            
            .ui-block-c {
                margin-top: 16px;
            }
            
            .ui-block-d {
                margin-top: 18px;
            }
            
            body,
            input,
            select,
            textarea,
            button,
            .ui-btn {
                line-height: 1.3;
                font-family: "Source Sans Pro";
            }
            
            #search-basic {
                font-size: 24sp;
            }
            
            .ui-panel-position-right.ui-panel-display-reveal {
                -webkit-box-shadow: left;
            }
            
            .ui-btn-icon-left:after,
            .ui-btn-icon-right:after,
            .ui-btn-icon-top:after,
            .ui-btn-icon-bottom:after,
            .ui-btn-icon-notext:after {
                content: none;
            }
            
            .ui-icon-delete:after {
                background-color: black;
            }
            
            .ui-listview>li.ui-last-child>a.ui-btn {
                border-bottom-width: 0px;
            }
            
            .ui-page-theme-a .ui-btn {
                background-color: white;
            }
            
            .ui-panel-inner {
                background-color: white;
            }
            
            #wowslider-container1 {
                margin: 0px;
            }
            
            .ui-content {
                padding: 0px;
            }
            
            .ui-grid-solo {
                text-align: center;
                font-family: "Source Sans Pro";
            }
            
            #news-content {
                text-align: justify;
                margin-left: 20px;
                margin-right: 20px;
            }
            
            #center-button {
                margin: 0 auto;
                text-align: center !important;
                font-family: "Source Sans Pro";
                padding: 1px;
            }
            
            .ui-btn ui-btn-inline-place {
                margin-left: 1000px;
            }
            
            .morecontent span {
                display: none;
            }
            
            .morelink {
                display: block;
                text-align: center;
                font-family: "Source Sans Pro";
                font-size: 12px;
                font: black;
            }
            
            .content-right {
                margin-right: 550px;
            }
            
            .ui-block-a-news {
                margin-right: 0px;
            }
            
            #location {
                text-align: center;
                font-family: "Source Sans Pro" !important;
                font-weight: bold;
                background-color: unset;
            }
            
            #mapid {
                height: 250px;
            }
            
            .draggable {
                height: 100% !important;
            }
             .ui-page-theme-a .ui-btn.ui-btn-active{
            background-color: rgb(184, 197, 7);
        } 
        /*div {
    display: show;
}*/
        </style>
</head>

<body>

    <div data-role="page">

        <div data-role="header" data-position="fixed" style="border-style: none;">
            <div class="ui-grid-c">
                <div class="ui-block-a" style="width: 30% !important; "><a href="http://localhost/London_Fashion_Week/product.php" data-transition="slide" data-ajax="false">
                <img src="images/back.png " width="50px " height="50px " alt="London Fashion Week Logo "></a></div>
                <div class="ui-block-b" style="width: 40% !important; ">Scan QR</div>
                <div class="ui-block-c" style="width: 15% !important; "><a href="http://localhost/London_Fashion_Week/ShoppingCart.php" data-transition="slide" data-ajax="false">
                <img src="images/cart.png " width="30px " height="27px " alt="Cart "></a></div>
                <div class="ui-block-d" style="width: 15% !important; ">
                    <a href="#nav-panel"><img src="images/hamburger.png " width="28px " height="23px " alt="Cart "></a>
                </div>
            </div>
            <!-- /grid-b -->
        </div>
        <!-- /header -->

        <?php          
   
        if(!isset($_SESSION['login_user'])){
            // header("Location: http://".$_SERVER['HTTP_HOST']."/London_Fashion_Week/Login.php", true, 302);
            echo "<div data-role=\"panel\" data-position=\"right\" data-display=\"reveal\" data-theme=\"c\" id=\"nav-panel\">";
            echo "<ul data-role=\"listview\">";
            echo "<li data-icon=\"delete\"><a href=\"#\" data-rel=\"close\">Menu</a></li>";    
            echo "<input type=\"search\" name=\"search\" data-corners=\"false\" id=\"search-basic\" value=\"\" />";    
            echo " <li><a href=\"http://localhost/London_Fashion_Week/Login.php\" data-transition=\"slide\" data-ajax=\"false\">Sign in</a></li>"; 
            echo " <li><a href=\"http://localhost/London_Fashion_Week/Register.php\" data-transition=\"slide\" data-ajax=\"false\">Register</a></li>";
            echo " <li><a href=\"http://localhost/London_Fashion_Week/index.php\" data-transition=\"slide\" data-ajax=\"false\">Home</a></li>"; 
            echo " <li><a href=\"http://localhost/London_Fashion_Week/all_news.php\" data-transition=\"slide\" data-ajax=\"false\">News</a></li>"; 
            echo " <li><a href=\"http://localhost/London_Fashion_Week/portfolio.php\" data-transition=\"slide\" data-ajax=\"false\">Portfolio</a></li>"; 
            echo " <li><a href=\"http://localhost/London_Fashion_Week/Events.php\" data-transition=\"slide\" data-ajax=\"false\">Events</a></li>"; 
            echo " <li><a href=\"http://localhost/London_Fashion_Week/Favorite_list.php\" data-transition=\"slide\" data-ajax=\"false\">Favorite List</a></li>"; 
            echo " <li><a href=\"http://localhost/London_Fashion_Week/ShoppingCart.php\" data-transition=\"slide\" data-ajax=\"false\">Shopping Cart</a></li>"; 
            echo " <li><a href=\"http://localhost/London_Fashion_Week/Point_of_Interest.php\" data-transition=\"slide\" data-ajax=\"false\">Point of Interest</a></li>"; 
            echo " <li><a href=\"http://localhost/London_Fashion_Week/gallery.php\" data-transition=\"slide\" data-ajax=\"false\">Gallery</a></li>";   
            echo " <li><a href=\"http://localhost/London_Fashion_Week/AboutUs.php\" data-transition=\"slide\" data-ajax=\"false\">About Us</a></li>";   
            echo " </ul>";   
            echo "</div>";
        }else{
            echo "<div data-role=\"panel\" data-position=\"right\" data-display=\"reveal\" data-theme=\"c\" id=\"nav-panel\">";
            echo "<ul data-role=\"listview\">";
            echo "<li data-icon=\"delete\"><a href=\"#\" data-rel=\"close\">Menu</a></li>";    
            echo "<input type=\"search\" name=\"search\" data-corners=\"false\" id=\"search-basic\" value=\"\" />";    
            echo " <li><a href=\"http://localhost/London_Fashion_Week/index.php\" data-transition=\"slide\" data-ajax=\"false\">Home</a></li>";             
            echo " <li><a href=\"http://localhost/London_Fashion_Week/MyAccount.php\" data-transition=\"slide\" data-ajax=\"false\">My Account</a></li>";
            echo " <li><a href=\"http://localhost/London_Fashion_Week/all_news.php\" data-transition=\"slide\" data-ajax=\"false\">News</a></li>"; 
            echo " <li><a href=\"http://localhost/London_Fashion_Week/portfolio.php\" data-transition=\"slide\" data-ajax=\"false\">Portfolio</a></li>"; 
            echo " <li><a href=\"http://localhost/London_Fashion_Week/Events.php\" data-transition=\"slide\" data-ajax=\"false\">Events</a></li>"; 
            echo " <li><a href=\"http://localhost/London_Fashion_Week/Favorite_list.php\" data-transition=\"slide\" data-ajax=\"false\">Favorite List</a></li>"; 
            echo " <li><a href=\"http://localhost/London_Fashion_Week/ShoppingCart.php\" data-transition=\"slide\" data-ajax=\"false\">Shopping Cart</a></li>"; 
            echo " <li><a href=\"http://localhost/London_Fashion_Week/Point_of_Interest.php\" data-transition=\"slide\" data-ajax=\"false\">Point of Interest</a></li>"; 
            echo " <li><a href=\"http://localhost/London_Fashion_Week/gallery.php\" data-transition=\"slide\" data-ajax=\"false\">Gallery</a></li>";   
            echo " <li><a href=\"http://localhost/London_Fashion_Week/AboutUs.php\" data-transition=\"slide\" data-ajax=\"false\">About Us</a></li>";    
            echo " <li><a href=\"http://localhost/London_Fashion_Week/Logout.php\" data-transition=\"slide\" data-ajax=\"false\">Sign out</a></li>";   
            echo " </ul>";   
            echo "</div>";
        }
        ?>
        <!-- /panel -->

        <!-- Slider -->
        <div data-role="content">

            <!-- News -->
            <div class="ui-grid-solo">

                <div class="ui-block-a-news" style="font-size:26px;">Scan The QR</div>


            </div>

            <video id="preview"></video>
            <script type="text/javascript">
              let scanner = new Instascan.Scanner({ video: document.getElementById('preview') });
              scanner.addListener('scan', function (content) {
                alert(content);
              });
              Instascan.Camera.getCameras().then(function (cameras) {
                if (cameras.length > 0) {
                  scanner.start(cameras[0]);
                } else {
                  console.error('No cameras found.');
                }
              }).catch(function (e) {
                console.error(e);
              });
            </script>


        </div>

        <!-- Footer -->
        <div data-role="footer" style="border-style: none;background-color: black;padding-top: 4px;margin-top: 227px;" data-position="relative">
            <div class="ui-grid">
                <div class="ui-bar" style="height:20px;margin-top: 15px;">TEAM STYLEHUNT © 2018</div>
            </div>
            <div class="ui-grid-c">
                <div class="ui-block-e" style="width: 16.66% !important; "></div>
                <div class="ui-block-e" style="width: 16.66% !important; "><img src="images/facebook.png " width="25px " height="25px " alt="Facebook Logo "></div>
                <div class="ui-block-e" style="width: 16.66% !important; "><img src="images/insta.png " width="25px " height="25px " alt="Insta Logo "></div>
                <div class="ui-block-e" style="width: 16.66% !important; "><img src="images/twitter.png " width="25px " height="25px " alt="Twitter Logo "></div>
                <div class="ui-block-e" style="width: 16.66% !important; "><img src="images/gplus.png " width="25px " height="25px " alt="Gplus Logo "></div>
                <div class="ui-block-e" style="width: 16.66% !important; "></div>
            </div>
            <div class="ui-grid">
                <div class="ui-bar" style="height:40px;margin-top: 7px">ALL RIGHTS RESERVED</div>
            </div>
        </div>
        <!-- /footer -->
    </div>
    <!-- /page -->


</body>

<script type="text/javascript">
    $('#qr-canvas').WebCodeCam({
        ReadQRCode: true, // false or true
        ReadBarcode: true, // false or true
        width: 320,
        height: 240,
        videoSource: {
            id: true,      //default Videosource
            maxWidth: 640, //max Videosource resolution width
            maxHeight: 480 //max Videosource resolution height
        },
        flipVertical: false,  // false or true
        flipHorizontal: false,  // false or true
        zoom: -1, // if zoom = -1, auto zoom for optimal resolution else int
        beep: "js/beep.mp3", // string, audio file location
        autoBrightnessValue: false, // functional when value autoBrightnessValue is int
        brightness: 0, // int 
        grayScale: false, // false or true
        contrast: 0, // int 
        threshold: 0, // int 
        sharpness: [], //or matrix, example for sharpness ->  [0, -1, 0, -1, 5, -1, 0, -1, 0]
        resultFunction: function (resText, lastImageSrc) {
            //    resText as decoded code, lastImageSrc as image source
            example:
            alert(resText);

        },
        getUserMediaError: function () {
            //  callback funtion to getUserMediaError
            example:
            alert('Sorry, the browser you are using doesn\'t support getUserMedia');

        },
        cameraError: function (error) {
            /* callback funtion to cameraError, 
            example:
            var p, message = 'Error detected with the following parameters:\n';
            for (p in error) {
                    message += p + ': ' + error[p] + '\n';
            }
            alert(message);
            */
        }
    });

    $('#qr-canvas').WebCodeCam();
</script>

</html>