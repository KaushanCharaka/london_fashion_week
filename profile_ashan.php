<?php
session_start();
?>
<!DOCTYPE html>
<html>

<head>
    <title>London Fashion Week</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />

    <link rel="stylesheet" href="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css" />
    <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script>
    <!-- Top Slider -->
    <link rel="stylesheet" type="text/css" href="engine1/style.css" />
    <script type="text/javascript" src="engine1/jquery.js"></script>
    <!-- Sliders -->
    <link rel="stylesheet" type="text/css" href="slick-1.8.1/slick/slick.css">
    <link rel="stylesheet" type="text/css" href="slick-1.8.1/slick/slick-theme.css">

    <!-- For Map -->
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.4.0/dist/leaflet.css" integrity="sha512-puBpdR0798OZvTTbP4A8Ix/l+A4dHDD0DGqYW6RQ+9jxkRFclaxxQb/SJAWZfWAkuyeQUytO7+7N4QKrDh+drA=="
        crossorigin="" />

    <script src="https://unpkg.com/leaflet@1.4.0/dist/leaflet.js" integrity="sha512-QVftwZFqvtRNi0ZyCtsznlKSWOStnDORoefr1enyq5mVL4tmKB3S/EnC3rRJcxCPavG10IcrVGSmPh6Qw5lwrg=="
        crossorigin=""></script>

    <style>
        .ui-grid-b {
            height: 80px !important;
        }
        
        img {
            max-width: 45%;
            max-height: 50%;
            display: block;
            margin-left: auto;
            margin-right: auto;
            margin-top: auto;
            margin-bottom: auto;
            align: center;
        }
        
        .portrait {
            height: 80px;
            width: 25px;
        }
        
        .landscape {
            height: 25px;
            width: 80px;
        }
        
        .square {
            height: 75px;
            width: 75px;
        }
        
        .ui-grid-c {
            background-color: black;
            border-style: none !important;
        }
        
        .ui-grid {
            background-color: black;
            border-style: none !important;
        }
        
        [data-role=page] {
            height: 100% !important;
            position: relative !important;
            font-family: "Source Sans Pro"
        }
        
        [data-role=header] {
            font-size: 25px;
            align-content: center;
        }

        [data-role=content] {
            height: 100%;
            margin: 0 auto;
            width: auto;
        }
        
        /* [data-role=footer] {
            position: relative !important;
            top: auto !important;
            width: 100%;
        } */
        
        [data-role=panel] {
            font-family: "Source Sans Pro";
            color: white;
        }
        
        .ui-block-a {
            margin-top: 5px;
            margin-bottom: 5px;
        }
        
        .ui-block-e {
            margin-top: 5px;
            margin-bottom: 5px;
            align: center;
        }
        
        .ui-block-b {
            margin-top: 15px;
            text-align: center;
            font-family: "Source Sans Pro";
            color: white;
            font-weight: normal;
            font-size: 18px;
            text-shadow: none;
        }
        
        .ui-bar {
            text-align: center;
            font-family: "Source Sans Pro";
            color: white;
            font-weight: normal;
            font-size: 12px;
            text-shadow: none;
            border-style: none !important;
        }
        
        .ui-block-c {
            margin-top: 16px;
        }
        
        .ui-block-d {
            margin-top: 18px;
        }
        
        body,
        input,
        select,
        textarea,
        button,
        .ui-btn {
            line-height: 1.3;
            font-family: "Source Sans Pro";
        }
        
        #search-basic {
            font-size: 24sp;
        }
        
        .ui-panel-position-right.ui-panel-display-reveal {
            -webkit-box-shadow: left;
        }
        
        .ui-btn-icon-left:after,
        .ui-btn-icon-right:after,
        .ui-btn-icon-top:after,
        .ui-btn-icon-bottom:after,
        .ui-btn-icon-notext:after {
            content: none;
        }
        
        .ui-icon-delete:after {
            background-color: black;
        }
        
        .ui-listview>li.ui-last-child>a.ui-btn {
            border-bottom-width: 0px;
        }
        
        .ui-page-theme-a .ui-btn {
            background-color: white;
        }
        
        .ui-panel-inner {
            background-color: white;
        }
        #wowslider-container1{
            margin: 0px;
        }
        .ui-content{
            padding: 0px;
        }
        .ui-grid-solo{
            text-align: center;
            font-family: "Source Sans Pro";

        }
        
        #news-content{
            text-align: justify;
            margin-left: 20px;
            margin-right: 20px;
        }
        #center-button{
            margin: 0 auto;
            text-align: center;
            font-family: "Source Sans Pro";
            padding: 1px;
        }

        .morecontent span {
            display: none;
        }
        
        .morelink {
            display: block;
            text-align: center;
            font-family: "Source Sans Pro";
            font-size: 12px;
            font: black;
        }

        #mapid { 
            height: 250px; 
            }

    </style>
</head>

<body>

    <div data-role="page">

        <div data-role="header" data-position="fixed" style="border-style: none;">
            <div class="ui-grid-c">
                <div class="ui-block-a" style="width: 30% !important; "><a href="http://localhost/London_Fashion_Week/AboutUs.php" data-transition="slide" data-ajax="false">
                <img src="images/back.png " width="50px " height="50px " alt="London Fashion Week Logo "></a></div>
                <div class="ui-block-b" style="width: 40% !important; ">Profile </div>
                <div class="ui-block-c" style="width: 15% !important; "><a href="http://localhost/London_Fashion_Week/ShoppingCart.php" data-transition="slide" data-ajax="false">
                <img src="images/cart.png " width="30px " height="27px " alt="Cart "></a></div>
                <div class="ui-block-d" style="width: 15% !important; ">
                    <a href="#nav-panel"><img src="images/hamburger.png " width="28px " height="23px " alt="Cart "></a>
                </div>
            </div>
            <!-- /grid-b -->
        </div>
        <!-- /header -->

        <?php          
   
        if(!isset($_SESSION['login_user'])){
            // header("Location: http://".$_SERVER['HTTP_HOST']."/London_Fashion_Week/Login.php", true, 302);
            echo "<div data-role=\"panel\" data-position=\"right\" data-display=\"reveal\" data-theme=\"c\" id=\"nav-panel\">";
            echo "<ul data-role=\"listview\">";
            echo "<li data-icon=\"delete\"><a href=\"#\" data-rel=\"close\">Menu</a></li>";    
            echo "<input type=\"search\" name=\"search\" data-corners=\"false\" id=\"search-basic\" value=\"\" />";    
            echo " <li><a href=\"http://localhost/London_Fashion_Week/Login.php\" data-transition=\"slide\" data-ajax=\"false\">Sign in</a></li>"; 
            echo " <li><a href=\"http://localhost/London_Fashion_Week/Register.php\" data-transition=\"slide\" data-ajax=\"false\">Register</a></li>";
            echo " <li><a href=\"http://localhost/London_Fashion_Week/index.php\" data-transition=\"slide\" data-ajax=\"false\">Home</a></li>"; 
            echo " <li><a href=\"http://localhost/London_Fashion_Week/all_news.php\" data-transition=\"slide\" data-ajax=\"false\">News</a></li>"; 
            echo " <li><a href=\"http://localhost/London_Fashion_Week/portfolio.php\" data-transition=\"slide\" data-ajax=\"false\">Portfolio</a></li>"; 
            echo " <li><a href=\"http://localhost/London_Fashion_Week/Events.php\" data-transition=\"slide\" data-ajax=\"false\">Events</a></li>"; 
            echo " <li><a href=\"http://localhost/London_Fashion_Week/Favorite_list.php\" data-transition=\"slide\" data-ajax=\"false\">Favorite List</a></li>"; 
            echo " <li><a href=\"http://localhost/London_Fashion_Week/ShoppingCart.php\" data-transition=\"slide\" data-ajax=\"false\">Shopping Cart</a></li>"; 
            echo " <li><a href=\"http://localhost/London_Fashion_Week/Point_of_Interest.php\" data-transition=\"slide\" data-ajax=\"false\">Point of Interest</a></li>"; 
            echo " <li><a href=\"http://localhost/London_Fashion_Week/gallery.php\" data-transition=\"slide\" data-ajax=\"false\">Gallery</a></li>";   
            echo " <li><a href=\"http://localhost/London_Fashion_Week/AboutUs.php\" data-transition=\"slide\" data-ajax=\"false\">About Us</a></li>";   
            echo " </ul>";   
            echo "</div>";
        }else{
            echo "<div data-role=\"panel\" data-position=\"right\" data-display=\"reveal\" data-theme=\"c\" id=\"nav-panel\">";
            echo "<ul data-role=\"listview\">";
            echo "<li data-icon=\"delete\"><a href=\"#\" data-rel=\"close\">Menu</a></li>";    
            echo "<input type=\"search\" name=\"search\" data-corners=\"false\" id=\"search-basic\" value=\"\" />";    
            echo " <li><a href=\"http://localhost/London_Fashion_Week/index.php\" data-transition=\"slide\" data-ajax=\"false\">Home</a></li>";             
            echo " <li><a href=\"http://localhost/London_Fashion_Week/MyAccount.php\" data-transition=\"slide\" data-ajax=\"false\">My Account</a></li>";
            echo " <li><a href=\"http://localhost/London_Fashion_Week/all_news.php\" data-transition=\"slide\" data-ajax=\"false\">News</a></li>"; 
            echo " <li><a href=\"http://localhost/London_Fashion_Week/portfolio.php\" data-transition=\"slide\" data-ajax=\"false\">Portfolio</a></li>"; 
            echo " <li><a href=\"http://localhost/London_Fashion_Week/Events.php\" data-transition=\"slide\" data-ajax=\"false\">Events</a></li>"; 
            echo " <li><a href=\"http://localhost/London_Fashion_Week/Favorite_list.php\" data-transition=\"slide\" data-ajax=\"false\">Favorite List</a></li>"; 
            echo " <li><a href=\"http://localhost/London_Fashion_Week/ShoppingCart.php\" data-transition=\"slide\" data-ajax=\"false\">Shopping Cart</a></li>"; 
            echo " <li><a href=\"http://localhost/London_Fashion_Week/Point_of_Interest.php\" data-transition=\"slide\" data-ajax=\"false\">Point of Interest</a></li>"; 
            echo " <li><a href=\"http://localhost/London_Fashion_Week/gallery.php\" data-transition=\"slide\" data-ajax=\"false\">Gallery</a></li>";   
            echo " <li><a href=\"http://localhost/London_Fashion_Week/AboutUs.php\" data-transition=\"slide\" data-ajax=\"false\">About Us</a></li>";    
            echo " <li><a href=\"http://localhost/London_Fashion_Week/Logout.php\" data-transition=\"slide\" data-ajax=\"false\">Sign out</a></li>";   
            echo " </ul>";   
            echo "</div>";
        }
        ?>

        <!-- /panel -->

        <!-- Slider -->
        <div data-role="content">
          

            <!-- Profile Info -->
            <div class="ui-grid-solo">
                    <div class="ui-block-a" style="font-size:26px;"></div>
            </div>

            <section class="one-time slider" style="background-color:rgb(243, 243, 243);">
                <div>
                    <img src="images/ashan.gif">
                    <!--<img src="images/News_1.jpg">-->
                    <div id="name">
                        <center>
                            <h2>Ashan De Vaas</h2>
                        </center>
                    </div>
                    <div id="position">
                        <center>
                            <h3>Co-Founder</h3>
                        </center>
                    </div>
                   
                </div>




            </section>

            <div id="news-content">

                    <p class="more" style="margin-bottom:0px;">Despite rumors that they are together and even after
                        gushing about Nick Jonas, Madeline Brewer still insists
                        that they are not in fact romantically involved. After Nick and Madeline were reportedly
                        spotted on a
                        dinner date together, she decided to address the romance speculations. But when she tried
                        to put the
                        rumors to rest’</p> 
                </div>


            <script src="https://code.jquery.com/jquery-2.2.0.min.js" type="text/javascript"></script>
            <script src="slick-1.8.1/slick/slick.js" type="text/javascript" charset="utf-8"></script>
            <script type="text/javascript">
                $(document).on('ready', function () {
                    $(".one-time").slick({
                        dots: true,
                        infinite: true,
                        speed: 300,
                        slidesToShow: 1,
                        adaptiveHeight: true
                    });
                    $(".regular").slick({
                        dots: true,
                        infinite: true,
                        slidesToShow: 4,
                        slidesToScroll: 3,
                        centerPadding: '60px'
                    });

                    // Configure/customize these variables.
                    var showChar = 100;  // How many characters are shown by default
                    var ellipsestext = "...";
                    var moretext = "Read More ";
                    var lesstext = "Read Less";


                    $('.more').each(function () {
                        var content = $(this).html();

                        if (content.length > showChar) {

                            var c = content.substr(0, showChar);
                            var h = content.substr(showChar, content.length - showChar);

                            var html = c + '<span class="moreellipses">' + ellipsestext + '&nbsp;</span><span class="morecontent" ><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';

                            $(this).html(html);
                        }

                    });

                    $(".morelink").click(function () {
                        if ($(this).hasClass("less")) {
                            $(this).removeClass("less");
                            $(this).html(moretext);
                        } else {
                            $(this).addClass("less");
                            $(this).html(lesstext);
                        }
                        $(this).parent().prev().toggle();
                        $(this).prev().toggle();
                        return false;
                    });

                    // Map functions    
                    var mymap = L.map('mapid').setView([51.505, -0.09], 13);
                    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
                        maxZoom: 18,
                        id: 'mapbox.streets',
                        accessToken: 'pk.eyJ1IjoibWVudXNoYTk1IiwiYSI6ImNqcXVha3Z0eTBpaHE0Mm4wMDFsaW5wZ2YifQ.8_IYx3kKCP3XGmWflEhyVA'
                    }).addTo(mymap);
                    var marker = L.marker([51.5, -0.09]).addTo(mymap);
                    marker.bindPopup("<b>Winchester</b><br>").openPopup();


            //         $('.back').click(function () {
            //     history.go(-1);
            // });
                });

            </script>

            <!-- Location -->
            <br>
            <div class="ui-grid-solo">

                <div class="ui-block-location" style="font-size:26px;">Location</div>
            </div>
            <div class="ui-grid-solo">
                <div id="mapid"></div>
            </div>

        </div>




        <!-- Footer -->
        <div data-role="footer" style="border-style: none;background-color: black;padding-top: 4px" data-position="relative">
            <div class="ui-grid">
                <div class="ui-bar" style="height:20px;margin-top: 15px;">TEAM STYLEHUNT © 2018</div>
            </div>
            <div class="ui-grid-c">
                <div class="ui-block-e" style="width: 16.66% !important; "></div>
                <div class="ui-block-e" style="width: 16.66% !important; "><img src="images/facebook.png " width="25px "
                        height="25px " alt="Facebook Logo "></div>
                <div class="ui-block-e" style="width: 16.66% !important; "><img src="images/insta.png " width="25px "
                        height="25px " alt="Insta Logo "></div>
                <div class="ui-block-e" style="width: 16.66% !important; "><img src="images/twitter.png " width="25px "
                        height="25px " alt="Twitter Logo "></div>
                <div class="ui-block-e" style="width: 16.66% !important; "><img src="images/gplus.png " width="25px "
                        height="25px " alt="Gplus Logo "></div>
                <div class="ui-block-e" style="width: 16.66% !important; "></div>
            </div>
            <div class="ui-grid">
                <div class="ui-bar" style="height:40px;margin-top: 7px">ALL RIGHTS RESERVED</div>
            </div>
        </div>
        <!-- /footer -->
    </div>
    <!-- /page -->

</body>

</html>