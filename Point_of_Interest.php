<?php
session_start();
?>
<!DOCTYPE html>
<html>

<head>

    <title>Page Title</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="http://code.jquery.com/mobile/1.4.2/jquery.mobile-1.4.2.min.css" />
    <script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
    <script src="http://code.jquery.com/mobile/1.4.2/jquery.mobile-1.4.2.min.js"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
    <link rel='stylesheet' href='http://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.3.0/css/font-awesome.min.css'>
    <link rel="stylesheet" type="text/css" href="css/PointOfInterest.css">

    <!-- Mobiscroll JS and CSS Includes -->
    <link rel="stylesheet" href="css/mobiscroll.jquery.min.css">
    <script src="js/mobiscroll.jquery.min.js"></script>

    <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
    <script src="js/PointOfInterest.js"></script>

    <style>
        img {
            max-width: 100%;
            max-height: 100%;
            display: block;
            
            align-content: center;
        }

        .ui-grid-b {
            height: 80px !important;
        }

        .search-container {
            margin-top: 25px;
            max-width: 100%;
            padding-left: 15px;
            padding-right: 15px;
        }


        [data-role=panel] {
            font-family: "Source Sans Pro";
            color: white;
        }

        .portrait {
            height: 80px;
            width: 25px;
        }

        .landscape {
            height: 25px;
            width: 80px;
        }

        .square {
            height: 75px;
            width: 75px;
        }

        .ui-grid-c {
            background-color: black;
            border-style: none !important;
        }

        [data-role=page] {
            height: 100% !important;
            position: relative !important;
            font-family: "Source Sans Pro"
        }

        [data-role=header] {
            font-size: 25px;
            align-content: center;
        }

        [data-role=footer] {
             bottom: 0;
             position: absolute !important;
             top: auto !important;
             width: 100%;
        }
        .ui-content{
            margin-bottom: 100px;
        }
        .ui-block-pl1 {
            /* margin-bottom: 10px; */
            padding: 1em;   
        }

        .ui-block-pl2 {
            /* margin-bottom: 10px;
            padding-left: 10px; */
            padding: 1em;         
            /* font-family: "Source Sans Pro";
            color: white;
            font-weight: normal;
            font-size: 18px;
            text-shadow: none; */
        }

        .ui-block-a-logo {
            margin-top: 5px;
            margin-bottom: 5px;
        }

        .ui-block-a-pagename {
                margin-top: 15px;
                text-align: center;
                font-family: "Source Sans Pro";
                color: white;
                font-weight: normal;
                font-size: 18px;
                text-shadow: none;
}
        .ui-block-a-carticon {
            margin-top: 16px;
        }

        .ui-block-a-hamburger {
            margin-top: 18px;           
        }

        .load_more_btn {
            padding-bottom: 20px;
            width: 175px;
        }

        .ui-bar {
            text-align: center;
            font-family: "Source Sans Pro";
            color: white;
            font-weight: normal;
            font-size: 12px;
            text-shadow: none;
            border-style: none !important;
        }

        #search-basic {
    font-size: 24sp;
}

.ui-panel-position-right.ui-panel-display-reveal {
    -webkit-box-shadow: none;
}

.menu_show {
    right:0!important;
}

    @media screen and (max-width: 425px) {
    header h1 {
    font-size: 16px;
    }
}
    @media screen and (max-width: 360px) {
    nav.menu {
        width: 100%;
        left: -100%; 
    }
}

    </style>
</head>

<body>
    <div data-role="page">
        <div data-role="header" style="border-style: none">
            <div class="ui-grid-c">
                <div class="ui-block-a ui-block-a-logo" style="width: 30% !important; "><img src="images/logo_black.png "
                        width="50px " height="50px " alt="London Fashion Week Logo "></div>
                <div class="ui-block-b ui-block-a-pagename" style="width: 40% !important; ">Point of Interest</div>
                <div class="ui-block-c ui-block-a-carticon" style="width: 15% !important; "><a href="http://localhost/London_Fashion_Week/ShoppingCart.php" data-transition="slide" data-ajax="false">
                <img src="images/cart.png " width="30px " height="27px " alt="Cart "></a></div>
                <div class="ui-block-d ui-block-a-hamburger" style="width: 15% !important; ">
                    <a href="#nav-panel"><img src="images/hamburger.png " width="28px " height="23px " alt="Cart "></a>
                </div>

            </div>
            <!-- /grid-b -->
        </div>
        <!-- /header -->

        <?php          
   
   if(!isset($_SESSION['login_user'])){
       // header("Location: http://".$_SERVER['HTTP_HOST']."/London_Fashion_Week/Login.php", true, 302);
       echo "<div data-role=\"panel\" data-position=\"right\" data-display=\"reveal\" data-theme=\"c\" id=\"nav-panel\">";
       echo "<ul data-role=\"listview\">";
       echo "<li data-icon=\"delete\"><a href=\"#\" data-rel=\"close\">Menu</a></li>";    
       echo "<input type=\"search\" name=\"search\" data-corners=\"false\" id=\"search-basic\" value=\"\" />";    
       echo " <li><a href=\"http://localhost/London_Fashion_Week/Login.php\" data-transition=\"slide\" data-ajax=\"false\">Sign in</a></li>"; 
       echo " <li><a href=\"http://localhost/London_Fashion_Week/Register.php\" data-transition=\"slide\" data-ajax=\"false\">Register</a></li>";
       echo " <li><a href=\"http://localhost/London_Fashion_Week/index.php\" data-transition=\"slide\" data-ajax=\"false\">Home</a></li>"; 
       echo " <li><a href=\"http://localhost/London_Fashion_Week/all_news.php\" data-transition=\"slide\" data-ajax=\"false\">News</a></li>"; 
       echo " <li><a href=\"http://localhost/London_Fashion_Week/portfolio.php\" data-transition=\"slide\" data-ajax=\"false\">Portfolio</a></li>"; 
       echo " <li><a href=\"http://localhost/London_Fashion_Week/Events.php\" data-transition=\"slide\" data-ajax=\"false\">Events</a></li>"; 
       echo " <li><a href=\"http://localhost/London_Fashion_Week/Favorite_list.php\" data-transition=\"slide\" data-ajax=\"false\">Favorite List</a></li>"; 
       echo " <li><a href=\"http://localhost/London_Fashion_Week/ShoppingCart.php\" data-transition=\"slide\" data-ajax=\"false\">Shopping Cart</a></li>";  
       echo " <li><a href=\"http://localhost/London_Fashion_Week/gallery.php\" data-transition=\"slide\" data-ajax=\"false\">Gallery</a></li>";   
       echo " <li><a href=\"http://localhost/London_Fashion_Week/AboutUs.php\" data-transition=\"slide\" data-ajax=\"false\">About Us</a></li>";   
       echo " </ul>";   
       echo "</div>";
   }else{
       echo "<div data-role=\"panel\" data-position=\"right\" data-display=\"reveal\" data-theme=\"c\" id=\"nav-panel\">";
       echo "<ul data-role=\"listview\">";
       echo "<li data-icon=\"delete\"><a href=\"#\" data-rel=\"close\">Menu</a></li>";    
       echo "<input type=\"search\" name=\"search\" data-corners=\"false\" id=\"search-basic\" value=\"\" />";    
       echo " <li><a href=\"http://localhost/London_Fashion_Week/index.php\" data-transition=\"slide\" data-ajax=\"false\">Home</a></li>";             
       echo " <li><a href=\"http://localhost/London_Fashion_Week/MyAccount.php\" data-transition=\"slide\" data-ajax=\"false\">My Account</a></li>";
       echo " <li><a href=\"http://localhost/London_Fashion_Week/all_news.php\" data-transition=\"slide\" data-ajax=\"false\">News</a></li>"; 
       echo " <li><a href=\"http://localhost/London_Fashion_Week/portfolio.php\" data-transition=\"slide\" data-ajax=\"false\">Portfolio</a></li>"; 
       echo " <li><a href=\"http://localhost/London_Fashion_Week/Events.php\" data-transition=\"slide\" data-ajax=\"false\">Events</a></li>"; 
       echo " <li><a href=\"http://localhost/London_Fashion_Week/Favorite_list.php\" data-transition=\"slide\" data-ajax=\"false\">Favorite List</a></li>"; 
       echo " <li><a href=\"http://localhost/London_Fashion_Week/ShoppingCart.php\" data-transition=\"slide\" data-ajax=\"false\">Shopping Cart</a></li>";  
       echo " <li><a href=\"http://localhost/London_Fashion_Week/gallery.php\" data-transition=\"slide\" data-ajax=\"false\">Gallery</a></li>";   
       echo " <li><a href=\"http://localhost/London_Fashion_Week/AboutUs.php\" data-transition=\"slide\" data-ajax=\"false\">About Us</a></li>";    
       echo " <li><a href=\"http://localhost/London_Fashion_Week/Logout.php\" data-transition=\"slide\" data-ajax=\"false\">Sign out</a></li>";   
       echo " </ul>";   
       echo "</div>";
   }
   ?>
        <!-- /panel -->

        <div data-role="main" class="ui-content">
            <!-- /grid-a -->
            <div class="ui-grid-a">
                <div class="ui-block-a ui-block-pl1">                 
                    <!-- <div class="ui-bar ui-bar-a" style="height:200px">  -->
                    <!-- <img src="data1/images/v1.jpg" alt="Sample photo"> -->
                    <!-- </div> -->
                    
                    <a href="localhost/london_fashion_week/PointOfInterest_SingleView.php" id="single" onclick="document.location='http://localhost/London_Fashion_Week/PointOfInterest_SingleView.php';return false;" >
                    <img src="data1/images/v1.jpg" alt="single" class="thumbnails" />
                    </a>

                    <h3>Paris Fashion Mall</h3>

                    <p> As the final week of a long and busy month of shows, Paris Fashion Week is up there as one of our favourites.
                    </p>
                    <section class='rating-widget'>
                        <!-- Rating Stars Box -->
                        <div class='rating-stars text-center'>
                            <ul id='stars'>
                                <li class='star' title='Poor' data-value='1'>
                                    <i class='fa fa-star fa-fw'></i>
                                </li>
                                <li class='star' title='Fair' data-value='2'>
                                    <i class='fa fa-star fa-fw'></i>
                                </li>
                                <li class='star' title='Good' data-value='3'>
                                    <i class='fa fa-star fa-fw'></i>
                                </li>
                                <li class='star' title='Excellent' data-value='4'>
                                    <i class='fa fa-star fa-fw'></i>
                                </li>
                                <li class='star' title='WOW!!!' data-value='5'>
                                    <i class='fa fa-star fa-fw'></i>
                                </li>
                            </ul>
                        </div>
                    </section>

                </div>

                <div class="ui-block-b ui-block-pl2">
                    <!-- <div class="ui-bar ui-bar-a" style="height:200px"> -->
                    <img src="data1/images/v2.jpg" alt="Sample photo">
                    <!-- </div> -->

                    <h3>German Fashion City</h3>

                    <p> It is the rise in spectacle that has become a new staple of the seasons for modles at germem fashion city.   
                    </p>
                    <section class='rating-widget'>
                        <!-- Rating Stars Box -->
                        <div class='rating-stars text-center'>
                            <ul id='stars'>
                                <li class='star' title='Poor' data-value='1'>
                                    <i class='fa fa-star fa-fw'></i>
                                </li>
                                <li class='star' title='Fair' data-value='2'>
                                    <i class='fa fa-star fa-fw'></i>
                                </li>
                                <li class='star' title='Good' data-value='3'>
                                    <i class='fa fa-star fa-fw'></i>
                                </li>
                                <li class='star' title='Excellent' data-value='4'>
                                    <i class='fa fa-star fa-fw'></i>
                                </li>
                                <li class='star' title='WOW!!!' data-value='5'>
                                    <i class='fa fa-star fa-fw'></i>
                                </li>
                            </ul>
                        </div>
                    </section>

                </div>
                <div class="ui-block-a ui-block-pl1">

                    <!-- <div class="ui-bar ui-bar-a" style="height:200px">  -->
                    <img src="data1/images/v3.jpg" alt="Sample photo">
                    <!-- </div> -->

                    <h3>High Tech Venue</h3>

                    <p> Professional Sound, Lighting, Acrylic stage, Led Screen, Projectors are placed for high level modeling shhows.
                    </p>
                    <section class='rating-widget'>
                        <!-- Rating Stars Box -->
                        <div class='rating-stars text-center'>
                            <ul id='stars'>
                                <li class='star' title='Poor' data-value='1'>
                                    <i class='fa fa-star fa-fw'></i>
                                </li>
                                <li class='star' title='Fair' data-value='2'>
                                    <i class='fa fa-star fa-fw'></i>
                                </li>
                                <li class='star' title='Good' data-value='3'>
                                    <i class='fa fa-star fa-fw'></i>
                                </li>
                                <li class='star' title='Excellent' data-value='4'>
                                    <i class='fa fa-star fa-fw'></i>
                                </li>
                                <li class='star' title='WOW!!!' data-value='5'>
                                    <i class='fa fa-star fa-fw'></i>
                                </li>
                            </ul>
                        </div>
                    </section>

                </div>
                <div class="ui-block-b ui-block-pl2">

                    <!-- <div class="ui-bar ui-bar-a" style="height:200px">  -->
                    <img src="data1/images/v4.jpg" alt="Sample photo">
                    <!-- </div> -->

                    <h3>Curbed NY</h3>

                    <p> Suffice to say, the fashion elite become more dexterous in their venture outside of their areas of comfort.
                    </p>
                    <section class='rating-widget'>
                        <!-- Rating Stars Box -->
                        <div class='rating-stars text-center'>
                            <ul id='stars'>
                                <li class='star' title='Poor' data-value='1'>
                                    <i class='fa fa-star fa-fw'></i>
                                </li>
                                <li class='star' title='Fair' data-value='2'>
                                    <i class='fa fa-star fa-fw'></i>
                                </li>
                                <li class='star' title='Good' data-value='3'>
                                    <i class='fa fa-star fa-fw'></i>
                                </li>
                                <li class='star' title='Excellent' data-value='4'>
                                    <i class='fa fa-star fa-fw'></i>
                                </li>
                                <li class='star' title='WOW!!!' data-value='5'>
                                    <i class='fa fa-star fa-fw'></i>
                                </li>
                            </ul>
                        </div>
                    </section>

                </div>
            </div>
            <!-- /grid-a -->

            <div id="images" class="ui-grid-a">

            </div>

            <Center>
                <div class="load_more_btn">
                    <button id="more" class="mbsc-btn-block">
                        <span class="md-btn-text">Load more</span>
                    </button>
                </div>
            </Center>

        </div>

        <!-- /content -->
        <div data-role="footer" style="border-style: none;background-color: black;">
            <div class="ui-grid">
                <div class="ui-bar" style="height:20px;margin-top: 15px;">TEAM STYLEHUNT © 2018</div>
            </div>
            <div class="ui-grid-c">
                <Center>
                    <div class="ui-block-e" style="width: 16.66% !important; "></div>
                    <div class="ui-block-e" style="width: 16.66% !important; "><img src="images/facebook.png " width="25px "
                            height="25px " alt="Facebook Logo "></div>
                    <div class="ui-block-e" style="width: 16.66% !important; "><img src="images/insta.png " width="25px "
                            height="25px " alt="Insta Logo "></div>
                    <div class="ui-block-e" style="width: 16.66% !important; "><img src="images/twitter.png " width="25px "
                            height="25px " alt="Twitter Logo "></div>
                    <div class="ui-block-e" style="width: 16.66% !important; "><img src="images/gplus.png " width="25px "
                            height="25px " alt="Gplus Logo "></div>
                    <div class="ui-block-e" style="width: 16.66% !important; "></div>
                </Center>
            </div>
            <div class="ui-grid">
                <div class="ui-bar" style="height:40px;margin-top: 7px">ALL RIGHTS RESERVED</div>
            </div>
        </div>
        <!-- /footer -->
    </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            var imagesCount = 4;


            $('#more').click(function () {

                var btnMore = $('#more');

                imagesCount += 4;
                if (imagesCount == 16) {
                    btnMore.hide();
                }

                var title = "<h4>Venue of Fashion Mall</h4>"
                var img1 = "\"data1/images/img_slider_1.jpg\"";
                var img2 = "\"data1/images/img2.jpg\"";
                var img3 = "\"data1/images/img3.jpg\"";
                var img4 = "\"data1/images/img4.jpg\"";
                var para = "<p> Every year, New York, London, Milan and (especially) Paris are the capitals of fashion for a few weeks. The greatest designers and upcoming stars of tomorrow present their new men's, women's, ready-to-wear and Haute-Couture collections to the world. </p>";
                var myDivElement = "<div class='rating-stars text-center'>" +
                    "<ul id='stars'>" +
                    " <li class='star' title='Poor' data-value='1'>" +
                    "<i class='fa fa-star fa-fw'></i>" +
                    " </li>" +
                    " <li class='star' title='Fair' data-value='2'>" +
                    "<i class='fa fa-star fa-fw'></i>" +
                    " </li>" +
                    " <li class='star' title='Good' data-value='3'> " +
                    " <i class='fa fa-star fa-fw'></i> " +
                    " </li> " +
                    " <li class='star' title='Excellent' data-value='4'> " +
                    " <i class='fa fa-star fa-fw'></i> " +
                    " </li> " +
                    " <li class='star' title='WOW!!!' data-value='5'> " +
                    " <i class='fa fa-star fa-fw'></i> " +
                    " </li> " +
                    " </ul> " +
                    "</div>";


                $('#images').append('<div class=\"ui-block-a ui-block-pl1\">'

                    + '<img src= ' + img1 + ' alt="Sample photo">'

                    + title + para + myDivElement + '</div>' +
                    '<div class=\"ui-block-b ui-block-pl2\">'

                    + '<img src= ' + img2 + ' alt="Sample photo">'

                    + title + para + myDivElement + '</div>' +
                    '<div class=\"ui-block-a ui-block-pl1\">'

                    + '<img src= ' + img3 + ' alt="Sample photo">'

                    + title + para + myDivElement + '</div>' +
                    '<div class=\"ui-block-b ui-block-pl2\">'

                    + '<img src= ' + img4 + ' alt="Sample photo">'

                    + title + para + myDivElement + '</div>');

            });

        });

    </script>
</body>

</html>