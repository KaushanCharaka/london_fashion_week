<?php
session_start();
?>
<!DOCTYPE html>
<html>

<head>
    <title>London Fashion Week</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css" />
    <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script>


    <style>
        .ui-grid-b {
            height: 80px !important;
        }
        
        img {
            max-width: 100%;
            max-height: 100%;
            display: block;
            margin-left: auto;
            margin-right: auto;
            margin-top: auto;
            margin-bottom: auto;
            align: center;
        }
        
        .portrait {
            height: 80px;
            width: 25px;
        }
        
        .landscape {
            height: 25px;
            width: 80px;
        }
        
        .square {
            height: 75px;
            width: 75px;
        }
        
        #header-grid {
            background-color: black;
            border-style: none !important;
        }
        
        .ui-grid {
            background-color: black;
            border-style: none !important;
        }
        
        [data-role=page] {
            height: 100% !important;
            position: relative !important;
            font-family: "Source Sans Pro"
        }
        
        [data-role=header] {
            font-size: 25px;
            align-content: center;
        }

        [data-role=content] {
            height: 100%;
            margin: 0 auto;
            width: auto;
        }
        
        /* [data-role=footer] {
            position: relative !important;
            top: auto !important;
            width: 100%;
        } */
        
        [data-role=panel] {
            font-family: "Source Sans Pro";
            color: white;
        }
        
        .ui-block-a {
            margin-top: 5px;
            margin-bottom: 5px;
        }
        
        .ui-block-e {
            margin-top: 0px;
            margin-bottom: 0px;
            align: center;
        }
        
        .ui-block-b {
            margin-top: 15px;
            text-align: center;
            font-family: "Source Sans Pro";
            color: white;
            font-weight: normal;
            font-size: 18px;
            text-shadow: none;
        }
        
        .ui-bar {
            text-align: center;
            font-family: "Source Sans Pro";
            color: white;
            font-weight: normal;
            font-size: 12px;
            text-shadow: none;
            border-style: none !important;
        }
        
        .ui-block-c {
            margin-top: 16px;
        }
        
        .ui-block-d {
            margin-top: 18px;
        }
        
        body,
        input,
        select,
        textarea,
        button,
        .ui-btn {
            line-height: 1.3;
            font-family: "Source Sans Pro";
        }
        
        #search-basic {
            font-size: 24sp;
        }
        
        .ui-panel-position-right.ui-panel-display-reveal {
            -webkit-box-shadow: left;
        }
        
        .ui-btn-icon-left:after,
        .ui-btn-icon-right:after,
        .ui-btn-icon-top:after,
        .ui-btn-icon-bottom:after,
        .ui-btn-icon-notext:after {
            content: none;
        }
        
        .ui-icon-delete:after {
            background-color: black;
        }
        
        .ui-listview>li.ui-last-child>a.ui-btn {
            border-bottom-width: 0px;
        }
        
        .ui-page-theme-a .ui-btn {
            background-color: white;
        }
        
        .ui-panel-inner {
            background-color: white;
        }
        #wowslider-container1{
            margin: 0px;
        }
        .ui-content{
            padding: 0px;
        }
        .ui-grid-solo{
            text-align: center;
            font-family: "Source Sans Pro";

        }
        
        #news-content{
            text-align: justify;
            margin-left: 20px;
            margin-right: 20px;
        }
        #center-button{
            margin: 0 auto;
            text-align: center;
            font-family: "Source Sans Pro";
            padding: 1px;
        }
        li.ui-block-b.ui-state-default.ui-corner-top{
            margin-top: 0px;

        }
        li.ui-block-c.ui-state-default.ui-corner-top{
            margin-top: 0px;
        }
        li.ui-block-a.ui-state-default.ui-corner-top{
            margin-top: 0px;
        }
        li.ui-block-d.ui-state-default.ui-corner-top{
            margin-top: 0px;
        }

        /* [data-role="navbar"]{
            padding: 15px;
        } */
        .ui-mini {
            margin-left: 18px;
            margin-right: 18px;
            margin-top: 19px;
            margin-bottom: 0px;
        }
        .ui-page-theme-a .ui-btn.ui-btn-active{
            background-color: black;
            border-color:black;
            lighting-color: aliceblue 
        }
        .ui-input-text, .ui-input-search{
            margin-left: 18px;
            margin-right: 18px;
            margin-top: 18px;

        }
        #bottom-points{
            height: 0% !important;
            margin-top: 0px !important;

        }
        #href{
            padding: 7px;
        }

        #like-btn{
            align:right !important;
        }
        #list-heading{
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
            max-width: 75ch;
            font-family: "Source Sans Pro";
            font-size: 13px;

        }
        #list-desc{
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
            max-width: 175ch;
            font-family: "Source Sans Pro";
            font-size: 11px;
       
        }
        .ui-btn .ui-btn-inline{
            padding: 3px;
        }

         .heart {
            margin-left: 231px;
            margin-top: -27px;
            margin-bottom:-31px;
            width: 69px;
            height: 70px;
            position: relative;
            left: 50%;
            top: 50%;
            transform: translate(-50%, -50%);
            background: url(images/heart.png) no-repeat;  
            cursor: pointer;  
        }
            .heart-blast {
            background-position: -2800px 0;
            transition: background 1s steps(28);
        }
      
    </style>
</head>

<body>

    <div data-role="page">

        <div data-role="header" data-position="fixed" style="border-style: none;">
            <div class="ui-grid-c" id="header-grid">
                <div class="ui-block-a" style="width: 30% !important; "><img src="images/logo_black.png " width="50px "
                        height="50px " alt="London Fashion Week Logo "></div>
                <div class="ui-block-b" style="width: 40% !important; ">Events</div>
                <div class="ui-block-c" style="width: 15% !important; "><a href="http://localhost/London_Fashion_Week/ShoppingCart.php" data-transition="slide" data-ajax="false">
                <img src="images/cart.png " width="30px " height="27px " alt="Cart "></a></div>
                <div class="ui-block-d" style="width: 15% !important; ">
                    <a href="#nav-panel"><img src="images/hamburger.png " width="28px " height="23px " alt="Cart "></a>
                </div>
            </div>
            <!-- /grid-b -->
        </div>
        <!-- /header -->

        <?php          
   
   if(!isset($_SESSION['login_user'])){
       // header("Location: http://".$_SERVER['HTTP_HOST']."/London_Fashion_Week/Login.php", true, 302);
       echo "<div data-role=\"panel\" data-position=\"right\" data-display=\"reveal\" data-theme=\"c\" id=\"nav-panel\">";
       echo "<ul data-role=\"listview\">";
       echo "<li data-icon=\"delete\"><a href=\"#\" data-rel=\"close\">Menu</a></li>";    
       echo "<input type=\"search\" name=\"search\" data-corners=\"false\" id=\"search-basic\" value=\"\" />";    
       echo " <li><a href=\"http://localhost/London_Fashion_Week/Login.php\" data-transition=\"slide\" data-ajax=\"false\">Sign in</a></li>"; 
       echo " <li><a href=\"http://localhost/London_Fashion_Week/Register.php\" data-transition=\"slide\" data-ajax=\"false\">Register</a></li>";
       echo " <li><a href=\"http://localhost/London_Fashion_Week/index.php\" data-transition=\"slide\" data-ajax=\"false\">Home</a></li>"; 
       echo " <li><a href=\"http://localhost/London_Fashion_Week/all_news.php\" data-transition=\"slide\" data-ajax=\"false\">News</a></li>"; 
       echo " <li><a href=\"http://localhost/London_Fashion_Week/portfolio.php\" data-transition=\"slide\" data-ajax=\"false\">Portfolio</a></li>"; 
       echo " <li><a href=\"http://localhost/London_Fashion_Week/Favorite_list.php\" data-transition=\"slide\" data-ajax=\"false\">Favorite List</a></li>"; 
       echo " <li><a href=\"http://localhost/London_Fashion_Week/ShoppingCart.php\" data-transition=\"slide\" data-ajax=\"false\">Shopping Cart</a></li>"; 
       echo " <li><a href=\"http://localhost/London_Fashion_Week/Point_of_Interest.php\" data-transition=\"slide\" data-ajax=\"false\">Point of Interest</a></li>"; 
       echo " <li><a href=\"http://localhost/London_Fashion_Week/gallery.php\" data-transition=\"slide\" data-ajax=\"false\">Gallery</a></li>";   
       echo " <li><a href=\"http://localhost/London_Fashion_Week/AboutUs.php\" data-transition=\"slide\" data-ajax=\"false\">About Us</a></li>";   
       echo " </ul>";   
       echo "</div>";
   }else{
       echo "<div data-role=\"panel\" data-position=\"right\" data-display=\"reveal\" data-theme=\"c\" id=\"nav-panel\">";
       echo "<ul data-role=\"listview\">";
       echo "<li data-icon=\"delete\"><a href=\"#\" data-rel=\"close\">Menu</a></li>";    
       echo "<input type=\"search\" name=\"search\" data-corners=\"false\" id=\"search-basic\" value=\"\" />";    
       echo " <li><a href=\"http://localhost/London_Fashion_Week/index.php\" data-transition=\"slide\" data-ajax=\"false\">Home</a></li>";             
       echo " <li><a href=\"http://localhost/London_Fashion_Week/MyAccount.php\" data-transition=\"slide\" data-ajax=\"false\">My Account</a></li>";
       echo " <li><a href=\"http://localhost/London_Fashion_Week/all_news.php\" data-transition=\"slide\" data-ajax=\"false\">News</a></li>"; 
       echo " <li><a href=\"http://localhost/London_Fashion_Week/portfolio.php\" data-transition=\"slide\" data-ajax=\"false\">Portfolio</a></li>";
       echo " <li><a href=\"http://localhost/London_Fashion_Week/Favorite_list.php\" data-transition=\"slide\" data-ajax=\"false\">Favorite List</a></li>"; 
       echo " <li><a href=\"http://localhost/London_Fashion_Week/ShoppingCart.php\" data-transition=\"slide\" data-ajax=\"false\">Shopping Cart</a></li>"; 
       echo " <li><a href=\"http://localhost/London_Fashion_Week/Point_of_Interest.php\" data-transition=\"slide\" data-ajax=\"false\">Point of Interest</a></li>"; 
       echo " <li><a href=\"http://localhost/London_Fashion_Week/gallery.php\" data-transition=\"slide\" data-ajax=\"false\">Gallery</a></li>";   
       echo " <li><a href=\"http://localhost/London_Fashion_Week/AboutUs.php\" data-transition=\"slide\" data-ajax=\"false\">About Us</a></li>";    
       echo " <li><a href=\"http://localhost/London_Fashion_Week/Logout.php\" data-transition=\"slide\" data-ajax=\"false\">Sign out</a></li>";   
       echo " </ul>";   
       echo "</div>";
   }
   ?>

        <!-- /panel -->


        <div data-role="content" class="cont">
            <div class="ui-grid-a">
                <div data-role="navbar">
                    <ul>
                        <li style="margin-top: 0px;" class="rt1"><a href="#">All</a></li>
                        <li style="margin-top: 0px;" class="rt2"><a href="#">Cat Walks</a></li>
                        <li style="margin-top: 0px;" class="rt3"><a href="#">Shows</a></li>
                        <li style="margin-top: 0px;" class="rt4"><a href="#">Talks</a></li>
                    </ul>
                </div>

            </div>

            <ul data-role="listview" data-filter="true" data-divider-theme="b" data-filter-placeholder="Search Events"
                data-inset="true" id="eventsUl">

                <script>
                    $('.rt1 a').click(function () {
                        // $(this).closest('.cont').find('#href-cat').hide('slow');
                        // return false;
                        var input, filter, ul, li, a, i, txtValue;
                        ul = document.getElementById("eventsUl");
                        li = ul.getElementsByTagName("li");
                        for (i = 0; i < li.length; i++) {
                            li[i].style.display = "";
                        }

                    });

                    $('.rt2 a').click(function () {
                        // $(this).closest('.cont').find('#href-cat').hide('slow');
                        // return false;
                        var input, filter, ul, li, a, i, txtValue;
                        input = "href-cat";
                        filter = input;
                        ul = document.getElementById("eventsUl");
                        li = ul.getElementsByTagName("li");
                        for (i = 0; i < li.length; i++) {
                            if (li[i].id == filter) {
                                li[i].style.display = "";
                            } else {
                                li[i].style.display = "none";
                            }
                        }

                    });

                    $('.rt3 a').click(function () {
                        // $(this).closest('.cont').find('#href-cat').hide('slow');
                        // return false;
                        var input, filter, ul, li, a, i, txtValue;
                        input = "href-shows";
                        filter = input;
                        ul = document.getElementById("eventsUl");
                        li = ul.getElementsByTagName("li");
                        for (i = 0; i < li.length; i++) {
                            if (li[i].id == filter) {
                                li[i].style.display = "";
                            } else {
                                li[i].style.display = "none";
                            }
                        }

                    });

                    $('.rt4 a').click(function () {
                        // $(this).closest('.cont').find('#href-cat').hide('slow');
                        // return false;
                        var input, filter, ul, li, a, i, txtValue;
                        input = "href-talks";
                        filter = input;
                        ul = document.getElementById("eventsUl");
                        li = ul.getElementsByTagName("li");
                        for (i = 0; i < li.length; i++) {
                            if (li[i].id == filter) {
                                li[i].style.display = "";
                            } else {
                                li[i].style.display = "none";
                            }
                        }

                    });

                </script>
                <li id="href-cat" data-role="list-divider">Cat Walks</li>
                <li id="href-cat"><a href="#" id="href" class="href-cat">
                        <div class="ui-grid-a" id="bottom-points" style="margin-top:4px; background-color: rgb(255, 255, 255);vertical-align: middle;">
                            <div class="ui-block-e" style="width: 30% !important;height:100% !important;"><img src="images/event_1.png"></div>

                            <div class="ui-block-e" style="width: 70% !important; ">
                                <div class="ui-grid-a" id="bottom-points">

                                    <div class="ui-block-e" id="list-heading" style="margin-left: 10px; width: 80% !important;font-size: 16px; font-weight: bold;max-lines: 3;height: 30px;margin-top: 10px;">
                                        Heads to Make a Major Fashion Event a Happening</div>
                                    <div class="ui-block-e" id="like-btn" style="font-weight: bold;">
                                        <!--    <div class="heart"></div>  -->
                                            <div class="heart"></div> 
                                    </div>
                                </div>
                                <div class="ui-grid-a" id="bottom-points">
                                    <div class="ui-block-e" id="list-desc" style="width: 90% !important; height: 20%;margin-left: 10px;">
                                        A human resource plan for the backstage portion of a fashion show
                                        ensures every detail, large and small, <br>is successfully completed. A fashion
                                        show is a hub of activity with models, designers, stylists, volunteer, staff
                                        and support staff roaming the halls. <br>The most famous fashion shows are
                                        staged
                                        in the fashion hubs of Paris, New York and Milan. But hosting a fashion sh
                                    </div>
                                    <div class="ui-block-e" style="width: 10% !important; "></div>

                                </div>
                                <div class="ui-grid-a" id="bottom-points">
                                    <div class="ui-block-e" style="width: 55% !important; font-size: 24px;"></div>
                                    <div class="ui-block-e" style="width: 45% !important;font-style: italic;text-align: center;">
                                        <!-- <img src="images/like_fill.png " style="width:30px;height:30px;" alt="favorite Logo "> -->
                                        <button class="ui-btn ui-btn-inline cat-more-btn" style="width:100px;font-weight: bold;font-size: 14px;vertical-align: middle; text-shadow: none;margin-top:30px ">More</button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </a></li>
                <li id="href-cat"><a href="#" id="href" class="href-cat">
                        <div class="ui-grid-a" id="bottom-points" style="margin-top:4px; background-color: rgb(255, 255, 255);vertical-align: middle;">
                            <div class="ui-block-e" style="width: 30% !important;height:100% !important;"><img src="images/event_2.jpg"></div>

                            <div class="ui-block-e" style="width: 70% !important; ">
                                <div class="ui-grid-a" id="bottom-points">

                                    <div class="ui-block-e" id="list-heading" style="margin-left: 10px;width: 80% !important;font-size: 16px; font-weight: bold;max-lines: 3;height: 30px;margin-top: 10px;">
                                        Fashion Events Calendar for 2018 - Contrado Blog - Contrado UK</div>
                                    <div class="ui-block-e" id="like-btn" style="font-weight: bold;">
                                        <!--    <div class="heart"></div>  -->
                                            <div class="heart"></div> 
                                    </div>
                                </div>
                                <div class="ui-grid-a" id="bottom-points">
                                    <div class="ui-block-e" id="list-desc" style="width: 90% !important; height: 20%;margin-left: 10px;">
                                        For the backstage portion of a fashion show
                                        ensures every detail, large and small, <br>is successfully completed. A fashion
                                        show is a hub of activity with models, designers, stylists, volunteer, staff
                                        and support staff roaming the halls. <br>The most famous fashion shows are
                                        staged
                                        in the fashion hubs of Paris, New York and Milan. But hosting a fashion sh
                                    </div>
                                    <div class="ui-block-e" style="width: 10% !important; "></div>

                                </div>
                                <div class="ui-grid-a" id="bottom-points">
                                    <div class="ui-block-e" style="width: 55% !important; font-size: 24px;"></div>
                                    <div class="ui-block-e" style="width: 45% !important;font-style: italic;text-align: center;">
                                        <!-- <img src="images/like_fill.png " style="width:30px;height:30px;" alt="favorite Logo "> -->
                                        <button class="ui-btn ui-btn-inline cat-more-btn" style="width:100px;font-weight: bold;font-size: 14px;vertical-align: middle; text-shadow: none; margin-top:30px">More</button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </a></li>
                <li id="href-cat"><a href="#" id="href" class="href-cat">
                        <div class="ui-grid-a" id="bottom-points" style="margin-top:4px; background-color: rgb(255, 255, 255);vertical-align: middle;">
                            <div class="ui-block-e" style="width: 30% !important;height:100% !important;"><img src="images/event_3.jpg"></div>

                            <div class="ui-block-e" style="width: 70% !important; ">
                                <div class="ui-grid-a" id="bottom-points">

                                    <div class="ui-block-e" id="list-heading" style="margin-left: 10px; width: 80% !important;font-size: 16px; font-weight: bold;max-lines: 3;height: 30px;margin-top: 10px;">
                                        Heads to Make a Major Fashion Event a Happening</div>
                                    <div class="ui-block-e" id="like-btn" style="font-weight: bold;">
                                           <div class="heart"></div> 
                                    </div>
                                </div>
                                <div class="ui-grid-a" id="bottom-points">
                                    <div class="ui-block-e" id="list-desc" style="width: 90% !important; height: 20%;margin-left: 10px;">
                                        A human resource plan for the backstage portion of a fashion show
                                        ensures every detail, large and small, <br>is successfully completed. A fashion
                                        show is a hub of activity with models, designers, stylists, volunteer, staff
                                        and support staff roaming the halls. <br>The most famous fashion shows are
                                        staged
                                        in the fashion hubs of Paris, New York and Milan. But hosting a fashion sh
                                    </div>
                                    <div class="ui-block-e" style="width: 10% !important; "></div>

                                </div>
                                <div class="ui-grid-a" id="bottom-points">
                                    <div class="ui-block-e" style="width: 55% !important; font-size: 24px;"></div>
                                    <div class="ui-block-e" style="width: 45% !important;font-style: italic;text-align: center;">
                                        <!-- <img src="images/like_fill.png " style="width:30px;height:30px;" alt="favorite Logo "> -->
                                        <button class="ui-btn ui-btn-inline cat-more-btn" style="width:100px;font-weight: bold;font-size: 14px;vertical-align: middle; text-shadow: none; margin-top:30px">More</button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </a></li>
                <li id="href-shows" data-role="list-divider">Shows</li>
                <li id="href-shows"><a href="#" id="href">
                        <div class="ui-grid-a" id="bottom-points" style="margin-top:4px; background-color: rgb(255, 255, 255);vertical-align: middle;">
                            <div class="ui-block-e" style="width: 30% !important;height:100% !important;"><img src="images/event_4.jpg"></div>

                            <div class="ui-block-e" style="width: 70% !important; ">
                                <div class="ui-grid-a" id="bottom-points">

                                    <div class="ui-block-e" id="list-heading" style="margin-left: 10px;width: 80% !important;font-size: 16px; font-weight: bold;max-lines: 3;height: 30px;margin-top: 10px;">Riccardo
                                        Tisci Unpacks His Beige Proposition In First Burberry Pre-Fall Collection</div>
                                    <div class="ui-block-e" id="like-btn" style="font-weight: bold;">
                                           <div class="heart"></div> 
                                    </div>
                                </div>
                                <div class="ui-grid-a" id="bottom-points">
                                    <div class="ui-block-e" id="list-desc" style="width: 90% !important; height: 20%;margin-left: 10px;">
                                        “I
                                        wanted
                                        this collection to be a continuation of the story I began to tell in
                                        September,” Tisci told press of his <br>second line for the house. “I’m
                                        focusing on
                                        establishing our codes.... through archive <br>prints, house colours and iconic
                                        outerwear, while.... cementing the new themes I set out last season, including
                                        the Thomas Burberry monogram, eveningwear and tailoring.”
                                    </div>
                                    <div class="ui-block-e" style="width: 10% !important; "></div>

                                </div>
                                <div class="ui-grid-a" id="bottom-points">
                                    <div class="ui-block-e" style="width: 55% !important; font-size: 24px;"></div>
                                    <div class="ui-block-e" style="width: 45% !important;font-style: italic;text-align: center;">
                                        <!-- <img src="images/like_fill.png " style="width:30px;height:30px;" alt="favorite Logo "> -->
                                        <button class="ui-btn ui-btn-inline shows-more-btn" style="width:100px;font-weight: bold;font-size: 14px;vertical-align: middle; text-shadow: none;margin-top:30px; ">More</button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </a></li>
                <li id="href-shows"><a href="#" id="href">
                        <div class="ui-grid-a" id="bottom-points" style="margin-top:4px; background-color: rgb(255, 255, 255);vertical-align: middle;">
                            <div class="ui-block-e" style="width: 30% !important;height:100% !important;"><img src="images/News_1.jpg"></div>

                            <div class="ui-block-e" style="width: 70% !important; ">
                                <div class="ui-grid-a" id="bottom-points">

                                    <div class="ui-block-e" id="list-heading" style="margin-left: 10px;width: 80% !important;font-size: 16px; font-weight: bold;max-lines: 3;height: 30px;margin-top: 10px;">
                                        Heads to Make a Major Fashion Event a Happening</div>
                                    <div class="ui-block-e" id="like-btn" style="font-weight: bold;">
                                           <div class="heart"></div> 
                                    </div>
                                </div>
                                <div class="ui-grid-a" id="bottom-points">
                                    <div class="ui-block-e" id="list-desc" style="width: 90% !important; height: 20%;margin-left: 10px;">
                                        “I
                                        wanted
                                        this collection to be a continuation of the story I began to tell in
                                        September,” Tisci told press of his <br>second line for the house. “I’m
                                        focusing on
                                        establishing our codes.... through archive <br>prints, house colours and iconic
                                        outerwear, while.... cementing the new themes I set out last season, including
                                        the Thomas Burberry monogram, eveningwear and tailoring.”
                                    </div>
                                    <div class="ui-block-e" style="width: 10% !important; "></div>

                                </div>
                                <div class="ui-grid-a" id="bottom-points">
                                    <div class="ui-block-e" style="width: 55% !important; font-size: 24px;"></div>
                                    <div class="ui-block-e" style="width: 45% !important;font-style: italic;text-align: center;">
                                        <!-- <img src="images/like_fill.png " style="width:30px;height:30px;" alt="favorite Logo "> -->
                                        <button class="ui-btn ui-btn-inline shows-more-btn" style="width:100px;font-weight: bold;font-size: 14px;vertical-align: middle; text-shadow: none; margin-top:30px">More</button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </a></li>
                <li id="href-shows"><a href="#" id="href">
                        <div class="ui-grid-a" id="bottom-points" style="margin-top:4px; background-color: rgb(255, 255, 255);vertical-align: middle;">
                            <div class="ui-block-e" style="width: 30% !important;height:100% !important;"><img src="images/News_2.jpg"></div>

                            <div class="ui-block-e" style="width: 70% !important; ">
                                <div class="ui-grid-a" id="bottom-points">

                                    <div class="ui-block-e" id="list-heading" style="margin-left: 10px;width: 80% !important;font-size: 16px; font-weight: bold;max-lines: 3;height: 30px;margin-top: 10px;">Riccardo
                                        Tisci Unpacks His Beige Proposition In First Burberry Pre-Fall Collection</div>
                                    <div class="ui-block-e" id="like-btn" style="font-weight: bold;">
                                           <div class="heart"></div> 
                                    </div>
                                </div>
                                <div class="ui-grid-a" id="bottom-points">
                                    <div class="ui-block-e" id="list-desc" style="width: 90% !important; height: 20%;margin-left: 10px;">
                                        Resource plan for the backstage portion of a fashion show
                                        ensures every detail, large and small, <br>is successfully completed. A fashion
                                        show is a hub of activity with models, designers, stylists, volunteer, staff
                                        and support staff roaming the halls. <br>The most famous fashion shows are
                                        staged
                                        in the fashion hubs of Paris, New York and Milan. But hosting a fashion sh
                                    </div>
                                    <div class="ui-block-e" style="width: 10% !important; "></div>

                                </div>
                                <div class="ui-grid-a" id="bottom-points">
                                    <div class="ui-block-e" style="width: 55% !important; font-size: 24px;"></div>
                                    <div class="ui-block-e" style="width: 45% !important;font-style: italic;text-align: center;">
                                        <!-- <img src="images/like_fill.png " style="width:30px;height:30px;" alt="favorite Logo "> -->
                                        <button class="ui-btn ui-btn-inline shows-more-btn" style="width:100px;font-weight: bold;font-size: 14px;vertical-align: middle; text-shadow: none; margin-top:30px">More</button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </a></li>
                <li id="href-talks" data-role="list-divider">Talks</li>
                <li id="href-talks"><a href="#" id="href">
                        <div class="ui-grid-a" id="bottom-points" style="margin-top:4px; background-color: rgb(255, 255, 255);vertical-align: middle;">
                            <div class="ui-block-e" style="width: 30% !important;height:100% !important;"><img src="images/News_3.jpg"></div>

                            <div class="ui-block-e" style="width: 70% !important; ">
                                <div class="ui-grid-a" id="bottom-points">

                                    <div class="ui-block-e" id="list-heading" style="margin-left: 10px;width: 80% !important;font-size: 16px; font-weight: bold;max-lines: 3;height: 30px;margin-top: 10px;">
                                        Tisci Unpacks His Beige Proposition In First Burberry Pre-Fall Collection</div>
                                    <div class="ui-block-e" id="like-btn" style="font-weight: bold;">
                                           <div class="heart"></div> 
                                    </div>
                                </div>
                                <div class="ui-grid-a" id="bottom-points">
                                    <div class="ui-block-e" id="list-desc" style="width: 90% !important; height: 20%;margin-left: 10px;">
                                        Max Mara inspiration for its Autumn/Winter 2015-2016 collection came from the
                                        icon Marilyn Monroe, this can be seen by <br>the mirrored style of tight
                                        fitting,
                                        curve hugging garments which oozes femininity. The whole collection was a
                                        success at empowering Marilyn Monroe in a <br>modern era, it consisted of
                                        quilted
                                        skirts made from silk and tops/dresses made from wool, the contrasting
                                        materials complemented each other and are perfect for office and professional
                                        looks.
                                    </div>
                                    <div class="ui-block-e" style="width: 10% !important; "></div>

                                </div>
                                <div class="ui-grid-a" id="bottom-points">
                                    <div class="ui-block-e" style="width: 55% !important; font-size: 24px;"></div>
                                    <div class="ui-block-e" style="width: 45% !important;font-style: italic;text-align: center;">
                                        <!-- <img src="images/like_fill.png " style="width:30px;height:30px;" alt="favorite Logo "> -->
                                        <button class="ui-btn ui-btn-inline shows-more-btn" style="width:100px;font-weight: bold;font-size: 14px;vertical-align: middle; text-shadow: none; margin-top:30px">More</button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </a></li>
                <li id="href-talks"><a href="#" id="href">
                        <div class="ui-grid-a" id="bottom-points" style="margin-top:4px; background-color: rgb(255, 255, 255);vertical-align: middle;">
                            <div class="ui-block-e" style="width: 30% !important;height:100% !important;"><img src="images/News_4.jpg"></div>

                            <div class="ui-block-e" style="width: 70% !important; ">
                                <div class="ui-grid-a" id="bottom-points">

                                    <div class="ui-block-e" id="list-heading" style="margin-left: 10px;width: 80% !important;font-size: 16px; font-weight: bold;max-lines: 3;height: 30px;margin-top: 10px;">Riccardo
                                        Tisci Unpacks His Beige Proposition In First Burberry Pre-Fall Collection</div>
                                    <div class="ui-block-e" id="like-btn" style="font-weight: bold;">
                                           <div class="heart"></div> 
                                    </div>
                                </div>
                                <div class="ui-grid-a" id="bottom-points">
                                    <div class="ui-block-e" id="list-desc" style="width: 90% !important; height: 20%;margin-left: 10px;">
                                        “I
                                        wanted
                                        this collection to be a continuation of the story I began to tell in
                                        September,” Tisci told press of his <br>second line for the house. “I’m
                                        focusing on
                                        establishing our codes.... through archive <br>prints, house colours and iconic
                                        outerwear, while.... cementing the new themes I set out last season, including
                                        the Thomas Burberry monogram, eveningwear and tailoring.”
                                    </div>
                                    <div class="ui-block-e" style="width: 10% !important; "></div>

                                </div>
                                <div class="ui-grid-a" id="bottom-points">
                                    <div class="ui-block-e" style="width: 55% !important; font-size: 24px;"></div>
                                    <div class="ui-block-e" style="width: 45% !important;font-style: italic;text-align: center;">
                                        <!-- <img src="images/like_fill.png " style="width:30px;height:30px;" alt="favorite Logo "> -->
                                        <button class="ui-btn ui-btn-inline shows-more-btn" style="width:100px;font-weight: bold;font-size: 14px;vertical-align: middle; text-shadow: none;margin-top:30px ">More</button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </a></li>
                <li id="href-talks"><a href="#" id="href">
                        <div class="ui-grid-a" id="bottom-points" style="margin-top:4px; background-color: rgb(255, 255, 255);vertical-align: middle;">
                            <div class="ui-block-e" style="width: 30% !important;height:100% !important;"><img src="images/News_1.jpg"></div>

                            <div class="ui-block-e" style="width: 70% !important; ">
                                <div class="ui-grid-a" id="bottom-points">

                                    <div class="ui-block-e" id="list-heading" style="margin-left: 10px;width: 80% !important;font-size: 16px; font-weight: bold;max-lines: 3;height: 30px;margin-top: 10px;">
                                        Tisci Unpacks His Beige Proposition In First Burberry Pre-Fall Collection</div>
                                    <div class="ui-block-e" id="like-btn" style="font-weight: bold;">
                                           <div class="heart"></div> 
                                    </div>
                                </div>
                                <div class="ui-grid-a" id="bottom-points">
                                    <div class="ui-block-e" id="list-desc" style="width: 90% !important; height: 20%;margin-left: 10px;">
                                        “I
                                        wanted
                                        this collection to be a continuation of the story I began to tell in
                                        September,” Tisci told press of his <br>second line for the house. “I’m
                                        focusing on
                                        establishing our codes.... through archive <br>prints, house colours and iconic
                                        outerwear, while.... cementing the new themes I set out last season, including
                                        the Thomas Burberry monogram, eveningwear and tailoring.”
                                    </div>
                                    <div class="ui-block-e" style="width: 10% !important; "></div>

                                </div>
                                <div class="ui-grid-a" id="bottom-points">
                                    <div class="ui-block-e" style="width: 55% !important; font-size: 24px;"></div>
                                    <div class="ui-block-e" style="width: 45% !important;font-style: italic;text-align: center;">
                                        <!-- <img src="images/like_fill.png " style="width:30px;height:30px;" alt="favorite Logo "> -->
                                        <button class="ui-btn ui-btn-inline shows-more-btn" style="width:100px;font-weight: bold;font-size: 14px;vertical-align: middle; text-shadow: none; margin-top:30px">More</button>
                                    </div>
                                </div>

                            </div>
                    </li>


            </ul>


        </div>

        <script type="text/javascript">
         $(function () {

            $(".heart").on("click", function () {
                $(this).toggleClass("heart-blast");
            });
        });
            function changeImage() {

                if (document.getElementById("imgClickAndChange").src == "images/like_fill.png") {
                    document.getElementById("imgClickAndChange").src = "images/like_empty.png";
                }
                else {
                    document.getElementById("imgClickAndChange").src = "images/like_fill.png";
                }

            }

            $(".cat-more-btn").on("click", function () {
                    window.location.replace("http://localhost/London_Fashion_Week/catwalk.php");

            });

            $(".talks-more-btn").on("click", function () {
                    window.location.replace("http://localhost/London_Fashion_Week/talks.php");

            });

            $(".shows-more-btn").on("click", function () {
                    window.location.replace("http://localhost/London_Fashion_Week/shows.php");

            });
            
        </script>



        <!-- Footer -->
        <div data-role="footer" style="border-style: none;background-color: black;padding-top: 4px" data-position="relative">
            <div class="ui-grid">
                <div class="ui-bar" style="height:20px;margin-top: 15px;">TEAM STYLEHUNT © 2018</div>
            </div>
            <div class="ui-grid-c">
                <div class="ui-block-e" style="width: 16.66% !important; "></div>
                <div class="ui-block-e" style="width: 16.66% !important; "><img src="images/facebook.png " width="25px "
                        height="25px " alt="Facebook Logo "></div>
                <div class="ui-block-e" style="width: 16.66% !important; "><img src="images/insta.png " width="25px "
                        height="25px " alt="Insta Logo "></div>
                <div class="ui-block-e" style="width: 16.66% !important; "><img src="images/twitter.png " width="25px "
                        height="25px " alt="Twitter Logo "></div>
                <div class="ui-block-e" style="width: 16.66% !important; "><img src="images/gplus.png " width="25px "
                        height="25px " alt="Gplus Logo "></div>
                <div class="ui-block-e" style="width: 16.66% !important; "></div>
            </div>
            <div class="ui-grid">
                <div class="ui-bar" style="height:40px;margin-top: 7px">ALL RIGHTS RESERVED</div>
            </div>
        </div>
        <!-- /footer -->
    </div>
    <!-- /page -->

</body>

</html>