<?php
session_start();
?>
<!DOCTYPE html>
<html>

<head>
    <title>London Fashion Week</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />

    <link rel="stylesheet" href="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css" />
    <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script>
    <!-- Top Slider -->
    <link rel="stylesheet" type="text/css" href="engine1/style.css" />
    <script type="text/javascript" src="engine1/jquery.js"></script>
    <!-- Sliders -->
    <link rel="stylesheet" type="text/css" href="slick-1.8.1/slick/slick.css">
    <link rel="stylesheet" type="text/css" href="slick-1.8.1/slick/slick-theme.css">

    <style>
        .ui-grid-b {
            height: 80px !important;
        }
        
        img {
            max-width: 100%;
            max-height: 100%;
            display: block;
            margin-left: auto;
            margin-right: auto;
            margin-top: auto;
            margin-bottom: auto;
            align: center;
        }
        
        .portrait {
            height: 80px;
            width: 25px;
        }
        
        .landscape {
            height: 25px;
            width: 80px;
        }
        
        .square {
            height: 75px;
            width: 75px;
        }
        
        #header-grid {
            background-color: black;
            border-style: none !important;
        }
        
        .ui-grid {
            background-color: black;
            border-style: none !important;
        }
        
        [data-role=page] {
            height: 100% !important;
            position: relative !important;
            font-family: "Source Sans Pro"
        }
        
        [data-role=header] {
            font-size: 25px;
            align-content: center;
        }

        [data-role=content] {
            height: 100%;
            margin: 0 auto;
            width: auto;
        }
        
        /* [data-role=footer] {
            position: relative !important;
            top: auto !important;
            width: 100%;
        } */
        
        [data-role=panel] {
            font-family: "Source Sans Pro";
            color: white;
        }
        
        .ui-block-a {
            margin-top: 5px;
            margin-bottom: 5px;
        }
        
        .ui-block-e {
            margin-top: 0px;
            margin-bottom: 0px;
            align: center;
        }
        
        .ui-block-b {
            margin-top: 15px;
            text-align: center;
            font-family: "Source Sans Pro";
            color: white;
            font-weight: normal;
            font-size: 18px;
            text-shadow: none;
        }
        
        .ui-bar {
            text-align: center;
            font-family: "Source Sans Pro";
            color: white;
            font-weight: normal;
            font-size: 12px;
            text-shadow: none;
            border-style: none !important;
        }
        
        .ui-block-c {
            margin-top: 16px;
        }
        
        .ui-block-d {
            margin-top: 18px;
        }
        
        body,
        input,
        select,
        textarea,
        button,
        .ui-btn {
            line-height: 1.3;
            font-family: "Source Sans Pro";
        }
        
        #search-basic {
            font-size: 24sp;
        }
        
        .ui-panel-position-right.ui-panel-display-reveal {
            -webkit-box-shadow: left;
        }
        
        .ui-btn-icon-left:after,
        .ui-btn-icon-right:after,
        .ui-btn-icon-top:after,
        .ui-btn-icon-bottom:after,
        .ui-btn-icon-notext:after {
            content: none;
        }
        
        .ui-icon-delete:after {
            background-color: black;
        }
        
        /* .ui-listview>li.ui-last-child>a.ui-btn {
            border-bottom-width: 0px;
        }
         */
        .ui-page-theme-a .ui-btn {
            background-color: white;
        }
        
        .ui-panel-inner {
            background-color: white;
        }
        #wowslider-container1{
            margin: 0px;
        }
        .ui-content{
            padding: 0px;
        }
        .ui-grid-solo{
            text-align: center;
            font-family: "Source Sans Pro";

        }
        
        #news-content{
            text-align: justify;
            margin-left: 20px;
            margin-right: 20px;
        }
        #center-button{
            margin: 0 auto;
            text-align: center;
            font-family: "Source Sans Pro";
            padding: 1px;
        }
        li.ui-block-b.ui-state-default.ui-corner-top{
            margin-top: 0px;

        }
        li.ui-block-c.ui-state-default.ui-corner-top{
            margin-top: 0px;
        }
        li.ui-block-a.ui-state-default.ui-corner-top{
            margin-top: 0px;
        }
        li.ui-block-d.ui-state-default.ui-corner-top{
            margin-top: 0px;
        }

        [data-role="navbar"]{
            padding: 15px;
        }
        .ui-mini {
            margin-left: 18px;
            margin-right: 18px;
            margin-top: 19px;
            margin-bottom: 0px;
        }
        .ui-page-theme-a .ui-btn.ui-btn-active{
            background-color: black;
            border-color:black;
            lighting-color: aliceblue 
        }
        .ui-input-text, .ui-input-search{
            margin-left: 18px;
            margin-right: 18px;
            margin-top: 18px;

        }
        #bottom-points{
            height: 0% !important;
            margin-top: 0px !important;

        }
        #href{
            padding: 7px;
        }

        #like-btn{
            align:right !important;
        }
        #list-heading{
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
            max-width: 75ch;
            font-family: "Source Sans Pro";
            font-size: 13px;

        }
        #list-desc{
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
            max-width: 175ch;
            font-family: "Source Sans Pro";
            font-size: 11px;
       
        }
        .ui-btn .ui-btn-inline{
            padding: 3px;
        }

        .ui-field-contain{
            padding: 0px;
            margin-left: 18px;
            margin-top: 0px;
        }

        .ui-select{
             height:50px;
             padding: 0px !important;
            margin-top: 0px;
        }
        .heart {
            margin-left: 19px;
            margin-top: 140px;
            width: 69px;
            height: 70px;
            position: relative;
            left: 50%;
            top: 50%;
            transform: translate(-50%, -50%);
            background: url(images/heart.png) no-repeat;  
            cursor: pointer;  
        }
            .heart-blast {
            background-position: -2800px 0;
            transition: background 1s steps(28);
        }
      
    </style>
</head>

<body>

    <div data-role="page">

        <div data-role="header" data-position="fixed" style="border-style: none;">
            <div class="ui-grid-c" id="header-grid">
                <div class="ui-block-a" style="width: 30% !important; "><img src="images/logo_black.png " width="50px "
                        height="50px " alt="London Fashion Week Logo "></div>
                <div class="ui-block-b" style="width: 40% !important; ">Favorite List</div>
                <div class="ui-block-c" style="width: 15% !important; "><a href="http://localhost/London_Fashion_Week/ShoppingCart.php" data-transition="slide" data-ajax="false">
                <img src="images/cart.png " width="30px " height="27px " alt="Cart "></a></div>
                <div class="ui-block-d" style="width: 15% !important; ">
                    <a href="#nav-panel"><img src="images/hamburger.png " width="28px " height="23px " alt="Cart "></a>
                </div>
            </div>
            <!-- /grid-b -->
        </div>
        <!-- /header -->

        <?php          
   
        if(!isset($_SESSION['login_user'])){
            // header("Location: http://".$_SERVER['HTTP_HOST']."/London_Fashion_Week/Login.php", true, 302);
            echo "<div data-role=\"panel\" data-position=\"right\" data-display=\"reveal\" data-theme=\"c\" id=\"nav-panel\">";
            echo "<ul data-role=\"listview\">";
            echo "<li data-icon=\"delete\"><a href=\"#\" data-rel=\"close\">Menu</a></li>";    
            echo "<input type=\"search\" name=\"search\" data-corners=\"false\" id=\"search-basic\" value=\"\" />";    
            echo " <li><a href=\"http://localhost/London_Fashion_Week/Login.php\" data-transition=\"slide\" data-ajax=\"false\">Sign in</a></li>"; 
            echo " <li><a href=\"http://localhost/London_Fashion_Week/Register.php\" data-transition=\"slide\" data-ajax=\"false\">Register</a></li>";
            echo " <li><a href=\"http://localhost/London_Fashion_Week/index.php\" data-transition=\"slide\" data-ajax=\"false\">Home</a></li>"; 
            echo " <li><a href=\"http://localhost/London_Fashion_Week/all_news.php\" data-transition=\"slide\" data-ajax=\"false\">News</a></li>"; 
            echo " <li><a href=\"http://localhost/London_Fashion_Week/portfolio.php\" data-transition=\"slide\" data-ajax=\"false\">Portfolio</a></li>"; 
            echo " <li><a href=\"http://localhost/London_Fashion_Week/Events.php\" data-transition=\"slide\" data-ajax=\"false\">Events</a></li>";  
            echo " <li><a href=\"http://localhost/London_Fashion_Week/ShoppingCart.php\" data-transition=\"slide\" data-ajax=\"false\">Shopping Cart</a></li>"; 
            echo " <li><a href=\"http://localhost/London_Fashion_Week/Point_of_Interest.php\" data-transition=\"slide\" data-ajax=\"false\">Point of Interest</a></li>"; 
            echo " <li><a href=\"http://localhost/London_Fashion_Week/gallery.php\" data-transition=\"slide\" data-ajax=\"false\">Gallery</a></li>";   
            echo " <li><a href=\"http://localhost/London_Fashion_Week/AboutUs.php\" data-transition=\"slide\" data-ajax=\"false\">About Us</a></li>";   
            echo " </ul>";   
            echo "</div>";
        }else{
            echo "<div data-role=\"panel\" data-position=\"right\" data-display=\"reveal\" data-theme=\"c\" id=\"nav-panel\">";
            echo "<ul data-role=\"listview\">";
            echo "<li data-icon=\"delete\"><a href=\"#\" data-rel=\"close\">Menu</a></li>";    
            echo "<input type=\"search\" name=\"search\" data-corners=\"false\" id=\"search-basic\" value=\"\" />";    
            echo " <li><a href=\"http://localhost/London_Fashion_Week/index.php\" data-transition=\"slide\" data-ajax=\"false\">Home</a></li>";             
            echo " <li><a href=\"http://localhost/London_Fashion_Week/MyAccount.php\" data-transition=\"slide\" data-ajax=\"false\">My Account</a></li>";
            echo " <li><a href=\"http://localhost/London_Fashion_Week/all_news.php\" data-transition=\"slide\" data-ajax=\"false\">News</a></li>"; 
            echo " <li><a href=\"http://localhost/London_Fashion_Week/portfolio.php\" data-transition=\"slide\" data-ajax=\"false\">Portfolio</a></li>"; 
            echo " <li><a href=\"http://localhost/London_Fashion_Week/Events.php\" data-transition=\"slide\" data-ajax=\"false\">Events</a></li>"; 
            echo " <li><a href=\"http://localhost/London_Fashion_Week/ShoppingCart.php\" data-transition=\"slide\" data-ajax=\"false\">Shopping Cart</a></li>"; 
            echo " <li><a href=\"http://localhost/London_Fashion_Week/Point_of_Interest.php\" data-transition=\"slide\" data-ajax=\"false\">Point of Interest</a></li>"; 
            echo " <li><a href=\"http://localhost/London_Fashion_Week/gallery.php\" data-transition=\"slide\" data-ajax=\"false\">Gallery</a></li>";   
            echo " <li><a href=\"http://localhost/London_Fashion_Week/AboutUs.php\" data-transition=\"slide\" data-ajax=\"false\">About Us</a></li>";    
            echo " <li><a href=\"http://localhost/London_Fashion_Week/Logout.php\" data-transition=\"slide\" data-ajax=\"false\">Sign out</a></li>";   
            echo " </ul>";   
            echo "</div>";
        }
        ?>

        <!-- /panel -->


        <div data-role="content">
            <div class="ui-grid-a">
                <div data-role="navbar">
                    <ul style="height:0px !important">
                        <li style="margin-top: 0px;" class="rt1"><a href="#">All</a></li>
                        <li style="margin-top: 0px;" class="rt2"><a href="#">News</a></li>
                        <li style="margin-top: 0px;" class="rt3"><a href="#">Products</a></li>
                    </ul>
                    <ul style="height:0px !important">
                        <li style="margin-top: 0px;" class="rt4"><a href="#">Events</a></li>
                        <li style="margin-top: 0px;" class="rt5"><a href="#">POI</a></li>
                        <li style="margin-top: 0px;" class="rt6"><a href="#">Team</a></li>
                    </ul>
                </div>

            </div>

            <ul data-role="listview" data-filter="true" data-divider-theme="b" data-filter-placeholder="Search Events"
                data-inset="true" id="eventsUl">

                <script>
                    $('.rt1 a').click(function () {
                        // $(this).closest('.cont').find('#href-cat').hide('slow');
                        // return false;
                        var input, filter, ul, li, a, i, txtValue;
                        ul = document.getElementById("eventsUl");
                        li = ul.getElementsByTagName("li");
                        for (i = 0; i < li.length; i++) {
                            li[i].style.display = "";
                        }

                    });

                    $('.rt2 a').click(function () {
                        // $(this).closest('.cont').find('#href-cat').hide('slow');
                        // return false;
                        var input, filter, ul, li, a, i, txtValue;
                        input = "href-news";
                        filter = input;
                        ul = document.getElementById("eventsUl");
                        li = ul.getElementsByTagName("li");
                        for (i = 0; i < li.length; i++) {
                            if (li[i].id == filter) {
                                li[i].style.display = "";
                            } else {
                                li[i].style.display = "none";
                            }
                        }

                    });

                    $('.rt3 a').click(function () {
                        // $(this).closest('.cont').find('#href-cat').hide('slow');
                        // return false;
                        var input, filter, ul, li, a, i, txtValue;
                        input = "href-products";
                        filter = input;
                        ul = document.getElementById("eventsUl");
                        li = ul.getElementsByTagName("li");
                        for (i = 0; i < li.length; i++) {
                            if (li[i].id == filter) {
                                li[i].style.display = "";
                            } else {
                                li[i].style.display = "none";
                            }
                        }

                    });

                    $('.rt4 a').click(function () {
                        // $(this).closest('.cont').find('#href-cat').hide('slow');
                        // return false;
                        var input, filter, ul, li, a, i, txtValue;
                        input = "href-events";
                        filter = input;
                        ul = document.getElementById("eventsUl");
                        li = ul.getElementsByTagName("li");
                        for (i = 0; i < li.length; i++) {
                            if (li[i].id == filter) {
                                li[i].style.display = "";
                            } else {
                                li[i].style.display = "none";
                            }
                        }

                    });

                    $('.rt5 a').click(function () {
                        // $(this).closest('.cont').find('#href-cat').hide('slow');
                        // return false;
                        var input, filter, ul, li, a, i, txtValue;
                        input = "href-poi";
                        filter = input;
                        ul = document.getElementById("eventsUl");
                        li = ul.getElementsByTagName("li");
                        for (i = 0; i < li.length; i++) {
                            if (li[i].id == filter) {
                                li[i].style.display = "";
                            } else {
                                li[i].style.display = "none";
                            }
                        }

                    });

                    $('.rt6 a').click(function () {
                        // $(this).closest('.cont').find('#href-cat').hide('slow');
                        // return false;
                        var input, filter, ul, li, a, i, txtValue;
                        input = "href-team";
                        filter = input;
                        ul = document.getElementById("eventsUl");
                        li = ul.getElementsByTagName("li");
                        for (i = 0; i < li.length; i++) {
                            if (li[i].id == filter) {
                                li[i].style.display = "";
                            } else {
                                li[i].style.display = "none";
                            }
                        }

                    });


                </script>

                <!-- News -->
                <li id="href-news" data-role="list-divider">News</li>
                <li id="href-news" class="news1"><a href="#" id="href" class="href-news">

                        <div class="ui-grid-a" id="bottom-points" style="margin-top:4px; background-color: rgb(255, 255, 255);vertical-align: middle;">
                            <div class="ui-block-e" style="width: 30% !important;height:100% !important;"><img src="images/News_1.jpg"></div>

                            <div class="ui-block-e" style="width: 70% !important;height:100% !important; ">
                                <div class="ui-grid-a" id="bottom-points" style="height:20% !important">
                                    <div class="ui-block-e" id="list-heading" style="width: 90% !important;font-size: 16px; font-weight: bold;max-lines: 3;height: 30px;margin-top: 10px; margin-left: 10px;">Max
                                        Mara Autumn/Winter 2015-2016 Collection | Fashion News
                                    </div>
                                    <div class="ui-block-e" id="like-btn" style="width: 10% !important;font-weight: bold;text-align: right"></div>
                                </div>
                                <div class="ui-grid-a" id="bottom-points">
                                    <div class="ui-block-e" id="list-desc" style="width: 90% !important; height: 20%;margin-left: 10px;">
                                        Gigi Hadid, at just 19 years old, is known for Sports Illustrator and being the
                                        face of Guess, nevertheless she opened <br>the Milan Fashion Week show this
                                        week
                                        for Max Mara, she smouldered down the catwalk in a camel coat that hugged her
                                        body, and beach like hair loosely tied up in a <br>bun. Max Mara is the man who
                                        knows how to make simple garments look sexy, this can be seen on Joan Smalls as
                                        she swaggers down the catwalk,her compelling stride, shows off her power and
                                        confidence when wearing the garment.
                                    </div>
                                    <div class="ui-block-e" style="width: 10% !important; "></div>

                                </div>
                                <div class="ui-grid-a" id="bottom-points">
                                    <div class="ui-block-e" style="width: 80% !important; "></div>
                                    <div class="ui-block-e" style="font-style: italic;text-align: center;">
                                        <img src="images/like_fill.png " style="width:30px;height:30px;" alt="favorite Logo ">
                                    </div>
                                </div>

                            </div>
                        </div>
                    </a>
                </li>
                <li id="href-news" class="news2"><a href="#" id="href" class="href-news">
                        <div class="ui-grid-a" id="bottom-points" style="margin-top:4px; background-color: rgb(255, 255, 255);vertical-align: middle;">
                            <div class="ui-block-e" style="width: 30% !important;height:100% !important;"><img src="images/News_2.jpg"></div>

                            <div class="ui-block-e" style="width: 70% !important;height:100% !important; ">
                                <div class="ui-grid-a" id="bottom-points" style="height:20% !important">
                                    <div class="ui-block-e" id="list-heading" style="width: 90% !important;font-size: 16px; font-weight: bold;max-lines: 3;height: 30px;margin-top: 10px; margin-left: 10px;">The
                                        whole collection was a
                                        success at empowering Marilyn Monroe in a</div>
                                    <div class="ui-block-e" id="like-btn" style="width: 10% !important;font-weight: bold;text-align: right"></div>
                                </div>
                                <div class="ui-grid-a" id="bottom-points">
                                    <div class="ui-block-e" id="list-desc" style="width: 90% !important; height: 20%;margin-left: 10px;">
                                        Max Mara inspiration for its Autumn/Winter 2015-2016 collection came from the
                                        icon Marilyn Monroe, this can be seen by <br>the mirrored style of tight
                                        fitting,
                                        curve hugging garments which oozes femininity. The whole collection was a
                                        success at empowering Marilyn Monroe in a <br>modern era, it consisted of
                                        quilted
                                        skirts made from silk and tops/dresses made from wool, the contrasting
                                        materials complemented each other and are perfect for office and professional
                                        looks.
                                    </div>
                                    <div class="ui-block-e" style="width: 10% !important; "></div>

                                </div>
                                <div class="ui-grid-a" id="bottom-points">
                                    <div class="ui-block-e" style="width: 80% !important; "></div>
                                    <div class="ui-block-e" style="font-style: italic;text-align: center;">
                                        <img src="images/like_fill.png " style="width:30px;height:30px;" alt="favorite Logo ">
                                        <!-- <button
                                        class="ui-btn ui-btn-inline" style="width:100px;font-weight: bold;font-size: 13px;vertical-align: middle">More</button> -->
                                    </div>
                                </div>

                            </div>
                        </div>
                    </a></li>
                <li id="href-news" class="news3"><a href="#" id="href" class="href-news">
                        <div class="ui-grid-a" id="bottom-points" style="margin-top:4px; background-color: rgb(255, 255, 255);vertical-align: middle;">
                            <div class="ui-block-e" style="width: 30% !important;height:100% !important;"><img src="images/News_3.jpg"></div>

                            <div class="ui-block-e" style="width: 70% !important;height:100% !important; ">
                                <div class="ui-grid-a" id="bottom-points" style="height:20% !important">
                                    <div class="ui-block-e" id="list-heading" style="width: 90% !important;font-size: 16px; font-weight: bold;max-lines: 3;height: 30px;margin-top: 10px; margin-left: 10px;">Unpacks
                                        His Beige Proposition In First Burberry Pre-Fall Collection</div>
                                    <div class="ui-block-e" id="like-btn" style="width: 10% !important;font-weight: bold;text-align: right"></div>
                                </div>
                                <div class="ui-grid-a" id="bottom-points">
                                    <div class="ui-block-e" id="list-desc" style="width: 90% !important; height: 20%;margin-left: 10px;">
                                        the mirrored style of tight fitting,
                                        curve hugging garments which oozes femininity. The whole collection was a
                                        success at empowering Marilyn Monroe in a <br>modern era, it consisted of
                                        quilted
                                        skirts made from silk and tops/dresses made from wool, Max Mara inspiration for
                                        its Autumn/Winter 2015-2016 collection came from the
                                        icon Marilyn Monroe, this can be seen by <br>the contrasting
                                        materials complemented each other and are perfect for office and professional
                                        looks.
                                    </div>
                                    <div class="ui-block-e" style="width: 10% !important; "></div>

                                </div>
                                <div class="ui-grid-a" id="bottom-points">
                                    <div class="ui-block-e" style="width: 80% !important; "></div>
                                    <div class="ui-block-e" style="font-style: italic;text-align: center;">
                                        <img src="images/like_fill.png " style="width:30px;height:30px;" alt="favorite Logo ">
                                        <!-- <button
                                        class="ui-btn ui-btn-inline" style="width:100px;font-weight: bold;font-size: 13px;vertical-align: middle">More</button> -->
                                    </div>
                                </div>

                            </div>
                        </div>
                    </a></li>
                <li id="href-news" class="news4"><a href="#" id="href" class="href-news">
                        <div class="ui-grid-a" id="bottom-points" style="margin-top:4px; background-color: rgb(255, 255, 255);vertical-align: middle;">
                            <div class="ui-block-e" style="width: 30% !important;height:100% !important;"><img src="images/News_4.jpg"></div>

                            <div class="ui-block-e" style="width: 70% !important;height:100% !important; ">
                                <div class="ui-grid-a" id="bottom-points" style="height:20% !important">
                                    <div class="ui-block-e" id="list-heading" style="width: 90% !important;font-size: 16px; font-weight: bold;max-lines: 3;height: 30px;margin-top: 10px; margin-left: 10px;">Riccardo
                                        Tisci Unpacks His Beige Proposition In First Burberry Pre-Fall Collection</div>
                                    <div class="ui-block-e" id="like-btn" style="width: 10% !important;font-weight: bold;text-align: right"></div>
                                </div>
                                <div class="ui-grid-a" id="bottom-points">
                                    <div class="ui-block-e" id="list-desc" style="width: 90% !important; height: 20%;margin-left: 10px;">
                                        Gigi Hadid, at compelling stride, shows off her power and
                                        confidence when wearing the garmentless she opened <br>the Milan Fashion Week
                                        show this
                                        week
                                        for Max Mara, she smouldered down the catwalk in a camel coat that hugged her
                                        body, and beach like hair loosely tied up in a <br>bun. Max Mara is the man who
                                        knows how to make simple garments look sexy, this can be seen on Joan Smalls as
                                        she swaggers down the catwalk,her compelling stride, shows off her power and
                                        confidence when wearing the garment.
                                    </div>
                                    <div class="ui-block-e" style="width: 10% !important; "></div>

                                </div>
                                <div class="ui-grid-a" id="bottom-points">
                                    <div class="ui-block-e" style="width: 80% !important; "></div>
                                    <div class="ui-block-e" style="font-style: italic;text-align: center;">
                                        <img src="images/like_fill.png " style="width:30px;height:30px;" alt="favorite Logo ">
                                        <!-- <button
                                        class="ui-btn ui-btn-inline" style="width:100px;font-weight: bold;font-size: 13px;vertical-align: middle">More</button> -->
                                    </div>
                                </div>

                            </div>
                        </div>
                    </a></li>

                <!-- Products -->
                <li id="href-products" data-role="list-divider">Products</li>
                <li id="href-products" class="product1"><a href="#" id="href" class="href-products">
                        <div class="ui-grid-a" id="bottom-points" style="margin-top:4px; background-color: rgb(255, 255, 255);vertical-align: middle;">
                            <div class="ui-block-e" style="width: 30% !important;height:100% !important;"><img src="images/product_1.jpg"></div>

                            <div class="ui-block-e" style="width: 70% !important; ">
                                <div class="ui-grid-a" id="bottom-points">

                                    <div class="ui-block-e" id="list-heading" style="width: 80% !important;font-size: 16px; font-weight: bold;max-lines: 3;height: 30px;margin-top: 10px;margin-left: 10px">ACQUA
                                        DI GIÒ ABSOLU</div>
                                    <div class="ui-block-e" id="like-btn" style="font-weight: bold;">
                                        <img src="images/like_fill.png " style="width:30px;height:30px;" alt="favorite Logo ">

                                    </div>
                                </div>
                                <div class="ui-grid-a" id="bottom-points">
                                    <div class="ui-block-e" id="list-desc" style="width: 90% !important; height: 20%;margin-left: 10px">
                                        Introducing ACQUA DI GIÒ ABSOLU fragrance for men, celebrating the union
                                        between fresh water and warm wood.<br> Seen through the eyes of Giorgio Armani,
                                        the
                                        alliance of water and wood takes us on a journey into timeless masculinity,
                                        embracing a deeper overall consciousness.

                                    </div>
                                    <div class="ui-block-e" style="width: 10% !important; "></div>

                                </div>
                                <div class="ui-grid-a" id="bottom-points">
                                    <div class="ui-block-e" style="width: 55% !important; font-size: 24px;">
                                        $68</div>
                                    <div class="ui-block-e" style="width: 45% !important;font-style: italic;text-align: center;">
                                        <!-- <img src="images/like_fill.png " style="width:30px;height:30px;" alt="favorite Logo "> -->
                                        <button class="ui-btn ui-btn-inline" style="width:100px;font-weight: bold;font-size: 14px;vertical-align: middle; background-color: rgb(8, 194, 8); color: white;text-shadow: none; ">Add
                                            to Cart</button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </a></li>
                <li id="href-products" class="product2"><a href="#" id="href" class="href-products">
                        <div class="ui-grid-a" id="bottom-points" style="margin-top:4px; background-color: rgb(255, 255, 255);vertical-align: middle;">
                            <div class="ui-block-e" style="width: 30% !important;height:100% !important;"><img src="images/product_2.jpg"></div>

                            <div class="ui-block-e" style="width: 70% !important; ">
                                <div class="ui-grid-a" id="bottom-points">

                                    <div class="ui-block-e" id="list-heading" style="width: 80% !important;font-size: 16px; font-weight: bold;max-lines: 3;height: 30px;margin-top: 10px;margin-left: 10px">COAT
                                        WITH PLEAT</div>
                                    <div class="ui-block-e" id="like-btn" style="font-weight: bold;">
                                        <img src="images/like_fill.png " style="width:30px;height:30px;" alt="favorite Logo ">

                                    </div>
                                </div>
                                <div class="ui-grid-a" id="bottom-points">
                                    <div class="ui-block-e" id="list-desc" style="width: 90% !important; height: 20%;margin-left: 10px;">
                                        Collared trench coat with a detachable hood. Featuring long sleeves with
                                        shoulder tabs, front welt pockets, <br>an A-line silhouette, a buttoned back
                                        pleat,
                                        side vents at the hem and double-breasted button fastening in the front.
                                    </div>
                                    <div class="ui-block-e" style="width: 10% !important; "></div>

                                </div>
                                <div class="ui-grid-a" id="bottom-points">
                                    <div class="ui-block-e" style="width: 55% !important; font-size: 24px;">
                                        $23</div>
                                    <div class="ui-block-e" style="width: 45% !important;font-style: italic;text-align: center;">
                                        <!-- <img src="images/like_fill.png " style="width:30px;height:30px;" alt="favorite Logo "> -->
                                        <button class="ui-btn ui-btn-inline" style="width:100px;font-weight: bold;font-size: 14px;vertical-align: middle; background-color: rgb(8, 194, 8); color: white;text-shadow: none; ">Add
                                            to Cart</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a></li>

                <li id="href-products" class="product3"><a href="#" id="href" class="href-products">
                        <div class="ui-grid-a" id="bottom-points" style="margin-top:4px; background-color: rgb(255, 255, 255);vertical-align: middle;">
                            <div class="ui-block-e" style="width: 30% !important;height:100% !important;"><img src="images/product_3.jpg"></div>

                            <div class="ui-block-e" style="width: 70% !important; ">
                                <div class="ui-grid-a" id="bottom-points">

                                    <div class="ui-block-e" id="list-heading" style="width: 80% !important;font-size: 16px; font-weight: bold;max-lines: 3;height: 30px;margin-top: 10px;margin-left: 10px">QUILTED
                                        VELVET PARKA</div>
                                    <div class="ui-block-e" id="like-btn" style="font-weight: bold;">
                                        <img src="images/like_fill.png " style="width:30px;height:30px;" alt="favorite Logo ">

                                    </div>
                                </div>
                                <div class="ui-grid-a" id="bottom-points">
                                    <div class="ui-block-e" id="list-desc" style="width: 90% !important; height: 20%;margin-left: 10px;">
                                        Collared parka with long sleeves and shoulder tabs. Featuring front welt
                                        pockets and snap-button fastening in<br> the front.Collared parka with long
                                        sleeves
                                        and shoulder tabs. Featuring front welt pockets and snap-button fastening in
                                        the front.
                                    </div>
                                    <div class="ui-block-e" style="width: 10% !important; "></div>

                                </div>
                                <div class="ui-grid-a" id="bottom-points">
                                    <div class="ui-block-e" style="width: 55% !important; font-size: 24px;">
                                        $128</div>
                                    <div class="ui-block-e" style="width: 45% !important;font-style: italic;text-align: center;">
                                        <!-- <img src="images/like_fill.png " style="width:30px;height:30px;" alt="favorite Logo "> -->
                                        <button class="ui-btn ui-btn-inline" style="width:100px;font-weight: bold;font-size: 14px;vertical-align: middle; background-color: rgb(8, 194, 8); color: white;text-shadow: none; ">Add
                                            to Cart</button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </a></li>
                <li id="href-products" class="product4"><a href="#" id="href" class="href-products">
                        <div class="ui-grid-a" id="bottom-points" style="margin-top:4px; background-color: rgb(255, 255, 255);vertical-align: middle;">
                            <div class="ui-block-e" style="width: 30% !important;height:100% !important;"><img src="images/product_4.jpg"></div>

                            <div class="ui-block-e" style="width: 70% !important; ">
                                <div class="ui-grid-a" id="bottom-points">

                                    <div class="ui-block-e" id="list-heading" style="width: 80% !important;font-size: 16px; font-weight: bold;max-lines: 3;height: 30px;margin-top: 10px;margin-left: 10px">TRENCH
                                        COAT WITH PLEAT</div>
                                    <div class="ui-block-e" id="like-btn" style="font-weight: bold;">
                                        <img src="images/like_fill.png " style="width:30px;height:30px;" alt="favorite Logo ">

                                    </div>
                                </div>
                                <div class="ui-grid-a" id="bottom-points">
                                    <div class="ui-block-e" id="list-desc" style="width: 90% !important; height: 20%;margin-left: 10px;">
                                        ong sleeves and shoulder tabs. Featuring front welt
                                        pockets and snap-button fastening in<br> the front.Collared parka with long
                                        sleeves
                                        and shoulder tabs. Featuring front welt pockets and snap-button fastening in
                                        the front.
                                    </div>
                                    <div class="ui-block-e" style="width: 10% !important; "></div>

                                </div>
                                <div class="ui-grid-a" id="bottom-points">
                                    <div class="ui-block-e" style="width: 55% !important; font-size: 24px;">
                                        $78</div>
                                    <div class="ui-block-e" style="width: 45% !important;font-style: italic;text-align: center;">
                                        <!-- <img src="images/like_fill.png " style="width:30px;height:30px;" alt="favorite Logo "> -->
                                        <button class="ui-btn ui-btn-inline" style="width:100px;font-weight: bold;font-size: 14px;vertical-align: middle; background-color: rgb(8, 194, 8); color: white;text-shadow: none; ">Add
                                            to Cart</button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </a></li>


                <!-- Events -->
                <li id="href-events" data-role="list-divider">Events</li>
                <li id="href-events" class="event1"><a href="#" id="href" class="href-events">
                        <div class="ui-grid-a" id="bottom-points" style="margin-top:4px; background-color: rgb(255, 255, 255);vertical-align: middle;">
                            <div class="ui-block-e" style="width: 30% !important;height:100% !important;"><img src="images/event_1.png"></div>

                            <div class="ui-block-e" style="width: 70% !important; ">
                                <div class="ui-grid-a" id="bottom-points">

                                    <div class="ui-block-e" id="list-heading" style="width: 80% !important;font-size: 16px; font-weight: bold;max-lines: 3;height: 30px;margin-top: 10px;margin-left: 10px;">Riccardo
                                        Tisci Unpacks His Beige Proposition In First Burberry Pre-Fall Collection</div>
                                    <div class="ui-block-e" id="like-btn" style="font-weight: bold;">
                                        <img src="images/like_fill.png " style="width:30px;height:30px;" alt="favorite Logo ">

                                    </div>
                                </div>
                                <div class="ui-grid-a" id="bottom-points">
                                    <div class="ui-block-e" id="list-desc" style="width: 90% !important; height: 20%;margin-left: 10px;">
                                        Developing a human resource plan for the backstage portion of a fashion show
                                        ensures every detail, large and small, <br>is successfully completed. A fashion
                                        show is a hub of activity with models, designers, stylists, volunteer, staff
                                        and support staff roaming the halls. <br>The most famous fashion shows are
                                        staged
                                        in the fashion hubs of Paris, New York and Milan. But hosting a fashion sh
                                    </div>
                                    <div class="ui-block-e" style="width: 10% !important; "></div>

                                </div>
                                <div class="ui-grid-a" id="bottom-points">
                                    <div class="ui-block-e" style="width: 55% !important; font-size: 24px;"></div>
                                    <div class="ui-block-e" style="width: 45% !important;font-style: italic;text-align: center;">
                                        <!-- <img src="images/like_fill.png " style="width:30px;height:30px;" alt="favorite Logo "> -->
                                        <button class="ui-btn ui-btn-inline" style="width:100px;font-weight: bold;font-size: 14px;vertical-align: middle; text-shadow: none; ">More</button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </a></li>

                <li id="href-events" class="event2"><a href="#" id="href" class="href-events">
                        <div class="ui-grid-a" id="bottom-points" style="margin-top:4px; background-color: rgb(255, 255, 255);vertical-align: middle;">
                            <div class="ui-block-e" style="width: 30% !important;height:100% !important;"><img src="images/event_2.jpg"></div>

                            <div class="ui-block-e" style="width: 70% !important; ">
                                <div class="ui-grid-a" id="bottom-points">

                                    <div class="ui-block-e" id="list-heading" style="width: 80% !important;font-size: 16px; font-weight: bold;max-lines: 3;height: 30px;margin-top: 10px;margin-left: 10px;">Your
                                        Fashion Events Calendar for 2018 - Contrado Blog - Contrado UK</div>
                                    <div class="ui-block-e" id="like-btn" style="font-weight: bold;">
                                        <img src="images/like_fill.png " style="width:30px;height:30px;" alt="favorite Logo ">

                                    </div>
                                </div>
                                <div class="ui-grid-a" id="bottom-points">
                                    <div class="ui-block-e" id="list-desc" style="width: 90% !important; height: 20%;margin-left: 10px;">
                                        For the backstage portion of a fashion show
                                        ensures every detail, large and small, <br>is successfully completed. A fashion
                                        show is a hub of activity with models, designers, stylists, volunteer, staff
                                        and support staff roaming the halls. <br>The most famous fashion shows are
                                        staged
                                        in the fashion hubs of Paris, New York and Milan. But hosting a fashion sh
                                    </div>
                                    <div class="ui-block-e" style="width: 10% !important; "></div>

                                </div>
                                <div class="ui-grid-a" id="bottom-points">
                                    <div class="ui-block-e" style="width: 55% !important; font-size: 24px;"></div>
                                    <div class="ui-block-e" style="width: 45% !important;font-style: italic;text-align: center;">
                                        <!-- <img src="images/like_fill.png " style="width:30px;height:30px;" alt="favorite Logo "> -->
                                        <button class="ui-btn ui-btn-inline" style="width:100px;font-weight: bold;font-size: 14px;vertical-align: middle; text-shadow: none; ">More</button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </a></li>
                <li id="href-events" class="event3"><a href="#" id="href" class="href-events">
                        <div class="ui-grid-a" id="bottom-points" style="margin-top:4px; background-color: rgb(255, 255, 255);vertical-align: middle;">
                            <div class="ui-block-e" style="width: 30% !important;height:100% !important;"><img src="images/event_3.jpg"></div>

                            <div class="ui-block-e" style="width: 70% !important; ">
                                <div class="ui-grid-a" id="bottom-points">

                                    <div class="ui-block-e" id="list-heading" style="width: 80% !important;font-size: 16px; font-weight: bold;max-lines: 3;height: 30px;margin-top: 10px;margin-left: 10px;">Turning
                                        Heads to Make a Major Fashion Event a Happening</div>
                                    <div class="ui-block-e" id="like-btn" style="font-weight: bold;">
                                        <img src="images/like_fill.png " style="width:30px;height:30px;" alt="favorite Logo ">

                                    </div>
                                </div>
                                <div class="ui-grid-a" id="bottom-points">
                                    <div class="ui-block-e" id="list-desc" style="width: 90% !important; height: 20%;margin-left: 10px;">
                                        A human resource plan for the backstage portion of a fashion show
                                        ensures every detail, large and small, <br>is successfully completed. A fashion
                                        show is a hub of activity with models, designers, stylists, volunteer, staff
                                        and support staff roaming the halls. <br>The most famous fashion shows are
                                        staged
                                        in the fashion hubs of Paris, New York and Milan. But hosting a fashion sh
                                    </div>
                                    <div class="ui-block-e" style="width: 10% !important; "></div>

                                </div>
                                <div class="ui-grid-a" id="bottom-points">
                                    <div class="ui-block-e" style="width: 55% !important; font-size: 24px;"></div>
                                    <div class="ui-block-e" style="width: 45% !important;font-style: italic;text-align: center;">
                                        <!-- <img src="images/like_fill.png " style="width:30px;height:30px;" alt="favorite Logo "> -->
                                        <button class="ui-btn ui-btn-inline" style="width:100px;font-weight: bold;font-size: 14px;vertical-align: middle; text-shadow: none; ">More</button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </a></li>
                <li id="href-events" class="event4"><a href="#" id="href" class="href-events">
                        <div class="ui-grid-a" id="bottom-points" style="margin-top:4px; background-color: rgb(255, 255, 255);vertical-align: middle;">
                            <div class="ui-block-e" style="width: 30% !important;height:100% !important;"><img src="images/event_4.jpg"></div>

                            <div class="ui-block-e" style="width: 70% !important; ">
                                <div class="ui-grid-a" id="bottom-points">

                                    <div class="ui-block-e" id="list-heading" style="width: 80% !important;font-size: 16px; font-weight: bold;max-lines: 3;height: 30px;margin-top: 10px;margin-left: 10px;">First
                                        'Change Fashion' Event Geared Toward Cleaning Up Supply</div>
                                    <div class="ui-block-e" id="like-btn" style="font-weight: bold;">
                                        <img src="images/like_fill.png " style="width:30px;height:30px;" alt="favorite Logo ">

                                    </div>
                                </div>
                                <div class="ui-grid-a" id="bottom-points">
                                    <div class="ui-block-e" id="list-desc" style="width: 90% !important; height: 20%;margin-left: 10px;">
                                        Resource plan for the backstage portion of a fashion show
                                        ensures every detail, large and small, <br>is successfully completed. A fashion
                                        show is a hub of activity with models, designers, stylists, volunteer, staff
                                        and support staff roaming the halls. <br>The most famous fashion shows are
                                        staged
                                        in the fashion hubs of Paris, New York and Milan. But hosting a fashion sh
                                    </div>
                                    <div class="ui-block-e" style="width: 10% !important; "></div>

                                </div>
                                <div class="ui-grid-a" id="bottom-points">
                                    <div class="ui-block-e" style="width: 55% !important; font-size: 24px;"> </div>
                                    <div class="ui-block-e" style="width: 45% !important;font-style: italic;text-align: center;">
                                        <!-- <img src="images/like_fill.png " style="width:30px;height:30px;" alt="favorite Logo "> -->
                                        <button class="ui-btn ui-btn-inline" style="width:100px;font-weight: bold;font-size: 14px;vertical-align: middle; text-shadow: none; ">More</button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </a></li>

                <!-- POI -->
                <li id="href-poi" data-role="list-divider">Point of Interest</li>
                <li id="href-poi" class="poi1"><a href="#" id="href" class="href-poi">
                        <div class="ui-grid-a" id="bottom-points" style="margin-top:4px; background-color: rgb(255, 255, 255);vertical-align: middle;">
                            <div class="ui-block-e" style="width: 30% !important;height:100% !important;"><img src="images/poi_01.jpg"></div>

                            <div class="ui-block-e" style="width: 70% !important; ">
                                <div class="ui-grid-a" id="bottom-points">

                                    <div class="ui-block-e" id="list-heading" style="width: 80% !important;font-size: 16px; font-weight: bold;max-lines: 3;height: 30px;margin-top: 10px;margin-left: 10px;">The
                                        Galleria</div>
                                    <div class="ui-block-e" id="like-btn" style="font-weight: bold;">
                                        <img src="images/like_fill.png " style="width:30px;height:30px;" alt="favorite Logo ">

                                    </div>
                                </div>
                                <div class="ui-grid-a" id="bottom-points">
                                    <div class="ui-block-e" id="list-desc" style="width: 90% !important; height: 20%;margin-left: 10px;">
                                        The Galleria, stylized theGalleria<br> or the Houston Galleria, is an upscale
                                        mixed-use urban development shopping mall located in the Uptown District of
                                        Houston, Texas, United States. The development <br>consists of a retail
                                        complex, as
                                        well as the Galleria Office Towers complex, two Westin hotels, and a private
                                        health club
                                    </div>
                                    <div class="ui-block-e" style="width: 10% !important; "></div>

                                </div>
                                <div class="ui-grid-a" id="bottom-points">
                                    <div class="ui-block-e" style="width: 55% !important; font-size: 24px;"> </div>
                                    <div class="ui-block-e" style="width: 45% !important;font-style: italic;text-align: center;">
                                        <!-- <img src="images/like_fill.png " style="width:30px;height:30px;" alt="favorite Logo "> -->
                                        <!-- <button class="ui-btn ui-btn-inline" style="width:100px;font-weight: bold;font-size: 14px;vertical-align: middle; text-shadow: none; ">More</button> -->
                                    </div>
                                </div>

                            </div>
                        </div>
                    </a></li>
                <li id="href-poi" class="poi2"><a href="#" id="href" class="href-poi">
                        <div class="ui-grid-a" id="bottom-points" style="margin-top:4px; background-color: rgb(255, 255, 255);vertical-align: middle;">
                            <div class="ui-block-e" style="width: 30% !important;height:100% !important;"><img src="images/poi_2.jpeg"></div>

                            <div class="ui-block-e" style="width: 70% !important; ">
                                <div class="ui-grid-a" id="bottom-points">

                                    <div class="ui-block-e" id="list-heading" style="width: 80% !important;font-size: 16px; font-weight: bold;max-lines: 3;height: 30px;margin-top: 10px;margin-left: 10px;">Villaggio
                                        Mall</div>
                                    <div class="ui-block-e" id="like-btn" style="font-weight: bold;">
                                        <img src="images/like_fill.png " style="width:30px;height:30px;" alt="favorite Logo ">

                                    </div>
                                </div>
                                <div class="ui-grid-a" id="bottom-points">
                                    <div class="ui-block-e" id="list-desc" style="width: 90% !important; height: 20%;margin-left: 10px;">
                                        Villaggio Mall is a shopping mall located in the Aspire Zone in the west end of
                                        Doha, the capital city of Qatar. It is <br>located on Al Waab street between
                                        the
                                        Hyatt Plaza and Sports City and has over 200 stores, <br>including many famous
                                        brands in the U.S., U.K., Italian and German markets
                                    </div>
                                    <div class="ui-block-e" style="width: 10% !important; "></div>

                                </div>
                                <div class="ui-grid-a" id="bottom-points">
                                    <div class="ui-block-e" style="width: 55% !important; font-size: 24px;"> </div>
                                    <div class="ui-block-e" style="width: 45% !important;font-style: italic;text-align: center;">
                                        <!-- <img src="images/like_fill.png " style="width:30px;height:30px;" alt="favorite Logo "> -->
                                        <!-- <button class="ui-btn ui-btn-inline" style="width:100px;font-weight: bold;font-size: 14px;vertical-align: middle; text-shadow: none; ">More</button> -->
                                    </div>
                                </div>

                            </div>
                        </div>
                    </a></li>

                <!-- Team Members -->
                <li id="href-team" data-role="list-divider">Team</li>
                <li id="href-team" class="team1"><a href="#" id="href" class="href-team">
                        <div class="ui-grid-a" id="bottom-points" style="margin-top:4px; background-color: rgb(255, 255, 255);vertical-align: middle;">
                            <div class="ui-block-e" style="width: 30% !important;height:120px;width: 120px;"><img src="images/kaushan.gif"></div>

                            <div class="ui-block-e" style="width: 70% !important; ">
                                <div class="ui-grid-a" id="bottom-points">

                                    <div class="ui-block-e" id="list-heading" style="width: 80% !important;font-size: 16px; font-weight: bold;max-lines: 3;height: 30px;margin-top: 10px;margin-left: 10px;">Kaushan
                                        Fernando</div>
                                    <div class="ui-block-e" id="like-btn" style="font-weight: bold;">
                                        <img src="images/like_fill.png " style="width:30px;height:30px;" alt="favorite Logo ">

                                    </div>
                                </div>
                                <div class="ui-grid-a" id="bottom-points">
                                    <div class="ui-block-e" id="list-desc" style="width: 90% !important; height: 20%;margin-left: 10px;">
                                        Enjoy being challenged and engaging with projects that <br>require me to work
                                        outside my comfort and knowledge set, as <br>continuing to learn new languages
                                        and
                                        development techniques are important to me and the success of your
                                        organization.
                                    </div>
                                    <div class="ui-block-e" style="width: 10% !important; "></div>

                                </div>
                                <div class="ui-grid-a" id="bottom-points">
                                    <div class="ui-block-e" style="width: 55% !important; font-size: 24px;"> </div>
                                    <div class="ui-block-e" style="width: 45% !important;font-style: italic;text-align: center;">
                                        <!-- <img src="images/like_fill.png " style="width:30px;height:30px;" alt="favorite Logo "> -->
                                        <!-- <button class="ui-btn ui-btn-inline" style="width:100px;font-weight: bold;font-size: 14px;vertical-align: middle; text-shadow: none; ">More</button> -->
                                    </div>
                                </div>

                            </div>
                        </div>
                    </a></li>
                <li id="href-team" class="team2"><a href="#" id="href" class="href-team">
                        <div class="ui-grid-a" id="bottom-points" style="margin-top:4px; background-color: rgb(255, 255, 255);vertical-align: middle;">
                            <div class="ui-block-e" style="width: 30% !important;height:120px;width: 120px;"><img src="images/menusha.gif"></div>

                            <div class="ui-block-e" style="width: 70% !important; ">
                                <div class="ui-grid-a" id="bottom-points">

                                    <div class="ui-block-e" id="list-heading" style="width: 80% !important;font-size: 16px; font-weight: bold;max-lines: 3;height: 30px;margin-top: 10px;margin-left: 10px;">Menusha
                                        Pathirana</div>
                                    <div class="ui-block-e" id="like-btn" style="font-weight: bold;">
                                        <img src="images/like_fill.png " style="width:30px;height:30px;" alt="favorite Logo ">

                                    </div>
                                </div>
                                <div class="ui-grid-a" id="bottom-points">
                                    <div class="ui-block-e" id="list-desc" style="width: 90% !important; height: 20%;margin-left: 10px;">
                                        Enjoy being challenged and engaging with projects that <br>require me to work
                                        outside my comfort and knowledge set, as <br>continuing to learn new languages
                                        and
                                        development techniques are important to me and the success of your
                                        organization.
                                    </div>
                                    <div class="ui-block-e" style="width: 10% !important; "></div>

                                </div>
                                <div class="ui-grid-a" id="bottom-points">
                                    <div class="ui-block-e" style="width: 55% !important; font-size: 24px;"> </div>
                                    <div class="ui-block-e" style="width: 45% !important;font-style: italic;text-align: center;">
                                        <!-- <img src="images/like_fill.png " style="width:30px;height:30px;" alt="favorite Logo "> -->
                                        <!-- <button class="ui-btn ui-btn-inline" style="width:100px;font-weight: bold;font-size: 14px;vertical-align: middle; text-shadow: none; ">More</button> -->
                                    </div>
                                </div>

                            </div>
                        </div>
                    </a></li>
                <li id="href-team" class="team3"><a href="#" id="href" class="href-team">
                        <div class="ui-grid-a" id="bottom-points" style="margin-top:4px; background-color: rgb(255, 255, 255);vertical-align: middle;">
                            <div class="ui-block-e" style="width: 30% !important;height:120px;width: 120px;"><img src="images/ashan.gif"></div>

                            <div class="ui-block-e" style="width: 70% !important; ">
                                <div class="ui-grid-a" id="bottom-points">

                                    <div class="ui-block-e" id="list-heading" style="width: 80% !important;font-size: 16px; font-weight: bold;max-lines: 3;height: 30px;margin-top: 10px;margin-left: 10px;">Ashan
                                        De Vaas</div>
                                    <div class="ui-block-e" id="like-btn" style="font-weight: bold;">
                                        <img src="images/like_fill.png " style="width:30px;height:30px;" alt="favorite Logo ">

                                    </div>
                                </div>
                                <div class="ui-grid-a" id="bottom-points">
                                    <div class="ui-block-e" id="list-desc" style="width: 90% !important; height: 20%;margin-left: 10px;">
                                        Enjoy being challenged and engaging with projects that <br>require me to work
                                        outside my comfort and knowledge set, as <br>continuing to learn new languages
                                        and
                                        development techniques are important to me and the success of your
                                        organization.
                                    </div>
                                    <div class="ui-block-e" style="width: 10% !important; "></div>

                                </div>
                                <div class="ui-grid-a" id="bottom-points">
                                    <div class="ui-block-e" style="width: 55% !important; font-size: 24px;"> </div>
                                    <div class="ui-block-e" style="width: 45% !important;font-style: italic;text-align: center;">
                                        <!-- <img src="images/like_fill.png " style="width:30px;height:30px;" alt="favorite Logo "> -->
                                        <!-- <button class="ui-btn ui-btn-inline" style="width:100px;font-weight: bold;font-size: 14px;vertical-align: middle; text-shadow: none; ">More</button> -->
                                    </div>
                                </div>

                            </div>
                        </div>
                    </a></li>
                <li id="href-team" class="team4"><a href="#" id="href" class="href-team">
                        <div class="ui-grid-a" id="bottom-points" style="margin-top:4px; background-color: rgb(255, 255, 255);vertical-align: middle;">
                            <div class="ui-block-e" style="width: 30% !important;height:120px;width: 120px;"><img src="images/haritha.gif"></div>

                            <div class="ui-block-e" style="width: 70% !important; ">
                                <div class="ui-grid-a" id="bottom-points">

                                    <div class="ui-block-e" id="list-heading" style="width: 80% !important;font-size: 16px; font-weight: bold;max-lines: 3;height: 30px;margin-top: 10px;margin-left: 10px;">Haritha
                                        Gayanayake</div>
                                    <div class="ui-block-e" id="like-btn" style="font-weight: bold;">
                                        <img src="images/like_fill.png " style="width:30px;height:30px;" alt="favorite Logo ">

                                    </div>
                                </div>
                                <div class="ui-grid-a" id="bottom-points">
                                    <div class="ui-block-e" id="list-desc" style="width: 90% !important; height: 20%;margin-left: 10px;">
                                        Enjoy being challenged and engaging with projects that <br>require me to work
                                        outside my comfort and knowledge set, as <br>continuing to learn new languages
                                        and
                                        development techniques are important to me and the success of your
                                        organization.
                                    </div>
                                    <div class="ui-block-e" style="width: 10% !important; "></div>

                                </div>
                                <div class="ui-grid-a" id="bottom-points">
                                    <div class="ui-block-e" style="width: 55% !important; font-size: 24px;"> </div>
                                    <div class="ui-block-e" style="width: 45% !important;font-style: italic;text-align: center;">
                                        <!-- <img src="images/like_fill.png " style="width:30px;height:30px;" alt="favorite Logo "> -->
                                        <!-- <button class="ui-btn ui-btn-inline" style="width:100px;font-weight: bold;font-size: 14px;vertical-align: middle; text-shadow: none; ">More</button> -->
                                    </div>
                                </div>

                            </div>
                        </div>
                    </a></li>

                <script>
                    $('.news1 a').click(function () {
                        removeItems("news1");
                    });
                    $('.news2 a').click(function () {
                        removeItems("news2");
                    });
                    $('.news3 a').click(function () {
                        removeItems("news3");
                    });
                    $('.news4 a').click(function () {
                        removeItems("news4");
                    });

                    $('.product1 a').click(function () {
                        removeItems("product1");
                    });

                    $('.product2 a').click(function () {
                        removeItems("product2");
                    });

                    $('.product3 a').click(function () {
                        removeItems("product3");
                    });

                    $('.product4 a').click(function () {
                        removeItems("product4");
                    });

                    $('.event1 a').click(function () {
                        removeItems("event1");
                    });

                    $('.event2 a').click(function () {
                        removeItems("event2");
                    });

                    $('.event3 a').click(function () {
                        removeItems("event3");
                    });

                    $('.event4 a').click(function () {
                        removeItems("event4");
                    });

                    $('.poi1 a').click(function () {
                        removeItems("poi1");
                    });

                    $('.poi2 a').click(function () {
                        removeItems("poi2");
                    });

                    $('.team1 a').click(function () {
                        removeItems("team1");
                    });

                    $('.team2 a').click(function () {
                        removeItems("team2");
                    });

                    $('.team3 a').click(function () {
                        removeItems("team3");
                    });

                    $('.team4 a').click(function () {
                        removeItems("team4");
                    });


                    function removeItems(itemClassName) {
                        var input, filter, ul, li, a, i, txtValue;
                        filter = itemClassName;
                        ul = document.getElementById("eventsUl");
                        li = ul.getElementsByTagName("li");
                        for (i = 0; i < li.length; i++) {
                            if (li[i].className == filter) {
                                li[i].remove();
                            }
                        }
                    }

                </script>

            </ul>

            <div class="ui-grid-solo">
                Share your Favorites
            </div>

            <form action="https://formspree.io/menusha69@gmail.com" method="POST">
                <input type="text" style="display:none;" name="name" value="News - Maxmara autumn winter, The whole collection, unpackes his bage proposition, recardo tesky unpacks his bage || Products - Acua de gio absolut, coat with fleet, quilted velverd pacca || Events - Recardo Tescy unpacks,Your fashion events calendar || POI - the gallery, Villagio mall || Team - kaushan, menusha, ashan, haritha">
                <input type="email" name="_replyto">
                <input type="submit" value="Send">
            </form>

        </div>






        <!-- Footer -->
        <div data-role="footer" style="border-style: none;background-color: black;padding-top: 4px" data-position="relative">
            <div class="ui-grid">
                <div class="ui-bar" style="height:20px;margin-top: 15px;">TEAM STYLEHUNT © 2018</div>
            </div>
            <div class="ui-grid-c">
                <div class="ui-block-e" style="width: 16.66% !important; "></div>
                <div class="ui-block-e" style="width: 16.66% !important; "><img src="images/facebook.png " width="25px "
                        height="25px " alt="Facebook Logo "></div>
                <div class="ui-block-e" style="width: 16.66% !important; "><img src="images/insta.png " width="25px "
                        height="25px " alt="Insta Logo "></div>
                <div class="ui-block-e" style="width: 16.66% !important; "><img src="images/twitter.png " width="25px "
                        height="25px " alt="Twitter Logo "></div>
                <div class="ui-block-e" style="width: 16.66% !important; "><img src="images/gplus.png " width="25px "
                        height="25px " alt="Gplus Logo "></div>
                <div class="ui-block-e" style="width: 16.66% !important; "></div>
            </div>
            <div class="ui-grid">
                <div class="ui-bar" style="height:40px;margin-top: 7px">ALL RIGHTS RESERVED</div>
            </div>
        </div>
        <!-- /footer -->
    </div>
    <!-- /page -->

</body>

</html>