<?php
   include("Config.php");
   session_start();
   $message = "";
   $color ="";
   if($_SERVER["REQUEST_METHOD"] == "POST") {
      // username and password sent from form 
      $fname = mysqli_real_escape_string($db,$_POST['fname']);
      $myusername = mysqli_real_escape_string($db,$_POST['email']);
      $mypassword = mysqli_real_escape_string($db,$_POST['password']); 

    if ($db->connect_error) {
        $color ="red";
        $message = "Connection Error";
        die("Connection failed: " . $db->connect_error);
    } 
    if($fname && $myusername && $mypassword){

      $sql = "INSERT INTO user (name,username,passwrd) VALUES('$fname','$myusername','$mypassword')";
    
    if ($db->query($sql) === TRUE) {
        $color ="green";
        $message = "New User Added successfully";
    } else {
        $color ="red";
        $message =  "Error: " . $sql . "<br>" . $db->error;
    }
   }else{
    $color ="red";
    $message = "Please fill all the details";
   }
    $db->close();
   }
?>
<!DOCTYPE html>
<html>

<head>
    <title>London Fashion Week</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />

    <link rel="stylesheet" href="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css" />
    <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script>
    <!-- Top Slider -->
    <link rel="stylesheet" type="text/css" href="engine1/style.css" />
    <script type="text/javascript" src="engine1/jquery.js"></script>
    <!-- Sliders -->
    <link rel="stylesheet" type="text/css" href="slick-1.8.1/slick/slick.css">
    <link rel="stylesheet" type="text/css" href="slick-1.8.1/slick/slick-theme.css">

    <style>
        .ui-grid-b {
            height: 80px !important;
        }
        
        img {
            max-width: 100%;
            max-height: 100%;
            display: block;
            margin-left: auto;
            margin-right: auto;
            margin-top: auto;
            margin-bottom: auto;
            align: center;
        }
        
        .portrait {
            height: 80px;
            width: 25px;
        }
        
        .landscape {
            height: 25px;
            width: 80px;
        }
        
        .square {
            height: 75px;
            width: 75px;
        }
        
        #header-grid {
            background-color: black;
            border-style: none !important;
        }
        
        .ui-grid {
            background-color: black;
            border-style: none !important;
        }
        
        [data-role=page] {
            height: 100% !important;
            position: relative !important;
            font-family: "Source Sans Pro"
        }
        
        [data-role=header] {
            font-size: 25px;
            align-content: center;
        }

        [data-role=content] {
            height: 100%;
            margin: 0 auto;
            width: auto;
        }
        
        /* [data-role=footer] {
            position: relative !important;
            top: auto !important;
            width: 100%;
        } */
        
        [data-role=panel] {
            font-family: "Source Sans Pro";
            color: white;
        }
        
        .ui-block-a {
            margin-top: 5px;
            margin-bottom: 5px;
        }
        
        .ui-block-e {
            margin-top: 5px;
            margin-bottom: 5px;
            align: center;
        }
        
        .ui-block-b {
            margin-top: 15px;
            text-align: center;
            font-family: "Source Sans Pro";
            color: white;
            font-weight: normal;
            font-size: 18px;
            text-shadow: none;
        }
        
        .ui-bar {
            text-align: center;
            font-family: "Source Sans Pro";
            color: white;
            font-weight: normal;
            font-size: 12px;
            text-shadow: none;
            border-style: none !important;
        }
        
        .ui-block-c {
            margin-top: 16px;
        }
        
        .ui-block-d {
            margin-top: 18px;
        }
        
        body,
        input,
        select,
        textarea,
        button,
        .ui-btn {
            line-height: 1.3;
            font-family: "Source Sans Pro";
        }
        
        #search-basic {
            font-size: 24sp;
        }
        
        .ui-panel-position-right.ui-panel-display-reveal {
            -webkit-box-shadow: left;
        }
        
        .ui-btn-icon-left:after,
        .ui-btn-icon-right:after,
        .ui-btn-icon-top:after,
        .ui-btn-icon-bottom:after,
        .ui-btn-icon-notext:after {
            content: none;
        }
        
        .ui-icon-delete:after {
            background-color: black;
        }
        
        .ui-listview>li.ui-last-child>a.ui-btn {
            border-bottom-width: 0px;
        }
        
        .ui-page-theme-a .ui-btn {
            background-color: white;
        }
        
        .ui-panel-inner {
            background-color: white;
        }
        #wowslider-container1{
            margin: 0px;
        }
        .ui-content{
            padding: 0px;
        }
        .ui-grid-solo{
            text-align: center;
            font-family: "Source Sans Pro";

        }
        
        #news-content{
            text-align: justify;
            margin-left: 20px;
            margin-right: 20px;
        }
        #center-button{
            margin: 0 auto;
            text-align: center;
            font-family: "Source Sans Pro";
            padding: 1px;
        }
        li.ui-block-b.ui-state-default.ui-corner-top{
            margin-top: 0px;

        }
        li.ui-block-c.ui-state-default.ui-corner-top{
            margin-top: 0px;
        }
        li.ui-block-a.ui-state-default.ui-corner-top{
            margin-top: 0px;
        }
        li.ui-block-d.ui-state-default.ui-corner-top{
            margin-top: 0px;
        }

        [data-role="navbar"]{
            padding: 15px;
        }
        .ui-mini {
            margin-left: 18px;
            margin-right: 18px;
            margin-top: 19px;
            margin-bottom: 0px;
        }
        .ui-page-theme-a .ui-btn.ui-btn-active{
            background-color: black;
            border-color:black;
            lighting-color: aliceblue 
        }
        .ui-input-text, .ui-input-search{
            margin-left: 18px;
            margin-right: 18px;
            margin-top: 0px;
        }
        #login-text{
            font-family: "Source Sans Pro";
            margin-left: 18px;
            margin-top: 18px;
            font-weight: bold;
        }
        #login-btn{
            font-family: "Source Sans Pro";
            margin-left: 18px;
            margin-right: 18px;
            margin-top: 30px;
            font-weight: bold;
        }

        #sign-btn{
            font-family: "Source Sans Pro";
            margin-left: 18px;
            margin-right: 18px;
            margin-top: 30px;
            font-weight: bold;
        }
    
    </style>
</head>

<body>

    <div data-role="page">

        <div data-role="header" data-position="fixed" style="border-style: none;">
            <div class="ui-grid-c" id="header-grid">
                <div class="ui-block-a" style="width: 30% !important; "><img src="images/logo_black.png " width="50px "
                        height="50px " alt="London Fashion Week Logo "></div>
                <div class="ui-block-b" style="width: 40% !important; ">Register</div>
                <div class="ui-block-c" style="width: 15% !important; "><a href="http://localhost/London_Fashion_Week/ShoppingCart.php" data-transition="slide" data-ajax="false">
                <img src="images/cart.png " width="30px " height="27px " alt="Cart "></a></div>
                <div class="ui-block-d" style="width: 15% !important; ">
                    <a href="#nav-panel"><img src="images/hamburger.png " width="28px " height="23px " alt="Cart "></a>
                </div>
            </div>
            <!-- /grid-b -->
        </div>
        <!-- /header -->

        <?php          
       // header("Location: http://".$_SERVER['HTTP_HOST']."/London_Fashion_Week/Login.php", true, 302);
       echo "<div data-role=\"panel\" data-position=\"right\" data-display=\"reveal\" data-theme=\"c\" id=\"nav-panel\">";
       echo "<ul data-role=\"listview\">";
       echo "<li data-icon=\"delete\"><a href=\"#\" data-rel=\"close\">Menu</a></li>";    
       echo "<input type=\"search\" name=\"search\" data-corners=\"false\" id=\"search-basic\" value=\"\" />";   
       echo " <li><a href=\"http://localhost/London_Fashion_Week/index.php\" data-transition=\"slide\" data-ajax=\"false\">Home</a></li>";
       echo " <li><a href=\"http://localhost/London_Fashion_Week/Login.php\" data-transition=\"slide\" data-ajax=\"false\">Sign in</a></li>";         
       echo " <li><a href=\"http://localhost/London_Fashion_Week/all_news.php\" data-transition=\"slide\" data-ajax=\"false\">News</a></li>"; 
       echo " <li><a href=\"http://localhost/London_Fashion_Week/portfolio.php\" data-transition=\"slide\" data-ajax=\"false\">Portfolio</a></li>"; 
       echo " <li><a href=\"http://localhost/London_Fashion_Week/Events.php\" data-transition=\"slide\" data-ajax=\"false\">Events</a></li>"; 
       echo " <li><a href=\"http://localhost/London_Fashion_Week/Favorite_list.php\" data-transition=\"slide\" data-ajax=\"false\">Favorite List</a></li>"; 
       echo " <li><a href=\"http://localhost/London_Fashion_Week/ShoppingCart.php\" data-transition=\"slide\" data-ajax=\"false\">Shopping Cart</a></li>"; 
       echo " <li><a href=\"http://localhost/London_Fashion_Week/Point_of_Interest.php\" data-transition=\"slide\" data-ajax=\"false\">Point of Interest</a></li>"; 
       echo " <li><a href=\"http://localhost/London_Fashion_Week/gallery.php\" data-transition=\"slide\" data-ajax=\"false\">Gallery</a></li>";   
       echo " <li><a href=\"http://localhost/London_Fashion_Week/AboutUs.php\" data-transition=\"slide\" data-ajax=\"false\">About Us</a></li>";      
       echo " </ul>";   
       echo "</div>";
   
   ?>

        <!-- /panel -->


        <div data-role="content">
            <label id="login-text" style="font-size: 18px;font-weight: normal" for="text-basic">Create an Account</label>

            <form action="" method="post">
           <?php 
           if($message){
            echo "<div style=\"transition: all 1s ease-in-out;\"><font style=\"color:".$color."\">".$message."</font></div>";
           }
           ?>
                <label id="login-text" for="text-basic">Your Name</label>
                <input type="text" name="fname" id="text-basic" value="">

                <label id="login-text" for="text-basic">Email</label>
                <input type="text" name="email" id="text-basic" value="">

                <label id="login-text" for="text-basic">Password</label>
                <input type="password" name="password" id="text-basic" value="">

                <!-- <label id="login-text" for="text-basic">Password Again</label>
            <input type="password" name="pass" id="text-basic" value=""> -->

                <input type="submit" value="SignUp" />
            </form>
            <!-- <a href="#" id="login-btn" class="ui-btn ui-btn-b">Login</a> -->
            <!-- <a href="#alertmessage" style="margin-bottom: 120px;" id="login-btn" data-transition="slideup" class="ui-btn ui-btn-b" -->
            <!-- data-rel="popup">Register</a> -->
            <div data-role="popup" id="alertmessage" class="ui-content" data-theme="a">
                <img src="images/success.png " style="padding-top:40px;" width="100px " height="100px ">
                <p style="padding-left:50px;padding-bottom:40px;padding-right:50px;">Register Successfull!</p>
            </div>

        </div>



        <!-- Footer -->
        <div data-role="footer" style="border-style: none;background-color: black;padding-top: 4px" data-position="relative">
            <div class="ui-grid">
                <div class="ui-bar" style="height:20px;margin-top: 15px;">TEAM STYLEHUNT © 2018</div>
            </div>
            <div class="ui-grid-c">
                <div class="ui-block-e" style="width: 16.66% !important; "></div>
                <div class="ui-block-e" style="width: 16.66% !important; "><img src="images/facebook.png " width="25px "
                        height="25px " alt="Facebook Logo "></div>
                <div class="ui-block-e" style="width: 16.66% !important; "><img src="images/insta.png " width="25px "
                        height="25px " alt="Insta Logo "></div>
                <div class="ui-block-e" style="width: 16.66% !important; "><img src="images/twitter.png " width="25px "
                        height="25px " alt="Twitter Logo "></div>
                <div class="ui-block-e" style="width: 16.66% !important; "><img src="images/gplus.png " width="25px "
                        height="25px " alt="Gplus Logo "></div>
                <div class="ui-block-e" style="width: 16.66% !important; "></div>
            </div>
            <div class="ui-grid">
                <div class="ui-bar" style="height:40px;margin-top: 7px">ALL RIGHTS RESERVED</div>
            </div>
        </div>
        <!-- /footer -->
    </div>
    <!-- /page -->

</body>

</html>