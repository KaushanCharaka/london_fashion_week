<?php
session_start();
?>
<!DOCTYPE html>
<html>

<head>
    <title>London Fashion Week</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css" />
    <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script>
    <!-- Top Slider -->
    <link rel="stylesheet" type="text/css" href="engine1/style.css" />
    <script type="text/javascript" src="engine1/jquery.js"></script>
    <!-- Sliders -->
    <link rel="stylesheet" type="text/css" href="slick-1.8.1/slick/slick.css">
    <link rel="stylesheet" type="text/css" href="slick-1.8.1/slick/slick-theme.css">

    <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js"></script>

    <style>

        img {
            max-width: 100%;
            max-height: 100%;
            display: block;
            margin-left: auto;
            margin-right: auto;
            margin-top: auto;
            margin-bottom: auto;
            align: center;
        }
        
        .portrait {
            height: 80px;
            width: 25px;
        }
        
        .landscape {
            height: 25px;
            width: 80px;
        }
        
        .square {
            height: 75px;
            width: 75px;
        }
        
        .ui-grid-c {
            background-color: black;
            border-style: none !important;
        }
        
        .ui-grid {
            background-color: black;
            border-style: none !important;
        }
        
        [data-role=page] {
            height: 100% !important;
            position: relative !important;
            font-family: "Source Sans Pro"
        }
        
        [data-role=header] {
            font-size: 25px;
            align-content: center;
        }
        
        [data-role=content] {
            height: 100%;
            margin: 0 auto;
            width: auto;
        }
                
        [data-role=panel] {
            font-family: "Source Sans Pro";
            color: white;
        }
        
        .ui-block-a-header {
            margin-top: 5px;
            margin-bottom: 5px;
        }
        
        .ui-block-e {
            margin-top: 5px;
            margin-bottom: 5px;
            align: center;
        }
        
        .ui-block-b-header {
            margin-top: 15px;
            text-align: center;
            font-family: "Source Sans Pro";
            color: white;
            font-weight: normal;
            font-size: 18px;
            text-shadow: none;
        }
        
        .ui-bar {
            text-align: center;
            font-family: "Source Sans Pro";
            color: white;
            font-weight: normal;
            font-size: 12px;
            text-shadow: none;
            border-style: none !important;
        }
        
        .ui-block-c-header {
            margin-top: 16px;
        }
        
        .ui-block-d {
            margin-top: 18px;
        }
        
        body,
        input,
        select,
        textarea,
        button,
        .ui-btn {
            line-height: 1.3;
            font-family: "Source Sans Pro";
        }
        
        #search-basic {
            font-size: 24sp;
        }
        
        .ui-panel-position-right.ui-panel-display-reveal {
            -webkit-box-shadow: left;
        }
        
        .ui-btn-icon-left:after,
        .ui-btn-icon-right:after,
        .ui-btn-icon-top:after,
        .ui-btn-icon-bottom:after,
        .ui-btn-icon-notext:after {
            content: none;
        }
        
        .ui-icon-delete:after {
            background-color: black;
        }
        
        .ui-listview>li.ui-last-child>a.ui-btn {
            border-bottom-width: 0px;
        }
        
        .ui-page-theme-a .ui-btn {
            background-color: white;
        }
        
        .ui-panel-inner {
            background-color: white;
        }
        
        #wowslider-container1 {
            margin: 0px;
        }
        
        .ui-content {
            padding: 0px;
        }
        
        .ui-grid-solo {
            text-align: center;
            font-family: "Source Sans Pro";
        }
        
        #news-content {
            text-align: justify;
            margin-left: 20px;
            margin-right: 20px;
        }
        
        #center-button {
            margin: 0 auto;
            text-align: center;
            font-family: "Source Sans Pro";
            padding: 1px;
        }
        
        .search-container {
            margin-top: 25px;
            max-width: 100%;
            padding-left: 15px;
            padding-right: 15px;
        }
        
        .ui-block-perfume {
            margin-right: 1200px;
        }
        
        .ui-bar ui-bar-a-perfume {
            margin-right: 1200px;
        }
        
        .ui-block-perfume-b {
            margin-right: 1000px;
        }
        
        .topic {
            color: black;
        }
        
        .right-button {
            margin-left: 1000px;
        }

        @media all and (width: 100%) {
        .my-breakpoint.ui-grid-b .ui-block-a { width: 30%; }
        .my-breakpoint.ui-grid-b .ui-block-b { width: 50%; }
        .my-breakpoint.ui-grid-b .ui-block-c { width: 20%; }
        }

        p {
        display: block;
        margin-block-start: 0em;
        margin-block-end: 0em;
         margin-inline-start: 10px;
         margin-inline-end: 0px;
}
        .single-thread{
           
            color: black;       
        }

        .load_more_btn {
            padding-bottom: 20px;
            width: 175px;
        }

        .heart {
            margin-left: 19px;
            margin-top: 140px;
            width: 69px;
            height: 70px;
            position: relative;
            left: 50%;
            top: 50%;
            transform: translate(-50%, -50%);
            background: url(images/heart.png) no-repeat;  
            cursor: pointer;  
        }
            .heart-blast {
            background-position: -2800px 0;
            transition: background 1s steps(28);
        }
        
}
        
    </style>
</head>

<body>

    <div data-role="page">

        <div data-role="header" data-position="fixed" style="border-style: none;">
            <div class="ui-grid-c">
                <div class="ui-block-a ui-block-a-header" style="width: 30% !important; ">
                <img src="images/logo_black.png " width="50px "
                        height="50px " alt="London Fashion Week Logo ">
                </div>
                <div class="ui-block-b ui-block-b-header" style="width: 40% !important; ">All News</div>
                <div class="ui-block-c ui-block-c-header" style="width: 15% !important; "><a href="http://localhost/London_Fashion_Week/ShoppingCart.php" data-transition="slide" data-ajax="false">
                <img src="images/cart.png " width="30px " height="27px " alt="Cart "></a></div>
                <div class="ui-block-d" style="width: 15% !important; ">
                    <a href="#nav-panel"><img src="images/hamburger.png " width="28px " height="23px " alt="Cart "></a>
                </div>
            </div>
            <!-- /grid-b -->
        </div>
        <!-- /header -->

        <?php          
   
   if(!isset($_SESSION['login_user'])){
       // header("Location: http://".$_SERVER['HTTP_HOST']."/London_Fashion_Week/Login.php", true, 302);
       echo "<div data-role=\"panel\" data-position=\"right\" data-display=\"reveal\" data-theme=\"c\" id=\"nav-panel\">";
       echo "<ul data-role=\"listview\">";
       echo "<li data-icon=\"delete\"><a href=\"#\" data-rel=\"close\">Menu</a></li>";    
       echo "<input type=\"search\" name=\"search\" data-corners=\"false\" id=\"search-basic\" value=\"\" />";    
       echo " <li><a href=\"http://localhost/London_Fashion_Week/Login.php\" data-transition=\"slide\" data-ajax=\"false\">Sign in</a></li>"; 
       echo " <li><a href=\"http://localhost/London_Fashion_Week/Register.php\" data-transition=\"slide\" data-ajax=\"false\">Register</a></li>";
       echo " <li><a href=\"http://localhost/London_Fashion_Week/index.php\" data-transition=\"slide\" data-ajax=\"false\">Home</a></li>"; 
       echo " <li><a href=\"http://localhost/London_Fashion_Week/portfolio.php\" data-transition=\"slide\" data-ajax=\"false\">Portfolio</a></li>"; 
       echo " <li><a href=\"http://localhost/London_Fashion_Week/Events.php\" data-transition=\"slide\" data-ajax=\"false\">Events</a></li>"; 
       echo " <li><a href=\"http://localhost/London_Fashion_Week/Favorite_list.php\" data-transition=\"slide\" data-ajax=\"false\">Favorite List</a></li>"; 
       echo " <li><a href=\"http://localhost/London_Fashion_Week/ShoppingCart.php\" data-transition=\"slide\" data-ajax=\"false\">Shopping Cart</a></li>"; 
       echo " <li><a href=\"http://localhost/London_Fashion_Week/Point_of_Interest.php\" data-transition=\"slide\" data-ajax=\"false\">Point of Interest</a></li>"; 
       echo " <li><a href=\"http://localhost/London_Fashion_Week/gallery.php\" data-transition=\"slide\" data-ajax=\"false\">Gallery</a></li>";   
       echo " <li><a href=\"http://localhost/London_Fashion_Week/AboutUs.php\" data-transition=\"slide\" data-ajax=\"false\">About Us</a></li>";   
       echo " </ul>";   
       echo "</div>";
   }else{
       echo "<div data-role=\"panel\" data-position=\"right\" data-display=\"reveal\" data-theme=\"c\" id=\"nav-panel\">";
       echo "<ul data-role=\"listview\">";
       echo "<li data-icon=\"delete\"><a href=\"#\" data-rel=\"close\">Menu</a></li>";    
       echo "<input type=\"search\" name=\"search\" data-corners=\"false\" id=\"search-basic\" value=\"\" />";    
       echo " <li><a href=\"http://localhost/London_Fashion_Week/index.php\" data-transition=\"slide\" data-ajax=\"false\">Home</a></li>";             
       echo " <li><a href=\"http://localhost/London_Fashion_Week/MyAccount.php\" data-transition=\"slide\" data-ajax=\"false\">My Account</a></li>";
       echo " <li><a href=\"http://localhost/London_Fashion_Week/portfolio.php\" data-transition=\"slide\" data-ajax=\"false\">Portfolio</a></li>"; 
       echo " <li><a href=\"http://localhost/London_Fashion_Week/Events.php\" data-transition=\"slide\" data-ajax=\"false\">Events</a></li>"; 
       echo " <li><a href=\"http://localhost/London_Fashion_Week/Favorite_list.php\" data-transition=\"slide\" data-ajax=\"false\">Favorite List</a></li>"; 
       echo " <li><a href=\"http://localhost/London_Fashion_Week/ShoppingCart.php\" data-transition=\"slide\" data-ajax=\"false\">Shopping Cart</a></li>"; 
       echo " <li><a href=\"http://localhost/London_Fashion_Week/Point_of_Interest.php\" data-transition=\"slide\" data-ajax=\"false\">Point of Interest</a></li>"; 
       echo " <li><a href=\"http://localhost/London_Fashion_Week/gallery.php\" data-transition=\"slide\" data-ajax=\"false\">Gallery</a></li>";   
       echo " <li><a href=\"http://localhost/London_Fashion_Week/AboutUs.php\" data-transition=\"slide\" data-ajax=\"false\">About Us</a></li>";    
       echo " <li><a href=\"http://localhost/London_Fashion_Week/Logout.php\" data-transition=\"slide\" data-ajax=\"false\">Sign out</a></li>";   
       echo " </ul>";   
       echo "</div>";
   }
   ?>

        <!-- Content -->
        <div data-role="content">
            <div id="news" role="main" class="ui-content">
                <hr class="single-thread">
                <div class="ui-grid-b my-breakpoint">
                    <div class="ui-block-a">
                        <div class="ui-body ui-body-d"><img src="images/news1.jpg" alt="Sample photo"></div>
                    </div>
                    <div class="ui-block-b" style="height: 100%;">
                        <!-- <div class="ui-body ui-body-d"> -->
                        <h5 style="font-size:2vw; margin-top: 1px;">The Best Denim Looks for Spring | Shop: Who What Wear</h5>
                        <p style="font-size:2vw;margin-left: 2px;">Brand: Bvlgari – Genuine Brand Perfume: Bvlgari Man
                            Extreme Eau de
                            Toilette 60ml</p>
                        <h6 style="font-size:2vw; margin-top: 15px;">By Genuine Brand Perfume<hr></h6>
                        <p style="font-size:2vw; margin-top: -35px;">5mins ago</p>
                        <button class="ui-btn ui-btn-inline more-btn" style="width:100px;font-weight: bold;font-size: 14px;vertical-align: middle; text-shadow: none; margin-top:30px">More</button>
                        <!-- </div> -->
                    </div>
                    <div class="ui-block-c" style="margin-bottom: -50px;">   
                        <div class="heart"></div> 

                    </div>
                </div>
                <hr class="single-thread">
                <div class="ui-grid-b my-breakpoint">
                    <div class="ui-block-a">
                        <div class="ui-body ui-body-d"><img src="images/news2.jpg" alt="Sample photo"></div>
                    </div>
                    <div class="ui-block-b" style="height: 100%;">
                        <!-- <div class="ui-body ui-body-d"> -->
                        <h5 style="font-size:2vw; margin-top: 1px;">Met Gala 2018: Best and worst red carpet looks on fashion's biggest night</h5>
                        <p style="font-size:2vw;margin-left: 2px;">Wearing a blinged out version of the Pope's hat, Rihanna had all eyes on her in her Maison Margiela look at the Heavenly Bodies.</p>
                         <h6 style="font-size:2vw; margin-top: 15px;">By Laura Tompson/NY Daily News<hr></h6>
                         <p style="font-size:2vw; margin-top: -35px;">5mins ago</p>
                         <button class="ui-btn ui-btn-inline more-btn" style="width:100px;font-weight: bold;font-size: 14px;vertical-align: middle; text-shadow: none; margin-top:30px">More</button>
                        <!-- </div> -->
                    </div>
                    <div class="ui-block-c" style="margin-bottom: -50px;">

                        <div class="heart"></div> 

                    </div>
                </div>
                <hr class="single-thread">
            </div>
            <!-- Content -->

            <Center>
                <div class="load_more_btn">
                    <button id="more_news" class="mbsc-btn-block">
                        <span class="md-btn-text">Load more</span>
                    </button>
                </div>
            </Center>       
        </div>
    </div>
    </div>

    <!-- Footer -->
    <div data-role="footer" style="border-style: none;background-color: black;padding-top: 4px" data-position="relative">
        <div class="ui-grid">
            <div class="ui-bar" style="height:20px;margin-top: 15px;">TEAM STYLEHUNT © 2018</div>
        </div>
        <div class="ui-grid-c">
            <div class="ui-block-e" style="width: 16.66% !important; "></div>
            <div class="ui-block-e" style="width: 16.66% !important; "><img src="images/facebook.png " width="25px "
                    height="25px " alt="Facebook Logo "></div>
            <div class="ui-block-e" style="width: 16.66% !important; "><img src="images/insta.png " width="25px "
                    height="25px " alt="Insta Logo "></div>
            <div class="ui-block-e" style="width: 16.66% !important; "><img src="images/twitter.png " width="25px "
                    height="25px " alt="Twitter Logo "></div>
            <div class="ui-block-e" style="width: 16.66% !important; "><img src="images/gplus.png " width="25px "
                    height="25px " alt="Gplus Logo "></div>
            <div class="ui-block-e" style="width: 16.66% !important; "></div>
        </div>
        <div class="ui-grid">
            <div class="ui-bar" style="height:40px;margin-top: 7px">ALL RIGHTS RESERVED</div>
        </div>
    </div>
    <!-- /footer -->
    </div>
    <!-- /page -->


    <script type="text/javascript">
        $(document).ready(function () {
            $(function () {

                $(".heart").on("click", function () {
                    $(this).toggleClass("heart-blast");
                });
            });

            var newsCount = 2;

            $('#more_news').click(function () {

                var btnMore = $('#more_news');

                newsCount += 2;
                if (newsCount == 8) {
                    btnMore.hide();
                }

                var img1 = "\"images/news1.jpg\"";
                var img2 = "\"images/news2.jpg\"";
                var head1 = "The Best Denim Looks for Spring | Shop: Who What Wear";
                var head2 = "Met Gala 2018: Best and worst red carpet looks on fashion's biggest night";
                var body1 = "Brand: Bvlgari – Genuine Brand Perfume: Bvlgari Man Extreme Eau de Toilette 60ml";
                var body2 = "Wearing a blinged out version of the Pope's hat, Rihanna had all eyes on her in her Maison Margiela look at the Heavenly Bodies.";
                var author1 = "By Genuine Brand Perfume";
                var author2 = "By Laura Tompson/NY Daily News";               
                var time = "5mins ago"

                $('#news').append(
                    '<div class="ui-grid-b my-breakpoint">'
                    + '<div class="ui-block-a">'
                    + '<div class="ui-body ui-body-d"><img src= ' + img1 + ' alt="Sample photo"></div>'
                    + '</div>'
                    + ' <div class="ui-block-b" style="height: 100%;">'
                    + '<h5 style="font-size:2vw; margin-top: 1px;">' + head1 + '</h5>'
                    + '<p style="font-size:2vw;margin-left: 2px;">' + body1 + '</p>'
                    + '<h6 style="font-size:2vw; margin-top: 15px;">' + author1 + '<hr></h6>'
                    +'<p style="font-size:2vw; margin-top: -35px;">' + time + '</p>'
                    +'  <button class="ui-btn ui-btn-inline more-btn" style="width:100px;font-weight: bold;font-size: 14px;vertical-align: middle; text-shadow: none; margin-top:30px">More</button></div>'
                    + ' <div class="ui-block-c" style="margin-bottom: -50px;">'
                    + '<div class="heart"></div>'
                    + ' </div> </div>'
                    + '<hr class="single-thread">'

                    +  '<div class="ui-grid-b my-breakpoint">'
                    + '<div class="ui-block-a">'
                    + '<div class="ui-body ui-body-d"><img src= ' + img2 + ' alt="Sample photo"></div>'
                    + '</div>'
                    + ' <div class="ui-block-b" style="height: 100%;">'
                    + '<h5 style="font-size:2vw; margin-top: 1px;">' + head2 + '</h5>'
                    + '<p style="font-size:2vw;margin-left: 2px;">' + body2 + '</p>'
                    + '<h6 style="font-size:2vw; margin-top: 15px;">' + author2 + '<hr></h6>'
                    +'<p style="font-size:2vw; margin-top: -35px;">' + time + '</p>'
                    +'  <button class="ui-btn ui-btn-inline more-btn" style="width:100px;font-weight: bold;font-size: 14px;vertical-align: middle; text-shadow: none; margin-top:30px">More</button></div>'
                    + ' <div class="ui-block-c" style="margin-bottom: -50px;">'
                    + '<div class="heart"></div>'
                    + ' </div> </div>'
                    + '<hr class="single-thread">'
                );

            });

            $(".more-btn").on("click", function () {
                    window.location.replace("http://localhost/London_Fashion_Week/news.php");

            });

        });



    </script>

</body>

</html>