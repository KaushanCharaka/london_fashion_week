<?php
session_start();
?>
<!DOCTYPE html>
<html>

<head>
    <title>London Fashion Week</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />

    <link rel="stylesheet" href="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css" />
    <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script>
    <!-- Top Slider -->
    <link rel="stylesheet" type="text/css" href="engine1/style.css" />
    <script type="text/javascript" src="engine1/jquery.js"></script>
    <!-- Sliders -->
    <link rel="stylesheet" type="text/css" href="slick-1.8.1/slick/slick.css">
    <link rel="stylesheet" type="text/css" href="slick-1.8.1/slick/slick-theme.css">

    <style>
        .ui-grid-b {
            height: 80px !important;
        }
        
        img {
            max-width: 100%;
            max-height: 100%;
            display: block;
            margin-left: auto;
            margin-right: auto;
            margin-top: auto;
            margin-bottom: auto;
            align: center;
        }
        
        .portrait {
            height: 80px;
            width: 25px;
        }
        
        .landscape {
            height: 25px;
            width: 80px;
        }
        
        .square {
            height: 75px;
            width: 75px;
        }
        
        #header-grid {
            background-color: black;
            border-style: none !important;
        }
        
        .ui-grid {
            background-color: black;
            border-style: none !important;
        }
        
        [data-role=page] {
            height: 100% !important;
            position: relative !important;
            font-family: "Source Sans Pro"
        }
        
        [data-role=header] {
            font-size: 25px;
            align-content: center;
        }
        
        [data-role=content] {
            height: 100%;
            margin: 0 auto;
            width: auto;
        }
        /* [data-role=footer] {
            position: relative !important;
            top: auto !important;
            width: 100%;
        } */
        
        [data-role=panel] {
            font-family: "Source Sans Pro";
            color: white;
        }
        
        .ui-block-a {
            margin-top: 5px;
            margin-bottom: 5px;
        }
        
        .ui-block-e {
            margin-top: 5px;
            margin-bottom: 5px;
            align: center;
        }
        
        .ui-block-b {
            margin-top: 15px;
            text-align: center;
            font-family: "Source Sans Pro";
            color: white;
            font-weight: normal;
            font-size: 18px;
            text-shadow: none;
        }
        
        .ui-bar {
            text-align: center;
            font-family: "Source Sans Pro";
            color: white;
            font-weight: normal;
            font-size: 12px;
            text-shadow: none;
            border-style: none !important;
        }
        
        .ui-block-c {
            margin-top: 16px;
        }
        
        .ui-block-d {
            margin-top: 18px;
        }
        
        body,
        input,
        select,
        textarea,
        button,
        .ui-btn {
            line-height: 1.3;
            font-family: "Source Sans Pro";
        }
        
        #search-basic {
            font-size: 24sp;
        }
        
        .ui-panel-position-right.ui-panel-display-reveal {
            -webkit-box-shadow: left;
        }
        
        .ui-btn-icon-left:after,
        .ui-btn-icon-right:after,
        .ui-btn-icon-top:after,
        .ui-btn-icon-bottom:after,
        .ui-btn-icon-notext:after {
            content: none;
        }
        
        .ui-icon-delete:after {
            background-color: black;
        }
        
        .ui-listview>li.ui-last-child>a.ui-btn {
            border-bottom-width: 0px;
        }
        
        .ui-page-theme-a .ui-btn {
            background-color: white;
        }
        
        .ui-panel-inner {
            background-color: white;
        }
        
        #wowslider-container1 {
            margin: 0px;
        }
        
        .ui-content {
            padding: 0px;
        }
        
        .ui-grid-solo {
            text-align: center;
            font-family: "Source Sans Pro";
        }
        
        #news-content {
            text-align: justify;
            margin-left: 20px;
            margin-right: 20px;
        }
        
        #center-button {
            margin: 0 auto;
            text-align: center;
            font-family: "Source Sans Pro";
            padding: 1px;
        }
        
        li.ui-block-b.ui-state-default.ui-corner-top {
            margin-top: 0px;
        }
        
        li.ui-block-c.ui-state-default.ui-corner-top {
            margin-top: 0px;
        }
        
        li.ui-block-a.ui-state-default.ui-corner-top {
            margin-top: 0px;
        }
        
        li.ui-block-d.ui-state-default.ui-corner-top {
            margin-top: 0px;
        }
        
        [data-role="navbar"] {
            padding: 15px;
        }
        
        .ui-mini {
            margin-left: 18px;
            margin-right: 18px;
            margin-top: 19px;
            margin-bottom: 0px;
        }
        
        .ui-page-theme-a .ui-btn.ui-btn-active {
            background-color: black;
            border-color: black;
            lighting-color: aliceblue
        }
        
        .ui-input-text,
        .ui-input-search {
            margin-left: 18px;
            margin-right: 18px;
            margin-top: 0px;
        }
        
        #name-text {
            text-align: center;
            font-family: "Source Sans Pro";
            font-weight: bold;
            font-size: 14px;
            height: 100% !important;
        }
        
        p {
            font-size: 14px;
            font-family: "Source Sans Pro";
            text-align: center;
        }
        
        h3 {
            font-size: 18px;
            font-family: "Source Sans Pro";
            text-align: center;
        }
        
        .heart {
            margin-left: 5px;
            margin-top: 15px;
            width: 69px;
            height: 70px;
            padding: 10px;
            position: relative;
            ;
            left: 50%;
            top: 50%;
            transform: translate(-50%, -50%);
            background: url(images/heart.png) no-repeat;
            cursor: pointer;
        }
        
        .heart-blast {
            background-position: -2800px 0;
            transition: background 1s steps(28);
        }
        
    </style>
</head>

<body>

    <div data-role="page">

        <div data-role="header" data-position="fixed" style="border-style: none;">
            <div class="ui-grid-c" id="header-grid">
                <div class="ui-block-a" style="width: 30% !important; "><img src="images/logo_black.png " width="50px " height="50px " alt="London Fashion Week Logo "></div>
                <div class="ui-block-b" style="width: 40% !important; ">About Us</div>
                <div class="ui-block-c" style="width: 15% !important; ">
                <a href="http://localhost/London_Fashion_Week/ShoppingCart.php" data-transition="slide" data-ajax="false">
                <img src="images/cart.png " width="30px " height="27px " alt="Cart "></a></div>
                <div class="ui-block-d" style="width: 15% !important; ">
                    <a href="#nav-panel"><img src="images/hamburger.png " width="28px " height="23px " alt="Cart "></a>
                </div>
            </div>
            <!-- /grid-b -->
        </div>
        <!-- /header -->

        <?php          
   
   if(!isset($_SESSION['login_user'])){
       // header("Location: http://".$_SERVER['HTTP_HOST']."/London_Fashion_Week/Login.php", true, 302);
       echo "<div data-role=\"panel\" data-position=\"right\" data-display=\"reveal\" data-theme=\"c\" id=\"nav-panel\">";
       echo "<ul data-role=\"listview\">";
       echo "<li data-icon=\"delete\"><a href=\"#\" data-rel=\"close\">Menu</a></li>";    
       echo "<input type=\"search\" name=\"search\" data-corners=\"false\" id=\"search-basic\" value=\"\" />";    
       echo " <li><a href=\"http://localhost/London_Fashion_Week/Login.php\" data-transition=\"slide\" data-ajax=\"false\">Sign in</a></li>"; 
       echo " <li><a href=\"http://localhost/London_Fashion_Week/Register.php\" data-transition=\"slide\" data-ajax=\"false\">Register</a></li>";
       echo " <li><a href=\"http://localhost/London_Fashion_Week/index.php\" data-transition=\"slide\" data-ajax=\"false\">Home</a></li>"; 
       echo " <li><a href=\"http://localhost/London_Fashion_Week/all_news.php\" data-transition=\"slide\" data-ajax=\"false\">News</a></li>"; 
       echo " <li><a href=\"http://localhost/London_Fashion_Week/portfolio.php\" data-transition=\"slide\" data-ajax=\"false\">Portfolio</a></li>"; 
       echo " <li><a href=\"http://localhost/London_Fashion_Week/Events.php\" data-transition=\"slide\" data-ajax=\"false\">Events</a></li>"; 
       echo " <li><a href=\"http://localhost/London_Fashion_Week/Favorite_list.php\" data-transition=\"slide\" data-ajax=\"false\">Favorite List</a></li>"; 
       echo " <li><a href=\"http://localhost/London_Fashion_Week/ShoppingCart.php\" data-transition=\"slide\" data-ajax=\"false\">Shopping Cart</a></li>"; 
       echo " <li><a href=\"http://localhost/London_Fashion_Week/Point_of_Interest.php\" data-transition=\"slide\" data-ajax=\"false\">Point of Interest</a></li>"; 
       echo " <li><a href=\"http://localhost/London_Fashion_Week/gallery.php\" data-transition=\"slide\" data-ajax=\"false\">Gallery</a></li>";   
       echo " </ul>";   
       echo "</div>";
   }else{
       echo "<div data-role=\"panel\" data-position=\"right\" data-display=\"reveal\" data-theme=\"c\" id=\"nav-panel\">";
       echo "<ul data-role=\"listview\">";
       echo "<li data-icon=\"delete\"><a href=\"#\" data-rel=\"close\">Menu</a></li>";    
       echo "<input type=\"search\" name=\"search\" data-corners=\"false\" id=\"search-basic\" value=\"\" />";    
       echo " <li><a href=\"http://localhost/London_Fashion_Week/index.php\" data-transition=\"slide\" data-ajax=\"false\">Home</a></li>";             
       echo " <li><a href=\"http://localhost/London_Fashion_Week/MyAccount.php\" data-transition=\"slide\" data-ajax=\"false\">My Account</a></li>";
       echo " <li><a href=\"http://localhost/London_Fashion_Week/all_news.php\" data-transition=\"slide\" data-ajax=\"false\">News</a></li>"; 
       echo " <li><a href=\"http://localhost/London_Fashion_Week/portfolio.php\" data-transition=\"slide\" data-ajax=\"false\">Portfolio</a></li>"; 
       echo " <li><a href=\"http://localhost/London_Fashion_Week/Events.php\" data-transition=\"slide\" data-ajax=\"false\">Events</a></li>"; 
       echo " <li><a href=\"http://localhost/London_Fashion_Week/Favorite_list.php\" data-transition=\"slide\" data-ajax=\"false\">Favorite List</a></li>"; 
       echo " <li><a href=\"http://localhost/London_Fashion_Week/ShoppingCart.php\" data-transition=\"slide\" data-ajax=\"false\">Shopping Cart</a></li>"; 
       echo " <li><a href=\"http://localhost/London_Fashion_Week/Point_of_Interest.php\" data-transition=\"slide\" data-ajax=\"false\">Point of Interest</a></li>"; 
       echo " <li><a href=\"http://localhost/London_Fashion_Week/gallery.php\" data-transition=\"slide\" data-ajax=\"false\">Gallery</a></li>";     
       echo " <li><a href=\"http://localhost/London_Fashion_Week/Logout.php\" data-transition=\"slide\" data-ajax=\"false\">Sign out</a></li>";   
       echo " </ul>";   
       echo "</div>";
   }
   ?>


        <div data-role="content">

            <!-- /panel -->
            <h3>Our Team</h3>
            <p>Founded in 2012<br>We have four team members<br>They are very experienced in software developing</p>


            <div class="ui-grid-a" id="name-text">
                <div class="ui-block-e" style="width: 50% !important; ">Haritha Gajanayake</div>
                <div class="ui-block-e" style="width: 50% !important; ">Ashan De Vaas</div>
            </div>
            <div class="ui-grid-a" id="name-text">
                <div class="ui-block-e" style="width: 50% !important; "><img src="images/haritha.gif" style="width: 120px;height: 120px;"></div>
                <div class="ui-block-e" style="width: 50% !important; "><img src="images/ashan.gif " style="width: 120px;height: 120px;"></div>
            </div>
            <div class="ui-grid-a" id="name-text">
                <div class="ui-block-e" style="width: 50% !important; ">
                    <div class="ui-grid-a">
                        <div class="ui-block-e" style="width: 30% !important; ">
                            <div class="heart"></div>

                        </div>
                        <div class ="ui-grid-solo">
                        <div class="ui-block-e" style=" !important;">
                            <div id="center-button">
                                <button id="haritha-btn" class="ui-btn ui-btn-inline haritha-btn" style="width: 100%; font-weight: bold;font-size: 13px;margin: 0px;">View
                                        Profile</button>
                                <div class="designation">
                                    <h6>Co-Founder</h6>
                                </div>
                                <div class="address">
                                    <h6>No:63 Galpoththa Road, Nawala,Rajagiriya,Sri Lanka</h6>
                                </div>
                                <div class="number"style="margin-top:-10px">
                                    <h6>0778466768</h6>
                                </div>
                            </div>
                        </div>
                        </div>

                        <div class="ui-block-e" style="width: 10% !important;"></div>

                    </div>

                </div>
                <div class="ui-block-e" style="width: 50% !important; ">
                    <div class="ui-grid-a">
                        <div class="ui-block-e" style="width: 30% !important; ">
                            <div class="heart"></div>

                        </div>
                        <div class ="ui-grid-solo">
                        <div class="ui-block-e" style=" !important;">
                            <div id="center-button">
                                <button class="ui-btn ui-btn-inline ashan-btn" style="width: 100%; font-weight: bold;font-size: 13px;margin: 0px;">View
                                        Profile</button>
                                <div class="designation">
                                    <h6>Co-Founder</h6>
                                </div>
                                <div class="address">
                                    <h6>No:143/A,Galwala Road,Piliyandala,Sri Lanka</h6>
                                </div>
                                <div class="number"style="margin-top:-10px">
                                    <h6>0714634836</h6>
                                </div>
                            </div>
                        </div>
                        </div>
                        <div class="ui-block-e" style="width: 10% !important;"></div>


                    </div>
                </div>
            </div>
            <div class="ui-grid-a" id="name-text">
                <div class="ui-block-e" style="width: 50% !important; ">Kaushan Fernando</div>
                <div class="ui-block-e" style="width: 50% !important; ">Menusha Pathirana</div>
            </div>
            <div class="ui-grid-a" id="name-text">
                <div class="ui-block-e" style="width: 50% !important; "><img src="images/kaushan.gif " style="width: 120px;height: 120px;"></div>
                <div class="ui-block-e" style="width: 50% !important; "><img src="images/menusha.gif " style="width: 120px;height: 120px;"></div>
            </div>
            <div class="ui-grid-a" id="name-text">
                <div class="ui-block-e" style="width: 50% !important; ">
                    <div class="ui-grid-a">
                        <div class="ui-block-e" style="width: 30% !important; ">
                            <div class="heart"></div>

                        </div>
                        <div class ="ui-grid-solo">
                        <div class="ui-block-e" style=" !important;">
                            <div id="center-button">
                                <button class="ui-btn ui-btn-inline kaushan-btn" style="width: 100%; font-weight: bold;font-size: 13px;margin: 0px;">View
                                    Profile</button>
                                <div class="designation">
                                    <h6>Co-Founder</h6>
                                </div>
                                <div class="address">
                                    <h6>No:A342,Kiwi Avenue,Panadura,Sri Lanka</h6>
                                </div>
                                 <div class="number"style="margin-top:-10px">
                                    <h6>0762334655</h6>
                                </div>
                            </div>
                        </div>
                        </div>
                        <div class="ui-block-e" style="width: 10% !important;"></div>


                    </div>
                </div>
                <div class="ui-block-e" style="width: 50% !important; ">
                    <div class="ui-grid-a">
                        <div class="ui-block-e" style="width: 30% !important; ">
                            <div class="heart"></div>

                        </div>
                        <div class ="ui-grid-solo">
                        <div class="ui-block-e" style=" !important;">
                            <div id="center-button">
                                <button class="ui-btn ui-btn-inline menusha-btn" style="width: 100%; font-weight: bold;font-size: 13px;margin: 0px;">View
                                    Profile</button>
                                <div class="designation">
                                    <h6>Co-Founder</h6>
                                </div>
                                <div class="address">
                                    <h6>No:A/132A,Sirimal Road,Piliyandala,Kottawa,Sri Lanka</h6>
                                </div>
                                <div class="number"style="margin-top:-10px">
                                    <h6>0725443889</h6>
                                </div>
                            </div>
                        </div>

                        </div>
                        <div class="ui-block-e" style="width: 10% !important;"></div>


                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript">
            $(document).ready(function () {
                $(function () {

                    $(".heart").on("click", function () {
                        $(this).toggleClass("heart-blast");
                    });
                });

                $(".haritha-btn").on("click", function () {
                    window.location.replace("http://localhost/London_Fashion_Week/profile_haritha.php");

                });

                $(".ashan-btn").on("click", function () {
                    window.location.replace("http://localhost/London_Fashion_Week/profile_ashan.php");

                });

                $(".kaushan-btn").on("click", function () {
                    window.location.replace("http://localhost/London_Fashion_Week/profile_kaushan.php");

                });

                $(".menusha-btn").on("click", function () {
                    window.location.replace("http://localhost/London_Fashion_Week/profile_menusha.php");

                });               
               
            });
        </script>

        <!-- Footer -->
        <div data-role="footer" style="border-style: none;background-color: black;padding-top: 4px" data-position="relative">
            <div class="ui-grid">
                <div class="ui-bar" style="height:20px;margin-top: 15px;">TEAM STYLEHUNT © 2018</div>
            </div>
            <div class="ui-grid-c">
                <div class="ui-block-e" style="width: 16.66% !important; "></div>
                <div class="ui-block-e" style="width: 16.66% !important; "><img src="images/facebook.png " width="25px " height="25px " alt="Facebook Logo "></div>
                <div class="ui-block-e" style="width: 16.66% !important; "><img src="images/insta.png " width="25px " height="25px " alt="Insta Logo "></div>
                <div class="ui-block-e" style="width: 16.66% !important; "><img src="images/twitter.png " width="25px " height="25px " alt="Twitter Logo "></div>
                <div class="ui-block-e" style="width: 16.66% !important; "><img src="images/gplus.png " width="25px " height="25px " alt="Gplus Logo "></div>
                <div class="ui-block-e" style="width: 16.66% !important; "></div>
            </div>
            <div class="ui-grid">
                <div class="ui-bar" style="height:40px;margin-top: 7px">ALL RIGHTS RESERVED</div>
            </div>
        </div>
        <!-- /footer -->
    </div>
    <!-- /page -->

</body>

</html>