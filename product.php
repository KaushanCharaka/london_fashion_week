<?php
session_start();
?>
<!DOCTYPE html>
<html>

<head>
    <title>London Fashion Week</title>

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />

    <link rel="stylesheet" href="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.css" />
    <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="http://code.jquery.com/mobile/1.4.5/jquery.mobile-1.4.5.min.js"></script>
    <!-- Stars -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <!-- Top Slider -->
    <link rel="stylesheet" type="text/css" href="engine1/style.css" />
    <script type="text/javascript" src="engine1/jquery.js"></script>
    <!-- Sliders -->
    <link rel="stylesheet" type="text/css" href="slick-1.8.1/slick/slick.css">
    <link rel="stylesheet" type="text/css" href="slick-1.8.1/slick/slick-theme.css">

    <script type="text/javascript" src="engine1/wowslider.js"></script>
    <script type="text/javascript" src="engine1/script.js"></script>
    <script src="https://code.jquery.com/jquery-2.2.0.min.js" type="text/javascript"></script>
    <script src="slick-1.8.1/slick/slick.js" type="text/javascript" charset="utf-8"></script>

    <style>
        .ui-grid-b {
            height: 80px !important;
        }
        
        img {
            max-width: 100%;
            max-height: 100%;
            display: block;
            margin-left: auto;
            margin-right: auto;
            margin-top: auto;
            margin-bottom: auto;
            align: center;
        }
        
        .portrait {
            height: 80px;
            width: 25px;
        }
        
        .landscape {
            height: 25px;
            width: 80px;
        }
        
        .square {
            height: 75px;
            width: 75px;
        }
        
        .ui-grid-c {
            background-color: black;
            border-style: none !important;
        }
        
        .ui-grid {
            background-color: black;
            border-style: none !important;
        }
        
        [data-role=page] {
            height: 100% !important;
            position: relative !important;
            font-family: "Source Sans Pro"
        }
        
        [data-role=header] {
            font-size: 25px;
            align-content: center;
        }
        
        [data-role=content] {
            height: 100%;
            margin: 0 auto;
            width: auto;
        }
        /* [data-role=footer] {
            position: relative !important;
            top: auto !important;
            width: 100%;
        } */
        
        [data-role=panel] {
            font-family: "Source Sans Pro";
            color: white;
        }
        
        .ui-block-a {
            margin-top: 5px;
            margin-bottom: 5px;
        }
        
        .ui-block-e {
            margin-top: 5px;
            margin-bottom: 5px;
            align: center;
        }
        
        .ui-block-b {
            margin-top: 15px;
            text-align: center;
            font-family: "Source Sans Pro";
            color: white;
            font-weight: normal;
            font-size: 18px;
            text-shadow: none;
        }
        
        .ui-bar {
            text-align: center;
            font-family: "Source Sans Pro";
            color: white;
            font-weight: normal;
            font-size: 12px;
            text-shadow: none;
            border-style: none !important;
        }
        
        .ui-block-c {
            margin-top: 16px;
        }
        
        .ui-block-d {
            margin-top: 18px;
        }
        
        body,
        input,
        select,
        textarea,
        button,
        .ui-btn {
            line-height: 1.3;
            font-family: "Source Sans Pro";
        }
        
        #search-basic {
            font-size: 24sp;
        }
        
        .ui-panel-position-right.ui-panel-display-reveal {
            -webkit-box-shadow: left;
        }
        
        .ui-btn-icon-left:after,
        .ui-btn-icon-right:after,
        .ui-btn-icon-top:after,
        .ui-btn-icon-bottom:after,
        .ui-btn-icon-notext:after {
            content: none;
        }
        
        .ui-icon-delete:after {
            background-color: black;
        }
        
        .ui-listview>li.ui-last-child>a.ui-btn {
            border-bottom-width: 0px;
        }
        
        .ui-page-theme-a .ui-btn {
            background-color: white;
        }
        
        .ui-panel-inner {
            background-color: white;
        }
        
        #wowslider-container1 {
            margin: 0px;
        }
        
        .ui-content {
            padding: 0px;
        }
        
        .ui-grid-solo {
            text-align: center;
            font-family: "Source Sans Pro";
        }
        
        #news-content {
            text-align: justify;
            margin-left: 20px;
            margin-right: 20px;
        }
        
        #center-button {
            margin: 0 auto;
            text-align: center;
            font-family: "Source Sans Pro";
            padding: 1px;
        }
        
        .checked {
            color: orange;
        }
        
        @media all and (max-width: 35em) {
            .my-breakpoint .ui-block-a,
            .my-breakpoint .ui-block-b,
            .my-breakpoint .ui-block-c,
            .my-breakpoint .ui-block-d,
            .my-breakpoint .ui-block-e {
                width: 100%;
                float: none;
            }
        }
        
        @media all and (min-width: 45em) {
            .my-breakpoint.ui-grid-b .ui-block-a {
                width: 25.00%;
            }
            .my-breakpoint.ui-grid-b .ui-block-b,
            .my-breakpoint.ui-grid-b .ui-block-c {
                width: 24.925%;
            }
        }
        .my-breakpoint{
            height: 100% !important;
        }
    </style>
</head>

<body>

    <div data-role="page">

        <div data-role="header" data-position="fixed" style="border-style: none;">
            <div class="ui-grid-c">
                <div class="ui-block-a" style="width: 30% !important; ">
                <a href="http://localhost/London_Fashion_Week/Product_list.php" data-transition="slide" data-ajax="false">
                <img src="images/back.png " width="50px " height="50px " alt="London Fashion Week Logo "></a></div>
                <div class="ui-block-b" style="width: 40% !important; ">Product</div>
                <div class="ui-block-c" style="width: 15% !important; "><a href="http://localhost/London_Fashion_Week/ShoppingCart.php" data-transition="slide" data-ajax="false">
                <img src="images/cart.png " width="30px " height="27px " alt="Cart "></a></div>
                <div class="ui-block-d" style="width: 15% !important; ">
                    <a href="#nav-panel"><img src="images/hamburger.png " width="28px " height="23px " alt="Cart "></a>
                </div>
            </div>
            <!-- /grid-b -->
        </div>
        <!-- /header -->

        <?php          
   
   if(!isset($_SESSION['login_user'])){
       // header("Location: http://".$_SERVER['HTTP_HOST']."/London_Fashion_Week/Login.php", true, 302);
       echo "<div data-role=\"panel\" data-position=\"right\" data-display=\"reveal\" data-theme=\"c\" id=\"nav-panel\">";
       echo "<ul data-role=\"listview\">";
       echo "<li data-icon=\"delete\"><a href=\"#\" data-rel=\"close\">Menu</a></li>";    
       echo "<input type=\"search\" name=\"search\" data-corners=\"false\" id=\"search-basic\" value=\"\" />";    
       echo " <li><a href=\"http://localhost/London_Fashion_Week/Login.php\" data-transition=\"slide\" data-ajax=\"false\">Sign in</a></li>"; 
       echo " <li><a href=\"http://localhost/London_Fashion_Week/Register.php\" data-transition=\"slide\" data-ajax=\"false\">Register</a></li>";
       echo " <li><a href=\"http://localhost/London_Fashion_Week/index.php\" data-transition=\"slide\" data-ajax=\"false\">Home</a></li>"; 
       echo " <li><a href=\"http://localhost/London_Fashion_Week/all_news.php\" data-transition=\"slide\" data-ajax=\"false\">News</a></li>"; 
       echo " <li><a href=\"http://localhost/London_Fashion_Week/portfolio.php\" data-transition=\"slide\" data-ajax=\"false\">Portfolio</a></li>"; 
       echo " <li><a href=\"http://localhost/London_Fashion_Week/Events.php\" data-transition=\"slide\" data-ajax=\"false\">Events</a></li>"; 
       echo " <li><a href=\"http://localhost/London_Fashion_Week/Favorite_list.php\" data-transition=\"slide\" data-ajax=\"false\">Favorite List</a></li>"; 
       echo " <li><a href=\"http://localhost/London_Fashion_Week/ShoppingCart.php\" data-transition=\"slide\" data-ajax=\"false\">Shopping Cart</a></li>"; 
       echo " <li><a href=\"http://localhost/London_Fashion_Week/Point_of_Interest.php\" data-transition=\"slide\" data-ajax=\"false\">Point of Interest</a></li>"; 
       echo " <li><a href=\"http://localhost/London_Fashion_Week/gallery.php\" data-transition=\"slide\" data-ajax=\"false\">Gallery</a></li>";   
       echo " <li><a href=\"http://localhost/London_Fashion_Week/AboutUs.php\" data-transition=\"slide\" data-ajax=\"false\">About Us</a></li>";   
       echo " </ul>";   
       echo "</div>";
   }else{
       echo "<div data-role=\"panel\" data-position=\"right\" data-display=\"reveal\" data-theme=\"c\" id=\"nav-panel\">";
       echo "<ul data-role=\"listview\">";
       echo "<li data-icon=\"delete\"><a href=\"#\" data-rel=\"close\">Menu</a></li>";    
       echo "<input type=\"search\" name=\"search\" data-corners=\"false\" id=\"search-basic\" value=\"\" />";    
       echo " <li><a href=\"http://localhost/London_Fashion_Week/index.php\" data-transition=\"slide\" data-ajax=\"false\">Home</a></li>";             
       echo " <li><a href=\"http://localhost/London_Fashion_Week/MyAccount.php\" data-transition=\"slide\" data-ajax=\"false\">My Account</a></li>";
       echo " <li><a href=\"http://localhost/London_Fashion_Week/all_news.php\" data-transition=\"slide\" data-ajax=\"false\">News</a></li>"; 
       echo " <li><a href=\"http://localhost/London_Fashion_Week/portfolio.php\" data-transition=\"slide\" data-ajax=\"false\">Portfolio</a></li>"; 
       echo " <li><a href=\"http://localhost/London_Fashion_Week/Events.php\" data-transition=\"slide\" data-ajax=\"false\">Events</a></li>"; 
       echo " <li><a href=\"http://localhost/London_Fashion_Week/Favorite_list.php\" data-transition=\"slide\" data-ajax=\"false\">Favorite List</a></li>"; 
       echo " <li><a href=\"http://localhost/London_Fashion_Week/ShoppingCart.php\" data-transition=\"slide\" data-ajax=\"false\">Shopping Cart</a></li>"; 
       echo " <li><a href=\"http://localhost/London_Fashion_Week/Point_of_Interest.php\" data-transition=\"slide\" data-ajax=\"false\">Point of Interest</a></li>"; 
       echo " <li><a href=\"http://localhost/London_Fashion_Week/gallery.php\" data-transition=\"slide\" data-ajax=\"false\">Gallery</a></li>";   
       echo " <li><a href=\"http://localhost/London_Fashion_Week/AboutUs.php\" data-transition=\"slide\" data-ajax=\"false\">About Us</a></li>";    
       echo " <li><a href=\"http://localhost/London_Fashion_Week/Logout.php\" data-transition=\"slide\" data-ajax=\"false\">Sign out</a></li>";   
       echo " </ul>";   
       echo "</div>";
   }
   ?>

        <!-- /panel -->

        <!-- Slider -->
        <div data-role="content">
            <div id="wowslider-container1">
                <div class="ws_images">
                    <ul>
                        <li><img src="data1/images/img_slider_1.jpg" alt="img_slider_1" title="img_slider_1" id="wows1_0" /></li>
                        <li><img src="data1/images/img_slider_2.jpg" alt="img_slider_2" title="img_slider_2" id="wows1_1" /></li>
                        <li><img src="data1/images/img_slider_3.jpg" alt="wow slider" title="img_slider_3" id="wows1_2" /></a>
                        </li>
                        <li><img src="data1/images/img_slider_4.jpg" alt="img_slider_4" title="img_slider_4" id="wows1_3" /></li>
                    </ul>
                </div>
                <div class="ws_bullets">
                    <div>
                        <a href="#" title="img_slider_1"><span><img src="data1/tooltips/img_slider_1.jpg" alt="img_slider_1" />1</span></a>
                        <a href="#" title="img_slider_2"><span><img src="data1/tooltips/img_slider_2.jpg" alt="img_slider_2" />2</span></a>
                        <a href="#" title="img_slider_3"><span><img src="data1/tooltips/img_slider_3.jpg" alt="img_slider_3" />3</span></a>
                        <a href="#" title="img_slider_4"><span><img src="data1/tooltips/img_slider_4.jpg" alt="img_slider_4" />4</span></a>
                    </div>
                </div>
                <div class="ws_script" style="position:absolute;left:-99%">
                    </a>
                </div>
                <div class="ws_shadow"></div>
            </div>

            <!-- Product name -->
            <div class="ui-grid-solo">

                <div class="ui-block-a-product" style="font-size:28px; text-align: left;"><b>Nike</b></div>
                <p class="productdes" style="margin-bottom:0px;">
                    Share
                    A smart fashion retailer knows that product descriptions can mean the difference between making a
                    sale,
                    and losing a potential customer forever.

                    They can give the consumer the information they need to feel comfortable parting with their money.
                    They can change “that’s a nice coat” into “I MUST have that coat”.</p>

                <h2>
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star"></span>
                    <span class="fa fa-star"></span>
                </h2>

                <h2 class="earnings">You'll earn 50 pts</h2>

                <h2 class="price">$52.52</h2>


                <div class="ui-grid-a">
                    <div class="ui-block-a">
                        <h2 class="quantity" >Quantity</h2>
                    </div>
                    <div class="ui-block-b">
                        <form>
                            <fieldset data-role="controlgroup" data-type="horizontal" data-mini="true" >

                                <label for="select-native-14">Select A</label>
                                <select name="select-native-14" id="select-native-14">
                                            <option value="#">1</option>
                                            <option value="#">2</option>
                                            <option value="#">3</option>
                                      <option value="#">4</option>
                                            <option value="#">5</option>
                                            <option value="#">6</option>
                                        </select>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
            <div class="ui-grid-solo">

                <button class="ui-btn ui-corner-all buy-product">Buy It Now</button>
                <a href="#" class="ui-btn ui-btn-b">Add To Cart</a>
            </div>
            <!-- Seller Grid -->
            <div class="ui-grid-b my-breakpoint">
                <div class="ui-block-a">
                    <div class="ui-body ui-body-d"><img src="data1/images/img_slider_1.jpg" alt="Sample photo"></div>
                </div>
                <div class="ui-block-b">
                    <!-- <div class="ui-body ui-body-d"><section>float: left; margin: 0 1.5%; width: 63%;</section>Block B</div> -->
                </div>

                <div class="ui-block-c">
                    <div class="ui-body ui-body-d">
                    <div style="font-size:18px";><b>Payment Methods</b></div>
                    
                    
                    <div class="ui-body ui-body-d"; style="width: 40px !important; height: 40px !important;"><img src="data1/icons/mastercard.svg" ></div>
                    <div class="ui-body ui-body-d"; style="width: 40px !important; height: 40px !important;"><img src="data1/icons/visa.svg" ></div>
                    <div class="ui-body ui-body-d"; style="width: 50px !important; height: 50px !important;"><img src="data1/icons/paypal.svg" ></div>
                    <div class="ui-body ui-body-d"; style="width: 40px !important; height: 40px !important;"><img src="data1/icons/cash.svg" ></div>

                    <br>
                    <div style="font-size:18px" ;><b>Seller Rating</b></div>

                <h2>
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star"></span>
                    <span class="fa fa-star"></span>
                </h2>
                </div>
                </div>


            </div>
        </div>


        <script type="text/javascript">
                $(document).on('ready', function () {
                    
                    $(".buy-product").on("click", function () {
                    window.location.replace("http://localhost/London_Fashion_Week/ShoppingCart.php");                    

                    });

                });
            </script>

    </div>




    <!-- Footer -->
    <div data-role="footer" style="border-style: none;background-color: black;padding-top: 4px" data-position="relative">
        <div class="ui-grid">
            <div class="ui-bar" style="height:20px;margin-top: 15px;">TEAM STYLEHUNT © 2018</div>
        </div>
        <div class="ui-grid-c">
            <div class="ui-block-e" style="width: 16.66% !important; "></div>
            <div class="ui-block-e" style="width: 16.66% !important; "><img src="images/facebook.png " width="25px "
                    height="25px " alt="Facebook Logo "></div>
            <div class="ui-block-e" style="width: 16.66% !important; "><img src="images/insta.png " width="25px "
                    height="25px " alt="Insta Logo "></div>
            <div class="ui-block-e" style="width: 16.66% !important; "><img src="images/twitter.png " width="25px "
                    height="25px " alt="Twitter Logo "></div>
            <div class="ui-block-e" style="width: 16.66% !important; "><img src="images/gplus.png " width="25px "
                    height="25px " alt="Gplus Logo "></div>
            <div class="ui-block-e" style="width: 16.66% !important; "></div>
        </div>
        <div class="ui-grid">
            <div class="ui-bar" style="height:40px;margin-top: 7px">ALL RIGHTS RESERVED</div>
        </div>
    </div>
    <!-- /footer -->
    </div>
    <!-- /page -->

</body>

</html>